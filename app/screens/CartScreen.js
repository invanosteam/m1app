import React ,{Component} from 'react';
import {
    Text,View,Image,
    ScrollView,ListView,Dimensions,
    TouchableHighlight,StyleSheet,
    AsyncStorage,TouchableOpacity,Alert,
    ToolbarAndroid,Picker,ActivityIndicator,
    ToastAndroid 
} from 'react-native'
import {Content,Container,Item,Card,Footer,Button,List,Input,Icon,ListItem,CardItem,Body,Thumbnail} from 'native-base';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import ActionBar from 'react-native-action-bar';

import CheckBox from 'react-native-checkbox';

import FooterTabBar from './FooterTabBar';

import Loader from './Loader';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import styles from '../styles/CartScreen.style';

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class CartScreen extends Component{
    constructor(){
        super()
        this.state={
            cartProductList:ds,
            crossSellProducts:ds,
            qty:'',
            subTotal:'',
            discount:'',
            grandTotal:'',
            activeTabNo:2,
            load:true,
            isEmpty:false,
            count:0,
            clicked:false,
            array:[],
        }
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('CartScreen'));
    }

    componentWillUnmount(){
        const analytics = new Analytics(urls.AppId, null, { debug: true });
        analytics.hit(new PageHit('ProductScreen'));
    }

    componentDidMount() {
        this.getAmount();
        this.getData();
    }

    getAmount(){
        fetchData.getMaster(urls.getCartTotal).then((responseJson)=>{
            // alert(JSON.stringify(responseJson.model))
            if(responseJson.code==0){
                var subtotal = Math.round(responseJson.model.subtotal)
                var discount = Math.round(responseJson.model.discount)
                var grandTotal = Math.round(responseJson.model.grand_total)
                this.setState({
                    subTotal:'₹ '+subtotal,
                    discount:'₹ '+discount,
                    grandTotal:'₹ '+grandTotal,
                }); 
            }else{
                ToastAndroid.show('Nodata Found'+responseJson.msg[0], ToastAndroid.SHORT);
            }
        });
    }


    getData(){
        this.setState({count:global.cartItemCount});

        fetchData.getMaster(urls.getCartInfo).then((responseJson)=>{
            if(responseJson.code==1){
                // alert(JSON.stringify(responseJson.model.cart_items))
                global.cartItemCount=responseJson.model.cart_items_count;
                // if(responseJson.model.cross_sell!=null){
                //     for (let prop in responseJson.model.cross_sell) {
                //         this.state.array.push(responseJson.model.cross_sell[prop]);
                //     }
                //     this.setState({
                //         crossSellProducts:ds.cloneWithRows(this.state.array),
                //     })
                // }
                this.setState({
                    cartProductList:ds.cloneWithRows(responseJson.model.cart_items),
                    count:responseJson.model.cart_items_count,
                    isEmpty:false,
                    load:false,
                });
            }else{
                this.setState({
                    isEmpty:true,
                    load:false,
                });
            }
        });
    };

    updateCart=(data,screenName)=>{
        fetchData.postMaster(urls.cartUpdate,data).then((responseJson)=>{
            if(responseJson.code==0){
                if(screenName!=null){
                    this.onClickEvent(screenName);
                }else{
                    this.getData();
                }
            }else{
                this.setState({load:false});
                ToastAndroid.show(''+responseJson.msg,ToastAndroid.SHORT);
            }
        })
    };

    removeProduct=(itemId,screenName)=>{
        this.setState({load:true});
        var url=urls.removeProduct+itemId;

        fetchData.getMaster(url).then((responseJson)=>{
            if(responseJson.code==0){
                if(screenName==null){
                    this.getData();
                }else{
                   this.onClickEvent(screenName);
                   this.setState({load:false});
                }
            }else if(responseJson.code==5) {
                alert(responseJson.msg);
                this.setState({load:false});
            }
        })
    }

    onClickEvent=(screenName)=>{
        this.props.navigation.navigate(screenName);
    }

    checkOut=()=>{
        this.setState({clicked:true});
        setTimeout(() => {
            this.setState({clicked:false});
            this.onClickEvent('Checkout');
        }, 150);
    };

    passData=(rowData)=>{
        global.product_id=rowData.entity_id;
        this.onClickEvent('ProductScreen')
        // alert(JSON.stringify(rowData));

        // this.setState({load:true})
            // this.getData();
        // setTimeout(() => {

        // }, 2500);
    };

    _renderRow=(rowData)=>{
        var array = [];
        for (let prop in rowData.custom_option) {
            array.push(rowData.custom_option[prop]);
        }

        let att=array.map(function(item,index){
            return(
                <View key={index} style={styles.customDataView}>
                    <Text style={styles.customDataText}>{item.label} : {item.value}</Text>
                </View>
            )
        })

        return (
            // style={{width:width}}
            <ListItem>
                <View style={styles.productContentView}>
                    <View style={styles.productImgView}>
                        <Image style={styles.productImg} source = {{ uri:rowData.thumbnail_pic_url}}/>
                    </View>
                    <View style={styles.productNameandPriceView}>
                        <View>
                            <Text numberOfLines={2} style={styles.productName}>{rowData.item_title}</Text>
                        </View>
                        {att}
                        <View style={styles.priceView}>
                            <Text style={styles.priceText}>₹ {Math.round(rowData.item_price)}</Text>
                        </View> 
                    </View>

                    <View style={styles.updateItemView}>     
                        <View style={styles.updateItemView1}>
                            <TouchableOpacity onPress={()=> updataAddItem(rowData)}>
                                <Text style={styles.plus}> + </Text>
                            </TouchableOpacity>
                            <Item rounded style={styles.qtyView}>
                                <Text style={styles.qtyText}>{rowData.qty}</Text>
                            </Item>
                            <TouchableOpacity onPress={()=> updataSubItem(rowData)}>
                                <Text style={styles.minus}> - </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ListItem>
        );
    };

    render(){
        const {goBack} = this.props.navigation;
        
        removeItem=(rowData, secId, rowId, rowMap)=>{
            rowMap[`${secId}${rowId}`].props.closeRow();
            var itemId=rowData.cart_item_id;
            Alert.alert(
                'Delete',
                'Do you want to remove this product ?',
                [
                    {
                        text: 'Cancel',
                        onPress: () => {},
                        style: 'cancel'
                    },
                    { text: 'Delete', onPress: () => this.removeProduct(itemId,null)}
                ],
                { cancelable: false }
            );
        };

        editItem=(rowData)=>{
            this.setState({load:true});
            global.product_id=rowData.item_id;
            global.itemId=rowData.cart_item_id
            global.updateQty=rowData.qty;
            // alert(JSON.stringify(rowData))
            global.isUpdate=true;

            if(rowData.product_type=='simple' && rowData.custom_option==''){
                global.product_type=rowData.product_type;
                this.setState({load:false});
                this.onClickEvent('ProductScreen');

            }else if(rowData.product_type=='configurable'){
                global.product_type=rowData.product_type;
                this.setState({load:false});
                this.removeProduct(global.itemId,'ProductScreen');

            }else if(rowData.product_type=='simple' && rowData.custom_option!=''){
                this.setState({load:false});
                global.product_type='custome';
                this.removeProduct(global.itemId,'ProductScreen');
            }
        }
    
        updataAddItem=(rowData)=>{
            let itemId=rowData.cart_item_id;
            var Qty=rowData.qty+1;
            if(Qty<9){
                this.setState({load:true});
                var data='cart['+itemId+'][qty]='+Qty;
                this.updateCart(data);
            }else{
                ToastAndroid.show('Only '+rowData.qty+' units for each customer.',ToastAndroid.SHORT);
            }
        };

        updataSubItem=(rowData)=>{
            let itemId=rowData.cart_item_id;
            var Qty=rowData.qty-1;
            if(Qty>=1){
                this.setState({load:true});
                var data = 'cart['+itemId+'][qty]='+Qty;
                this.updateCart(data);
            }else{
                ToastAndroid.show('Minimum Quantity 1',ToastAndroid.SHORT);
            }
        };

        checkData=(data,rowData)=>{
            var url;
            if(data===false){
                url=urls.wishListAdd+rowData.entity_id

            }else{
                url=urls.wishListDel+rowData.entity_id
            }

            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.code==0&&responseJson.msg!=null){
                    this.setState({checked:true})
                    ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
                }else if(responseJson.code==5) {
                    Alert.alert(
                        'Sign in',
                        'Register with us and enjoy more feature!',
                        [
                            {
                                text: 'May be later',
                                onPress: () => {this.setState({checked:false})},
                                style: 'cancel'
                            },
                            { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
                        ],
                        { cancelable: false }
                    );
                }
            })
        };
        
        return(
            <Container style={styles.container}>
                <ActionBar
                backgroundColor={'#ffffff'}
                containerStyle={styles.actionbarContainer}
                titleContainerStyle={styles.actionbarTitleContainer}
                leftIconName={'back-icon'}
                onLeftPress={() =>goBack()}
                />

                {(this.state.isEmpty)?
                    <View style={styles.mainView}>
                        <Image style={styles.logo} source = {require('../logos/emptycart.png')}/>
                        <Text style={styles.label}>Nothing here</Text>
                        <Text style={styles.labelOffers}>Checkout great products & offers</Text>

                        <View style={{marginTop:10}}>
                            <Button rounded style={styles.button1} onPress={()=>this.onClickEvent('MainScreen')}>
                                <Text style={styles.buttonText1}>Shop Now</Text>
                            </Button>
                        </View>
                    </View>
                    :
                    <ScrollView>
                    {(!this.state.load)?
                        <View>
                            <List
                                dataSource={this.state.cartProductList}
                                renderRow={(rowData) => this._renderRow(rowData)}
                                renderLeftHiddenRow={rowData =>
                                    <Button full style={{backgroundColor:'#201e1f'}} onPress={() => editItem(rowData)}>
                                        <Image source = {require('../logos/edit-icon1.png')} />
                                    </Button>}
                                renderRightHiddenRow={(rowData, secId, rowId, rowMap) =>
                                    <Button full danger onPress={()=>removeItem(rowData, secId, rowId, rowMap)}>
                                        <Image source = {require('../logos/delete-icon1.png')}/>
                                    </Button>}
                                leftOpenValue={100}
                                rightOpenValue={-100}
                            />
                            {((this.state.array).length>0)?
                                <View style={{marginTop:15}}>
                                    <View style={{marginLeft:10}}>
                                        <Text style={{fontSize:responsiveFontSize(2),color:'#efac27',fontWeight:'bold'}}>Similar Products</Text>
                                    </View>
                                    <ListView
                                        horizontal={true}
                                        style={{flex:1,}}
                                        dataSource={this.state.crossSellProducts}
                                        renderRow={(rowData) => 
                                            <TouchableOpacity onPress={()=>this.passData(rowData)}>
                                                <View style={styles.containerCrossSell}>
                                                    <View style={styles.imgView}>
                                                        <Image style={styles.imgSize} source = {{ uri:rowData.image_url}}/>
                                                    </View>
                                                    <View style={styles.nameViewCrossSell}>
                                                        <Text numberOfLines={1} style={styles.nameTextCrossSell}>{rowData.name}</Text>
                                                    </View>
                                                    <View style={styles.priceViewCrossSell}>
                                                        <Text numberOfLines={1} style={styles.priceTextCrossSell}>₹ {Math.round(rowData.price)}</Text>
                                                        <View>
                                                            <CheckBox
                                                                // label='.'
                                                                checked={this.state.checked}
                                                                labelStyle={{color:'white'}}
                                                                uncheckedImage={require('../logos/wish-icon.png')}
                                                                checkedImage={require('../logos/wish-icon1.png')}
                                                                onChange={(data) =>checkData(data,rowData)}
                                                            />
                                                        </View>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        }
                                    />
                                </View>
                                :
                                null
                            }
                        </View>
                        :
                        null
                    }
                    </ScrollView>
                }

                {(!this.state.load && !this.state.isEmpty)?
                    <View>
                        <View style={styles.totalView}>
                            <View style={styles.totalView1}>
                                <Text style={styles.titleText}>Subtotal : </Text>
                                <View style={styles.totalView2}>
                                    <Text style={styles.totalText}>{this.state.subTotal}</Text>
                                </View>
                            </View>
                            <View style={styles.totalView1}>
                                <Text style={styles.titleText}>Discount : </Text>
                                <View style={styles.totalView2}>
                                    <Text style={styles.totalText}>{this.state.discount}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.buttonView}>
                            <View>
                                <Button rounded style={styles.button}  onPress={this.checkOut}>
                                {(this.state.clicked)?
                                    <ActivityIndicator size={responsiveFontSize(3)} color='#efac27'/>    
                                    :
                                    <Text style={styles.buttonText}>PLACE THIS ORDER  |   {this.state.grandTotal}</Text>
                                }               
                                </Button>
                            </View>
                        </View>
                    </View>
                    :
                    null
                }

                <Footer style={{height:responsiveHeight(8.2)}}>
                    <FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>
                </Footer>
                <Loader loading={this.state.load}/>
            </Container>
        );
    }
}