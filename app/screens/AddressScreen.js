import React, { Component } from 'react';
import {View,Text,Image,ScrollView,DrawerLayoutAndroid,ActivityIndicator,Keyboard,TouchableHighlight,StyleSheet,ToolbarAndroid,List,StatusBar,
  ListView,TouchableWithoutFeedback,AsyncStorage,ToastAndroid,Checkbox,Picker,Dimensions} from 'react-native';
import { Container,Card,CardItem, Header,Body, Content, Form,Button,Item,Footer,Input ,Segment,Label,Left,Right } from 'native-base';

import ActionBar from 'react-native-action-bar';

import Loader from './Loader';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import RadioGroup from 'react-native-custom-radio-group';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

import CheckBox from 'react-native-checkbox';

import OrderPlace from './OrderPlace';

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import styles from '../styles/AddressScreen.style';

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
const orderPlace = new OrderPlace();

var format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


var mandatory=(
    <Text style={{color:'#efac27'}}>*</Text>
)

var mandatoryFirstName=mandatory;
var mandatoryLastName=mandatory;
export default class AddressScreen extends Component{
    constructor(props) {
        super(props)
        this.state = {
            email:'',
            bfirstName:'',
            bmiddleName:'',
            blastName:'',
            bcompany:'',
            baddress:'',
            bstreet:'',
            bcity:'',
            bcurrentState:'',
            bcountry:'',
            bcontactNo:'',
            bfax:'',
            bzip:'',
            sfirstName:'',
            smiddleName:'',
            slastName:'',
            scompany:'',
            saddress:'',
            sstreet:'',
            scity:'',
            scurrentState:'',
            scountry:'',
            scontactNo:0,
            sfax:0,
            szip:0,
            value: false,
            isForShipping:0,
            shippingMethods:ds,
            countryList:[],
            stateList:[],
            load:true,
            radioData:[],
            clicked:false,
            shippingLoad:false,
            billingLoad:false,
        }
    }

    componentDidMount() {
        this.getData(); 
    }

    componentWillUnmount(){
        const analytics = new Analytics(urls.AppId, null, { debug: true });
        analytics.hit(new PageHit('CartScreen'));
    }

    getData(){
        if(global.userEmailId!=null){
            this.setState({
                email:global.userEmailId,
                isEnable:false
            });
        }else{
            this.setState({
                isEnable:true
            });
        }

        fetchData.getMaster(urls.getCountry).then((responseJson)=>{
            if(responseJson.model!==null){
                this.setState({
                    countryList:responseJson.model.countries,
                    load:false,
                });
            }
        });
    };

    setAddressFetch(url,data){
        fetchData.postMaster(url,data).then((responseJson)=>{
            if(responseJson.code==0){
                this.getShippingMethod();
            }else if(responseJson.code==1){
                alert('msg : '+responseJson.msg);
                this.setState({
                    load:false,
                    billingLoad:false,
                    shippingLoad:false,
                });
            }
        })
    };

    getShippingMethod(){
        fetchData.getMaster(urls.getShippingMethod).then((responseJson)=>{
            if(responseJson.model!==null){
                var array =[];
                for (let prop in responseJson.model) {
                    array.push(responseJson.model[prop]);
                }
                var array1 =[];
                for (var key in array) {
                    let value = array[key];
                    array1.push(value[0]);
                }
                this.setState({
                    radioData:array1,
                    shippingLoad:false,
                    billingLoad:false,
                    load:false,
                });
            }
        });
    };


    getRegions(country){
        var url=urls.getRegions+country;
        fetchData.getMaster(url).then((responseJson)=>{
             if(responseJson.model!=null && responseJson.code===0){
                this.setState({
                    stateList:responseJson.model.regions,
                    load:false,
                })
            }
        })
    };

    setAddress(){
        Keyboard.dismiss();
            const { bfirstName,bmiddleName,blastName,bcompany,baddress,bstreet,bcity,bcurrentState,bcountry,bzip,bcontactNo,bfax,sfirstName,smiddleName,slastName,scompany,saddress,sstreet,scity,scurrentState,scountry,szip,scontactNo,sfax,email,isForShipping } = this.state

        if(isForShipping==1){
            if(email!='' && bfirstName!='' && blastName!='' && baddress!='' && bstreet!='' && bcity!='' && bcurrentState!='' && bcountry!='' && bzip!='' && bcontactNo!=''){
                if(format.test(email)){
                    var billingData='billing[email]='+email+'&billing[address_id]=null&billing[firstname]='+bfirstName+'&billing[middlename]='+bmiddleName+'&billing[lastname]='+blastName+'&billing[company]='+bcompany+'&billing[street][]='+baddress+'&billing[street][]='+bstreet+'&billing[city]='+bcity+'&billing[region_id]=null&billing[region]='+bcurrentState+'&billing[postcode]='+bzip+'&billing[country_id]='+bcountry+'&billing[telephone]='+bcontactNo+'&billing[fax]='+bfax+'&billing[save_in_address_book]=1&billing[use_for_shipping]='+isForShipping+'&billing_address_id:null';
                    
                    var url=urls.setBillingAddress;
                    this.setState({billingLoad:true,tryAgain:false});
                    this.setAddressFetch(url,billingData);
                }else{
                    this.setState({tryAgain:true});
                    ToastAndroid.show('Invalid Email Id', ToastAndroid.SHORT);
                }
            }else{
                ToastAndroid.show('Address data is Empty', ToastAndroid.SHORT);
                this.setState({load:false});
            }
        }else if(isForShipping==0){

            if(email!='' &bfirstName!='' && blastName!='' && baddress!='' && bstreet!='' && bcity!='' && bcurrentState!='' && bcountry!='' && bzip!='' && bcontactNo!='' &&sfirstName!='' && slastName!='' && saddress!='' && sstreet!='' && scity!='' && scurrentState!='' && scountry!='' && szip!='' && scontactNo!=''){
                if(format.test(email)){
                    var billingUrl = urls.setBillingAddress;
                    var billingData = 'billing[email]='+email+'&billing[address_id]=null&billing[firstname]='+bfirstName+'&billing[middlename]='+bmiddleName+'&billing[lastname]='+blastName+'&billing[company]='+bcompany+'&billing[street][]='+baddress+'&billing[street][]='+bstreet+'&billing[city]='+bcity+'&billing[region_id]=null&billing[region]='+bcurrentState+'&billing[postcode]='+bzip+'&billing[country_id]='+bcountry+'&billing[telephone]='+bcontactNo+'&billing[fax]='+bfax+'&billing[save_in_address_book]=1&billing[use_for_shipping]='+isForShipping+'&billing_address_id:null';
                    this.setState({shippingLoad:true,tryAgainShipping:false});

                    return fetch(billingUrl, {
                        method: 'POST',
                        headers: {
                            'Accept': '*/*',
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                        },
                        body:billingData
                    })
                    .then((response) => response.json())
                    .then((responseData) => {
                        if(responseData.code==0){
                            
                            var shippingUrl=urls.setShippingAddress;
                            var shippingData= 'shipping[email]='+email+'&shipping[address_id]=null&shipping[firstname]='+sfirstName+'&shipping[middlename]='+smiddleName+'&shipping[lastname]='+slastName+'&shipping[company]='+scompany+'&shipping[street][]='+saddress+'&shipping[street][]='+sstreet+'&shipping[city]='+scity+'&shipping[region_id]=null&shipping[region]='+scurrentState+'&shipping[postcode]='+szip+'&shipping[country_id]='+scountry+'&shipping[telephone]='+scontactNo+'&shipping[fax]='+sfax+'&shipping[save_in_address_book]=1&shipping_address_id:null';

                            this.setAddressFetch(shippingUrl,shippingData);

                        }else if(responseData.code===1){
                            alert('msg : '+responseData.msg);
                            this.setState({load:false,});
                        }
                    })
                    .catch((error) => {
                        alert('ErrorMsg s2 : '+error);
                        this.setState({load:false,});
                    });
                }else{
                    this.setState({tryAgainShipping:true});
                    ToastAndroid.show('Invalid Email Id', ToastAndroid.SHORT);
                }
            }else{
                ToastAndroid.show('Address data is Empty', ToastAndroid.SHORT);
                this.setState({load:false,});
            }
        }
    };

    checkBoxData=(data)=>{
        const { email,isForShipping,bfirstName,bmiddleName,blastName,bcompany,baddress,bstreet,bcity,bcurrentState,bcountry,bzip,bcontactNo,bfax} = this.state
        if(data==true){
            this.setState({value:false,isForShipping:0});
            
        }else{
            this.setState({value:true,isForShipping:1});
            setTimeout(() => {
                if(email!='' && bfirstName!='' && blastName!='' && baddress!='' && bstreet!='' && bcity!='' && bcurrentState!='' && bcountry!='' && bzip!='' && bcontactNo!=''){
                    if(format.test(email)){
                        this.setAddress();
                    }else{
                        ToastAndroid.show('Invalid Email Id', ToastAndroid.SHORT);
                    }
                }
            }, 100);
        }
    };

    submitCheck(){
        const { email,isForShipping,bfirstName,bmiddleName,blastName,bcompany,baddress,bstreet,bcity,bcurrentState,bcountry,bzip,bcontactNo,bfax} = this.state
        if(isForShipping==1){
            if(email!='' && bfirstName!='' && blastName!='' && baddress!='' && bstreet!='' && bcity!='' && bcurrentState!='' && bcountry!='' && bzip!='' && bcontactNo!=''){
                if(format.test(email)){
                        this.setAddress();
                }else{
                    ToastAndroid.show('Invalid Email Id', ToastAndroid.SHORT);
                }
            }
        }

        // if(bfirstName!='' && bfirstName!=null ){

        // }
    }

    setShipping=()=>{
        if(this.state.ShippingMethodCode!=null){
            this.setState({clicked:true});
            var data = 'shipping_method='+this.state.ShippingMethodCode;
            fetchData.postMaster(urls.setShippingMethod,data).then((responseJson)=>{
                if(responseJson.code==0){
                    this.setState({clicked:false});
                    // orderPlace.getAddress();
                    this.props.navigation.navigate('PaymentMethod');
                }else{
                    alert('Error : '+responseJson.msg);
                    this.setState({clicked:false});
                }
            });
        }else{
            ToastAndroid.show('ShippingMethod is not selected', ToastAndroid.SHORT);
        }
    }

    render(){
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('AddressScreen'));

        let cDataList=this.state.countryList.map((item,index)=>{
            return(
                <Picker.Item key={index} value={item.country_id} label={item.name} />
            )
        });

        let sDataList=this.state.stateList.map((item,index)=>{
            return(
                <Picker.Item key={index} value={item.region_id} label={item.name}/>
            )
        });  

        currentState=(country)=>{
            this.setState({
                bcountry: country,
                bcurrentState:'',
            });

            this.getRegions(country);
        };

        shippingState=(country)=>{
            this.setState({
                scountry: country,
                scurrentState:'',
            });
            this.getRegions(country);
        };

        passData=(rowData)=>{
            this.setState({
                ShippingMethodCode:rowData.code
            });
            ToastAndroid.show('Selected ShippingMethod :'+rowData.method_title, ToastAndroid.SHORT);
        };

        return(
            <Container style={styles.container}>
                <StatusBar barStyle="dark-content"/>
                <ScrollView>
                    <View style={styles.view1}>
                        <Text style={styles.titleText}>Billing To This Address</Text>
                    </View>
                    <Content style={styles.subContainer}>
                        <View style={styles.rowView}>
                            <Item floatingLabel style={styles.item1}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >First Name {mandatoryFirstName}</Label>
                                <Input
                                    style={styles.dataInput}
                                    value={this.state.bfirstName}
                                    onChangeText={(firstName) => this.setState({bfirstName:firstName})}
                                    onEndEditing={()=>this.submitCheck()}
                                />
                            </Item>

                            <Item floatingLabel style={styles.item2}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >Middle Name</Label>
                                <Input 
                                    style={styles.dataInput}
                                    value={this.state.bmiddleName}
                                    onChangeText={(middleName) => this.setState({bmiddleName:middleName})}
                                />
                            </Item>        
                        </View>

                        <Item floatingLabel style={styles.item1}>
                            <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                >Last Name {mandatoryLastName}</Label>
                            <Input 
                                style={styles.dataInput}
                                value={this.state.blastName} 
                                onChangeText={(lastName) => this.setState({blastName:lastName})}
                                onEndEditing={()=>this.submitCheck()}
                            />
                        </Item>

                        <Item floatingLabel style={styles.item3}>
                            <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                >Email Address {mandatory}</Label>
                            <Input 
                                style={styles.dataInput} 
                                editable={this.state.isEnable}
                                value={this.state.email}
                                keyboardType = 'email-address'
                                onChangeText={(email) => this.setState({email:email})}
                                onEndEditing={()=>this.submitCheck()}
                            />
                        </Item>

                        <Item floatingLabel style={styles.item3}>
                            <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                >Company</Label>
                            <Input 
                                style={styles.dataInput} 
                                value={this.state.bcompany}
                                onChangeText={(company) => this.setState({bcompany:company})}
                            />
                        </Item>

                        <Item floatingLabel style={styles.item5}>
                            <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                >Address {mandatory}</Label>
                            <Input 
                                style={styles.dataInput} 
                                value={this.state.baddress}
                                onChangeText={(address) => this.setState({baddress:address})}
                                onEndEditing={()=>this.submitCheck()}
                            />
                        </Item>

                        <Item floatingLabel style={styles.item5}>
                            <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                >Street Address {mandatory}</Label>
                            <Input 
                                style={styles.dataInput}
                                value={this.state.bstreet} 
                                onChangeText={(street) => this.setState({bstreet:street})}
                                onEndEditing={()=>this.submitCheck()}
                            />
                        </Item>

                        <Item floatingLabel style={styles.item5}>
                            <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                >Contact No {mandatory}</Label>
                            <Input 
                                style={styles.dataInput} 
                                value={this.state.bcontactNo}
                                onChangeText={(contactNo) => this.setState({bcontactNo:contactNo})}
                                maxLength = {10}
                                keyboardType = 'numeric'
                                autoCapitalize={'none'}
                                onEndEditing={()=>this.submitCheck()}
                            />
                        </Item>

                        <View style={styles.rowView}>
                            <View style={styles.view3}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4)}}
                                    >Country {mandatory}</Label>
                                <Picker style={styles.picker}
                                selectedValue={this.state.bcountry}
                                onValueChange={(country) => currentState(country)}>
                                <Picker.Item label="Your country"/>
                                {cDataList}
                                </Picker>
                            </View>

                            <View style={styles.view3}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4)}}
                                    >State {mandatory}</Label>
                                <Picker 
                                    style={styles.picker}
                                    selectedValue={this.state.bcurrentState}
                                    onValueChange={(currentS) => this.setState({bcurrentState:currentS,bcity:''})}>
                                <Picker.Item label="Choose State"/>
                                {sDataList}
                                </Picker>
                            </View>
                        </View>

                        <View style={styles.rowView}>
                            <View style={styles.view3}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4)}}
                                    >City {mandatory}</Label>
                                <Picker 
                                    style={styles.picker}
                                    selectedValue={this.state.bcity}
                                    onValueChange={(itemValue, itemIndex) => this.setState({bcity:itemValue})}>
                                    <Picker.Item label="City" value="City"/>
                                    <Picker.Item label="Mumbai" value="Mumbai"/>
                                    <Picker.Item label="Delhi" value="Delhi"/>
                                    <Picker.Item label="Bangalore" value="Bangalore"/>
                                    <Picker.Item label="Bangalore" value="Bangalore"/>
                                    <Picker.Item label="Ahmedabad" value="Ahmedabad"/>
                                    <Picker.Item label="Chennai" value="Chennai"/>
                                    <Picker.Item label="Kolkata" value="Kolkata"/>
                                    <Picker.Item label="Surat" value="Surat"/>
                                    <Picker.Item label="Pune" value="Pune"/>
                                    <Picker.Item label="Jaipur" value="Jaipur"/>
                                </Picker>
                            </View>

                            <Item floatingLabel style={styles.item6}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >Fax</Label>
                                <Input 
                                    style={styles.dataInput}
                                    value={this.state.bfax}
                                    maxLength = {8}
                                    keyboardType = 'numeric'
                                    onChangeText={(fax) => this.setState({bfax:fax})}
                                />
                            </Item>
                        </View>

                        <Item floatingLabel style={styles.item6}>
                            <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                >Zip Code {mandatory}</Label>
                            <Input 
                                style={styles.dataInput}
                                value={this.state.bzip}
                                maxLength = {8}
                                keyboardType = 'numeric'
                                onChangeText={(zip) => this.setState({bzip:zip})}
                                onEndEditing={()=>this.submitCheck()}

                            />
                        </Item>

                        <View style={{flexDirection:'row'}}>
                            <View  style={styles.checkBox}>
                                <CheckBox
                                    label='Same Address for Shipping?'
                                    labelStyle={{fontSize:responsiveFontSize(1.4)}}
                                    checkboxStyle={{height:responsiveHeight(3),width:responsiveHeight(3)}}
                                    checkedImage={require('../logos/checked.png')}
                                    uncheckedImage={require('../logos/unchecked.png')}
                                    onChange={(checked) =>this.checkBoxData(checked)}
                                    checked={this.state.value}
                                />
                            </View>

                            <View style={{marginLeft:10,marginTop:25,}}>
                                {(this.state.billingLoad)?
                                    <ActivityIndicator size={responsiveFontSize(2.5)} color='#efac27'/>
                                    :
                                    <View>
                                        {(this.state.tryAgain)?
                                            <TouchableWithoutFeedback onPress = {()=>this.submitCheck()}>
                                                <Image style={{width:24,height:24,tintColor:'#efac27'}} source = {require('../logos/refresh-icon.png')}/>
                                            </TouchableWithoutFeedback>
                                            :
                                            null
                                        }
                                    </View>
                                }
                            </View>
                        </View>
                    </Content>

                    {(this.state.isForShipping==0)?
                        <Content style={styles.subContainer}>
                            <View style={styles.view2}>
                                <Text style={styles.titleText}>Shipping On This Address</Text>
                            </View>

                            <View style={styles.rowView}>
                                <Item floatingLabel style={styles.item1}>
                                    <Label 
                                        style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                        >First Name {mandatory}</Label>
                                    <Input 
                                        style={styles.dataInput}
                                        onChangeText={(firstName) => this.setState({sfirstName:firstName})}
                                    />
                                </Item>

                                <Item floatingLabel style={styles.item2}>
                                    <Label 
                                        style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                        >Middle Name</Label>
                                    <Input 
                                        style={styles.dataInput} 
                                        onChangeText={(middleName) => this.setState({smiddleName:middleName})}
                                    />
                                </Item>        
                            </View>

                            <Item floatingLabel style={styles.item1}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >Last Name {mandatory}</Label>
                                <Input 
                                    style={styles.dataInput} 
                                    onChangeText={(lastName) => this.setState({slastName:lastName})}
                                />
                            </Item>

                            <Item floatingLabel style={styles.item3}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >Company</Label>
                                <Input 
                                    style={styles.dataInput} 
                                    onChangeText={(company) => this.setState({scompany:company})}
                                />
                            </Item>

                            <Item floatingLabel style={styles.item5}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >Address {mandatory}</Label>
                                <Input 
                                    style={styles.dataInput} 
                                    onChangeText={(address) => this.setState({saddress:address})}
                                />
                            </Item>

                            <Item floatingLabel style={styles.item5}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >Street Address {mandatory}</Label>
                                <Input 
                                    style={styles.dataInput} 
                                    onChangeText={(street) => this.setState({sstreet:street})}
                                />
                            </Item>

                            <Item floatingLabel style={styles.item5}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >Contact No {mandatory}</Label>
                                <Input 
                                    style={styles.dataInput} 
                                    onChangeText={(contactNo) => this.setState({scontactNo:contactNo})}
                                    maxLength = {10}
                                    keyboardType = 'numeric'
                                    autoCapitalize={'none'}
                                />
                            </Item>

                            <View style={styles.rowView}>
                                <View style={styles.view3}>
                                    <Label 
                                        style={{fontSize:responsiveFontSize(1.4)}}
                                        >Country {mandatory}</Label>
                                    <Picker style={styles.picker}
                                        selectedValue={this.state.scountry}
                                        onValueChange={(country) => shippingState(country)}>
                                        <Picker.Item label="Your country"/>
                                        {cDataList}
                                    </Picker>
                                </View>

                                <View style={styles.view3}>
                                    <Label 
                                        style={{fontSize:responsiveFontSize(1.4)}}
                                        >State {mandatory}</Label>
                                    <Picker style={styles.picker}
                                        selectedValue={this.state.scurrentState}
                                        onValueChange={(currentS) => this.setState({scurrentState:currentS,scity:''})}>
                                        <Picker.Item label="Choose State"/>
                                        {sDataList}
                                    </Picker>
                                </View>
                            </View>

                            <View style={styles.rowView}>
                                <View style={styles.view3}>
                                    <Label 
                                        style={{fontSize:responsiveFontSize(1.4)}}
                                        >City {mandatory}</Label>
                                    <Picker 
                                    style={styles.picker}
                                    onValueChange={(itemValue,itemIndex) => this.setState({scity:itemValue})}
                                    selectedValue={this.state.scity}>
                                        <Picker.Item label="City" value="City"/>
                                        <Picker.Item label="Mumbai" value="Mumbai"/>
                                        <Picker.Item label="Delhi" value="Delhi"/>
                                        <Picker.Item label="Bangalore" value="Bangalore"/>
                                        <Picker.Item label="Bangalore" value="Bangalore"/>
                                        <Picker.Item label="Ahmedabad" value="Ahmedabad"/>
                                        <Picker.Item label="Chennai" value="Chennai"/>
                                        <Picker.Item label="Kolkata" value="Kolkata"/>
                                        <Picker.Item label="Surat" value="Surat"/>
                                        <Picker.Item label="Pune" value="Pune"/>
                                        <Picker.Item label="Jaipur" value="Jaipur"/>
                                    </Picker>
                                </View>

                            <Item floatingLabel style={styles.item6}>
                                <Label 
                                    style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                    >Fax</Label>
                                    <Input 
                                        style={styles.dataInput}
                                        maxLength = {8}
                                        keyboardType = 'numeric'
                                        onChangeText={(fax) => this.setState({sfax:fax})}
                                    />
                                </Item>
                            </View>

                            <View style={{flexDirection:'row'}}>
                                <Item floatingLabel style={styles.item6}>
                                    <Label 
                                        style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}
                                        >Zip Code {mandatory}</Label>
                                    <Input 
                                        style={styles.dataInput}
                                        maxLength = {8}
                                        keyboardType = 'numeric'
                                        onChangeText={(zip) => this.setState({szip:zip})}
                                        onEndEditing={()=>this.setAddress()}
                                    />
                                </Item>


                                <View style={{marginLeft:10,marginTop:45,}}>
                                    {(this.state.shippingLoad)?
                                        <ActivityIndicator size={responsiveFontSize(2.5)} color='#efac27'/>
                                        :
                                        <View>
                                            {(this.state.tryAgainShipping)?
                                                <TouchableWithoutFeedback onPress = {()=>this.submitCheck()}>
                                                    <Image style={{width:24,height:24,tintColor:'#efac27'}} source = {require('../logos/refresh-icon.png')}/>
                                                </TouchableWithoutFeedback>
                                                :
                                                null
                                            }
                                        </View>
                                    }
                                </View>
                            </View>
                        </Content>
                        :
                        null
                    }

                    <View style={{backgroundColor:'#f1f1f1'}}>
                        <Text style={styles.deliveryText}>Delivery</Text>
                        <View style={styles.deliveryView}>
                            <RadioGroup 
                                buttonContainerStyle={StyleSheet.flatten(styles.radioButtonContainer)}
                                buttonTextStyle={StyleSheet.flatten(styles.radioButtonText)}
                                radioGroupList={this.state.radioData}
                                onChange={(selected)=>this.setState({ShippingMethodCode:selected})}
                            />
                        </View>
                    </View>
                </ScrollView>

                <Footer style={styles.footerBackGround}>
                    <TouchableWithoutFeedback onPress={this.setShipping}>
                    {(this.state.clicked)?
                        <ActivityIndicator size={responsiveFontSize(3)} color='#efac27'/>
                        :
                        <View style={styles.footerButton}>
                            <View style={styles.buttonTextView}>
                                <Text style={styles.buttonText}>CONTINUE TO PAYMENT</Text>
                            </View>
                            <View style={styles.view4}>
                                <View style={styles.view5}>
                                    <Image style={styles.image} source = {require('../logos/forward-icon.png')}/>
                                </View>
                            </View>
                        </View>
                    }
                    </TouchableWithoutFeedback>
                </Footer>
                <Loader loading={this.state.load}/>
            </Container>
        );
    }
}