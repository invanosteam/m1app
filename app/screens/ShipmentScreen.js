import React, {Component} from 'react';

import {View,Text,Image,Button,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,
	ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import {Content,Container,Item,Card,CardItem,Body,Thumbnail,Footer} from 'native-base';

import FooterTabBar from './FooterTabBar';

import EmptyShipmentScreen from './EmptyShipmentScreen';


var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});


export default class ShipmentScreen extends Component {
  constructor(){
    super()
    this.state={
      myData:[],
      myProductData:ds,
      activeTabNo:4,
      myOrderData:'',
      subTotal:0,
      shippingAmount:0,
      totalQty:0,
      isEmpty:false,
      count:0,
    }
  }
  componentDidMount() {
    this.getUserOrderInfo();
  }
  async getUserOrderInfo(){
    var cartCount= await AsyncStorage.getItem('count');
    this.setState({count:cartCount});
    // var OrderId= await AsyncStorage.getItem('OrderId');
    var url='http://stageapp.invanos.net/xapi/Order/getShipments/order_id/100000184';
    // +OrderId
    return fetch(url)
    .then((response) => response.json())
    .then((responseJson) => {

      if(responseJson.code===0 && responseJson.msg==='get Shipments success!'){
      // ToastAndroid.show('MO1::'+responseJson.model.shipments[0].increment_id, ToastAndroid.SHORT);

        this.setState({
          isEmpty:false,

          // myProductData:ds.cloneWithRows(responseJson.model.items),
          // myOrderData:responseJson.model,
          // paymentMethod:responseJson.model.pay_method.title,
          // street:responseJson.model.address.shipping_address.street,
          // city:responseJson.model.address.shipping_address.city,
          // region:responseJson.model.address.shipping_address.region,
          // country:responseJson.model.address.shipping_address.country_id,
          // phone:responseJson.model.address.shipping_address.telephone,
          // postcode:responseJson.model.address.shipping_address.postcode,
          totalQty:responseJson.model.shipments[0].total_qty,
          // subTotal:responseJson.model.invoices[0].subtotal,
          // grandTotal:responseJson.model.invoices[0].grand_total,
        }); 
      }else{
        this.setState({isEmpty:true});
      }
    })
    .catch((error) => {
      alert('ErrorMsg :'+error);
    });
  }
  onClick=()=>{
    this.props.navigation.navigate('ProductList');
  };

  render() {
    if(this.state.isEmpty===true){
      return(<EmptyShipmentScreen Click={this.onClick}/>)


    }else if(this.state.isEmpty===false){

      return (
        <Container style={styles.container}>
        <ScrollView>

            <View style={{justifyContent:'center'}}>
            <Text style={{fontSize:20,fontWeight:'bold',color:'#474747',marginBottom:10,marginTop:10,paddingLeft:10}}>Shipment Details</Text>
            <Card>
            <CardItem>
            <View style={{flex:1}}>
              <View style={{flexDirection:'row',flex:1}}>
                <Text style={styles.titleText}>Total Qty : </Text>
                <View style={{flex:1}}>
                  <Text style={{fontSize:18,color:'#201e1f',fontWeight:'bold',textAlign:'right'}}>{this.state.totalQty}</Text>
                </View>
              </View>
            </View>
            </CardItem>
            </Card>
            </View>

        </ScrollView>
           <Footer style={{height:58}}>
            <FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>
          </Footer>
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    width:width,
    height:height,
  },
  imgSize:{
    width: 130,
    height: 130,
    resizeMode: 'contain',
  },
  button: {
    marginBottom: 30,
    width: 180,
    marginLeft:25,
    marginRight:25,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  titleText:{
    fontSize:18,
    color:'#474747',
  },
  buttonText: {
    padding: 10,
    color: 'white'
  },
})