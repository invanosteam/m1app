import React, { Component } from 'react';
import {View,Text,Image,ScrollView,DrawerLayoutAndroid,ActivityIndicator,TouchableHighlight,TouchableWithoutFeedback,StyleSheet,ToolbarAndroid,List,StatusBar,
  ListView,TouchableOpacity,AsyncStorage,ToastAndroid,Checkbox,Picker,Dimensions} from 'react-native';
import { Container, Header,Body, Content, Form,Button, Item,Footer,Input,Segment,Label,Left,Right } from 'native-base';

import Loader from './Loader';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import RadioButton from 'radio-button-react-native';

import RadioGroup from 'react-native-custom-radio-group';

var {height, width} = Dimensions.get('window');

import styles from '../styles/SavedAddressScreen.style'

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class SavedAddressScreen extends Component{
    constructor(props) {
        super(props)
        this.state = {
            email:'',
            firstName:'',
            middleName:'',
            lastName:'',
            company:'',
            address:'',
            street:'',
            city:'',
            currentState:'',
            zip:'',
            country:'',
            contactNo:'',
            fax:'',
            value: true,
            isForShipping:1,
            qty:'',
            addressList:[],
            load:true,
            radioData:[],
            clicked:false,
            deliveryView:false,
        }
    }

    componentDidMount() {
        this.getData();
    }

    getData(){
        this.setState({email:global.userEmailId});

        if(global.userEmailId!=''){

            fetchData.getMaster(urls.getAddressList).then((responseJson)=>{
                if(responseJson.code==0 &&responseJson.model!=''){
                    this.setState({
                        addressList:responseJson.model,
                        load:false,
                    })
                }else if(responseJson.code==5 && responseJson.model==null){
                    this.setState({
                        load:false,
                    });
                }else{
                    this.setState({
                        addressList:[],
                        load:false,
                    });
                }
            });
        }  
    };


    getShippingMethod(){
        fetchData.getMaster(urls.getShippingMethod).then((responseJson)=>{
            if(responseJson.model!==null){
                var array =[];
                for (let prop in responseJson.model) {
                    array.push(responseJson.model[prop]);
                }
                var array1 =[];
                for (var key in array) {
                    let value = array[key];
                    array1.push(value[0]);
                }
                this.setState({
                    radioData:array1,
                    load:false,
                    deliveryView:true,
                });
            }
        });
    };

    _isForShipping=(data)=>{
        this.setState({isForShipping:data});
    };

    setShipping=()=>{
        this.setState({clicked:true,});
        var data = 'shipping_method='+this.state.ShippingMethodCode;

        fetchData.postMaster(urls.setShippingMethod,data).then((responseJson)=>{
            if(responseJson.code===0){
                this.setState({clicked:false,});
                this.props.navigation.navigate('PaymentMethod');

            }else{
                alert('Error :'+responseJson.msg);
                this.setState({clicked:false,});
            }
        });
    };

    setAddress=()=>{
        const { firstName,middleName,lastName,company,address,street,city,currentState,country,zip,contactNo,fax,email,isForShipping,addressId} = this.state

        if(email!=''&&firstName!='' && lastName!='' && address!='' && street!='' && city!='' && currentState!='' && country!='' && zip!='' && contactNo!=''&& addressId!=undefined){

            this.setState({load:true,});

            var url = urls.setBillingAddress;

            var data = 'billing[email]='+email+'&billing[address_id]='+addressId+'&billing[firstname]='+firstName+'&billing[lastname]='+lastName+'&billing[company]='+company+'&billing[street][]='+address+'&billing[street][]='+street+'&billing[city]='+city+'&billing[region_id]=null&billing[region]='+currentState+'&billing[postcode]='+zip+'&billing[country_id]='+country+'&billing[telephone]='+contactNo+'&billing[fax]='+fax+'&billing[save_in_address_book]=1&billing[use_for_shipping]='+isForShipping+'&billing_address_id:null';

            fetchData.postMaster(url,data).then((responseJson)=>{
                if(responseJson.code==0){
                    this.getShippingMethod();
                }else if(responseJson.code===1){
                    this.setState({load:false,});
                    alert('errormsg : '+responseJson.msg);
                }
            })
        }else{
            ToastAndroid.show('Please select billing address', ToastAndroid.SHORT);
            this.setState({load:false,});
        }
    };

    setBillingShippingAddress=()=>{
        const { firstName,middleName,lastName,company,address,street,city,currentState,country,zip,contactNo,fax,sfirstName,smiddleName,slastName,scompany,saddress,sstreet,scity,scurrentState,scountry,szip,scontactNo,sfax,email,isForShipping,addressId,saddressId } = this.state

        if(email!=''&&firstName!='' && lastName!='' && address!='' && street!='' && city!='' && currentState!='' && country!='' && zip!='' && contactNo!=''&& addressId!=undefined && sfirstName!='' && slastName!='' && saddress!='' && sstreet!='' && scity!='' && scurrentState!='' && scountry!='' && szip!='' && scontactNo!=''&& saddressId!=undefined ){

            this.setState({load:true,});

            var billingUrl=urls.setBillingAddress

            var billingData='billing[email]='+email+'&billing[address_id]='+addressId+'&billing[firstname]='+firstName+'&billing[lastname]='+lastName+'&billing[company]='+company+'&billing[street][]='+address+'&billing[street][]='+street+'&billing[city]='+city+'&billing[region_id]=null&billing[region]='+currentState+'&billing[postcode]='+zip+'&billing[country_id]='+country+'&billing[telephone]='+contactNo+'&billing[fax]='+fax+'&billing[save_in_address_book]=1&billing[use_for_shipping]='+isForShipping+'&billing_address_id:null';



            return fetch(billingUrl, {
                method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                body:billingData
            })
            .then((response) => response.json())
            .then((responseJson) => {
                if(responseJson.code==0){
                
                    var shippingUrl=urls.setShippingAddress;
                    var shippingData='shipping[email]='+email+'&shipping[address_id]='+saddressId+'&shipping[firstname]='+sfirstName+'&shipping[middlename]='+smiddleName+'&shipping[lastname]='+slastName+'&shipping[company]='+scompany+'&shipping[street][]='+saddress+'&shipping[street][]='+sstreet+'&shipping[city]='+scity+'&shipping[region_id]=null&shipping[region]='+scurrentState+'&shipping[postcode]='+szip+'&shipping[country_id]='+scountry+'&shipping[telephone]='+scontactNo+'&shipping[fax]='+sfax+'&shipping[save_in_address_book]=1&shipping_address_id:null';

                    fetchData.postMaster(shippingUrl,shippingData).then((responseJson)=>{
                        if(responseJson.code===0){
                            this.getShippingMethod();
                        }else if(responseJson.code===1){
                            alert('msg : '+responseJson.msg);
                            this.setState({load:false,});
                        }
                    });

                }else if(responseJson.code===1){
                    alert('msg : '+responseJson.msg);
                    this.setState({load:false,});
                }
            })
            .catch((error) => {
                alert('ErrorMsg s2 : '+error);
                this.setState({load:false,});
            });
        }else{
            ToastAndroid.show('Please select billing and shipping address', ToastAndroid.SHORT);
            this.setState({load:false,});
        }
    };

    continue=()=>{
        if(this.state.isForShipping===1){
            this.setAddress();

        }else if(this.state.isForShipping===0){            
            this.setBillingShippingAddress();

        }
    };

    render(){
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('SavedAddressScreen'));

        let aDataList=this.state.addressList.map((item,index)=>{
            return(
                <Picker.Item key={index} value={item.entity_id} label={item.name+','+item.street[0]+','+item.street[1]+','+item.city+','+item.region+','+item.postcode} />
            )
        });

        selectAddress=(id)=>{
            if(id!=undefined){
                this.setState({addressId:id});
            }

            var url=urls.getSingleAddress+id;
            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.model!==null && responseJson.code!==2){
                    var array = [];
                    for (let prop in responseJson.model.street) {
                      array.push(responseJson.model.street[prop]);
                    }
                    this.setState({
                        firstName:responseJson.model.firstname,
                        lastName:responseJson.model.lastname,
                        company:responseJson.model.company,
                        address:array[0],
                        street:array[1],
                        city:responseJson.model.city,
                        currentState:responseJson.model.region,
                        zip:responseJson.model.postcode,
                        country:responseJson.model.country,
                        contactNo:responseJson.model.telephone,
                        fax:responseJson.model.fax,
                        load:false,
                    })
                } 
            });
        };

        selectShippingAddress=(id)=>{
            if(id!=undefined){
                this.setState({saddressId:id});
                // alert('id : '+id);
            }

            var url=urls.getSingleAddress+id;
            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.model!==null && responseJson.code!==2){
                    var array = [];
                    for (let prop in responseJson.model.street) {
                      array.push(responseJson.model.street[prop]);
                    }
                    this.setState({
                        sfirstName:responseJson.model.firstname,
                        slastName:responseJson.model.lastname,
                        scompany:responseJson.model.company,
                        saddress:array[0],
                        sstreet:array[1],
                        scity:responseJson.model.city,
                        scurrentState:responseJson.model.region,
                        szip:responseJson.model.postcode,
                        scountry:responseJson.model.country,
                        scontactNo:responseJson.model.telephone,
                        sfax:responseJson.model.fax,
                        load:false,
                    })
                } 
            });
        };

        return(
            <Container>
                <StatusBar barStyle="dark-content"/>
                {(global.userEmailId!=null)?
                    <Content style={styles.container}>   
                        <View style={styles.view5}>
                            <View style={styles.view1}>
                                <Label>Select Billing Address </Label>
                                <Picker 
                                    style={styles.picker}
                                    selectedValue={this.state.addressId}
                                    onValueChange={(id) => selectAddress(id)}>
                                    <Picker.Item label="Choose billing address"/>
                                    {aDataList}
                                </Picker>
                            </View>
                        </View>

                        {(this.state.isForShipping===0)?
                            <View style={styles.view5}>
                                <View style={styles.view1}>
                                    <Label>Select Shipping Address </Label>
                                    <Picker 
                                        style={styles.picker}
                                        selectedValue={this.state.saddressId}
                                        onValueChange={(id) => selectShippingAddress(id)}>
                                        <Picker.Item label="Choose shipping address"/>
                                        {aDataList}
                                    </Picker>
                                </View>
                            </View>
                            :
                            null
                        }

                        <View style={styles.view1}>
                            <RadioButton 
                                currentValue={this.state.isForShipping} 
                                value={1} 
                                onPress={(value)=>this._isForShipping(value)}>
                                <Text style={styles.text1}>Ship to this address</Text>
                            </RadioButton>
                        </View>

                        <View style={styles.view1} >
                            <RadioButton 
                                currentValue={this.state.isForShipping} 
                                value={0} 
                                onPress={(value)=>this._isForShipping(value)}>
                                <Text style={styles.text1}>Ship to different address</Text>
                            </RadioButton>
                        </View>

                        <View style={styles.buttonView}>
                            <View>
                                <Button rounded style={styles.button}  onPress={this.continue}>
                                    <Text style={styles.buttonText}>Continue</Text>
                                </Button>
                            </View>
                        </View>

                        {(this.state.deliveryView)?
                            <View style={styles.view2}>
                                <Text style={styles.deliveryText}>Delivery</Text>
                                <View style={styles.deliveryView}>
                                    <RadioGroup 
                                        radioGroupList={this.state.radioData}
                                        onChange={(selected)=>this.setState({ShippingMethodCode:selected})}
                                    />
                                </View>
                            </View>
                            :
                            null
                        }
                    </Content>
                    :
                    <View style={styles.mainView}>
                        <Text style={styles.label}>Login with us to use this feature</Text>
                    </View>
                }

                <Footer style={styles.footerBackGround}>
                {(global.userEmailId!=null)?
                    <TouchableWithoutFeedback onPress={this.setShipping}>
                    {(this.state.clicked)?
                        <ActivityIndicator size={30} color='#efac27'/>
                        :
                        <View style={styles.footerButton}>
                            <View style={styles.buttonTextView}>
                                <Text style={styles.buttonText}>CONTINUE TO PAYMENT</Text>
                            </View>
                            <View style={styles.view3}>
                                <View style={styles.view4}>
                                    <Image style={styles.image} source = {require('../logos/forward-icon.png')}/>
                                </View>
                            </View>
                        </View>
                    }
                    </TouchableWithoutFeedback>
                    :
                    <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Login')}>
                        <View style={styles.footerButton}>
                            <View style={styles.buttonTextView}>
                                <Text style={styles.buttonText}>Login</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                }
                </Footer>
                <Loader loading={this.state.load}/>
            </Container>
        );
    }
}