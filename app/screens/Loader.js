import React, { Component } from 'react';
import { ActivityIndicator, View, StyleSheet } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import SleekLoadingIndicator from 'react-native-sleek-loading-indicator';

export default class Loader extends Component {
    render() {
        return (
            <View style={styles.container}>
            {(this.props.loading)?
                <View style={{width:responsiveHeight(8),height:responsiveHeight(8),justifyContent:'center',alignItems:'center',backgroundColor:'#201e1f',borderRadius:10,}}>
                    <ActivityIndicator
                        animating={this.props.loading}
                        size={responsiveFontSize(4)}
                        color="#efac27"
                    />
                </View>
                :
                null
            }
            </View>
        )
    }
}


const styles = StyleSheet.create ({
    container: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#ffffff',
    },
})