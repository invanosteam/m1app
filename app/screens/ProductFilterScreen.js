import React ,{Component} from 'react';
import {View,Text,ToastAndroid,ScrollView,Button,AsyncStorage,Dimensions,TouchableWithoutFeedback,TouchableHighlight,StyleSheet } from 'react-native';
import {Container,Footer} from 'native-base';
import ActionBar from 'react-native-action-bar';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


import RadioGroup from 'react-native-custom-radio-group1';
import SelectMultiple from 'react-native-select-multiple';

import Loader from './Loader';

import Slider from 'react-native-slider';

import urls from '../config/urls';

import styles from '../styles/ProductFilterScreen.style';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class ProductFilterScreen extends Component{
    constructor(){
        super()
        this.state={
            load:true,
            radioData:[],
            checkData:[],
            checkedValue:[],
            value: 0,
            selectedPrice:5000,
            selectedColor:'',
            selectedSize:'',
        }
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('ProductFilterScreen'));
    }

    componentDidMount() {
        this.getData();
    }

    getData(){
        var priceArray=[];
        var colorArray=[];
        var sizeArray=[];
        var url=urls.productFilter+global.category_id;


        fetchData.getMaster(url).then((responseJson)=>{
            if(responseJson!=null && responseJson.length>0){
                for (var i = 0; i < responseJson.length; i++){
                    if(responseJson[i].attribute_code=='price'){
                        var price=[]
                        for (let prop in responseJson[i].attribute_option) {
                            price.push(responseJson[i].attribute_option[prop]);
                            for (var p = 0; p < price.length; p++) {
                                priceArray[p]={label:price[p].label,value:price[p].value}
                            }
                        }
                    }

                    if(responseJson[i].attribute_code=='color'){
                        var color=[]
                        for (let prop in responseJson[i].attribute_option) {
                            color.push(responseJson[i].attribute_option[prop]);
                            for (var c = 0; c < color.length; c++) {
                                colorArray[c]={label:color[c].label,value:color[c].value}
                            }
                            if(color.length > 0){
                                this.setState({
                                    radioData:colorArray,
                                    colorFilter:'Yes'
                                });
                            }
                        }
                    }

                    if(responseJson[i].attribute_code=='size'){
                        var size=[]
                        for (let prop in responseJson[i].attribute_option) {
                            size.push(responseJson[i].attribute_option[prop]);
                            for (var s = 0; s < size.length; s++) {
                                sizeArray[s]={label:size[s].label,value:size[s].value}
                            }
                            if(size.length > 0){
                                this.setState({
                                    checkData:sizeArray,
                                    sizeFiltet:'Yes',
                                });
                            }
                        }
                    }
                }
            } 
            this.setState({
                load:false,
            });
        });
    }

    applyFilter=()=>{
        const {selectedColor,selectedPrice,selectedSize}=this.state

        var filterUrl=urls.getFilteredProduct+global.category_id;

        if(selectedPrice!='' && selectedPrice!=null){
            filterUrl+='/price/-'+selectedPrice;
        }

        if(selectedSize!='' && selectedSize!=null ) {
            filterUrl+='/size/'+selectedSize;
        }

        if(selectedColor!=''&& selectedColor!=null ){
            filterUrl+='/color/'+selectedColor;
        }

        global.filterUrl=filterUrl
        this.props.navigation.navigate('ProductList');
    };

    sliderChange=(value)=> {
        this.setState({
            value:value,
            selectedPrice:Math.round(value),
        });
    };

    onSelectionsChangeCheckBox = (checkedValue) => {
        this.setState({checkedValue});
        multiselect=[]
        if(checkedValue!=''){
            for (var i = 0; i < checkedValue.length; i++) {
                multiselect.push(checkedValue[i].value);
            } 
        }
        let multipleselectData=multiselect.join('');
        this.setState({selectedSize:multipleselectData});
    };

    configSelect=(selected)=>{
        this.setState({selectedColor:selected});
    };

    clearClicke=()=>{
        this.setState({
            value: 0,
            selectedPrice:5000,
            selectedColor:'',
            checkedValue:[],
        });

    }

    render(){
        const {goBack} = this.props.navigation;

        return(
            <Container style={styles.container}>
                <ActionBar
                backgroundColor={'#ffffff'}
                containerStyle={styles.actionBarContainer}
                titleContainerStyle={styles.titleContainerStyle}
                leftIconName={'back-icon'}
                onLeftPress={() =>  goBack()}
                rightIcons={[
                    {
                        name: 'close-icon',
                        onPress: () => this.clearClicke(),
                    },
                ]}

                />
                <View style={{flex:1}}>
                    <View style={styles.view1}>
                        <Text style={styles.text1}>Filter by Price</Text>
                        <Slider
                            value={this.state.value}
                            minimumTrackTintColor={'#efac27'}
                            maximumTrackTintColor={'#f1f1f1'}
                            thumbTintColor={'#efac27'}
                            minimumValue={0}
                            maximumValue={5000}
                            onValueChange={(value) =>this.sliderChange(value)}/>

                        <View style={styles.view2}>
                            <Text style={styles.text3}>Min Price</Text>
                            <Text style={styles.text3}>Max Price</Text>
                        </View>
                        <View style={styles.view3}>
                            <Text style={styles.text2}>Min</Text>
                            <Text style={styles.text2}>₹ {this.state.selectedPrice}</Text>
                        </View>
                    </View>


                    {(this.state.colorFilter==='Yes')?
                        <View style={styles.view4}>
                            <Text style={styles.text1}>Filter by Color</Text>
                            <RadioGroup
                                buttonContainerStyle={styles.radioButtonContainer}
                                buttonTextStyle={styles.radioButtonText}
                                radioGroupList={this.state.radioData}
                                onChange={(selected)=> this.configSelect(selected)}
                            />
                        </View>
                        :
                        null
                    }

                    {(this.state.sizeFiltet==='Yes')?
                        <View style={styles.view5}>
                            <Text style={styles.text1}>Filter by Size</Text>

                            <SelectMultiple
                                checkboxStyle={styles.checkbox}
                                labelStyle={styles.checkboxText}
                                items={this.state.checkData}
                                selectedItems={this.state.checkedValue}
                                onSelectionsChange={(value)=>this.onSelectionsChangeCheckBox(value)}
                            />
                        </View>
                        :
                        null
                    }

                </View>
                <Footer style={styles.footer}>
                    <TouchableWithoutFeedback onPress={this.applyFilter}>
                        <View style={styles.footerbutton}>
                            <Text style={styles.buttonText}>Apply</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </Footer>
                <Loader loading={this.state.load}/>
            </Container>
        );
    }
}