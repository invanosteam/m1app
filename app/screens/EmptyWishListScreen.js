import React, {Component} from 'react';

import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import { Container, Header, Content, Form,Button,Icon,Item,Input,Footer,Label,Left,Right } from 'native-base';

import FooterTabBar from './FooterTabBar';

var {height, width} = Dimensions.get('window');

import styles from '../styles/EmptyWishListScreen.style';

export default class EmptyWishListScreen extends Component {
    constructor(){
        super()
        this.state={
            activeTabNo:4,
        }
    }

	onClick=()=>{
        this.props.Click();
	};
  
    render() {
        return (
            <Container style={styles.container}>
                <View style={styles.mainView}>
                    <Image style={styles.image} source = {require('../logos/empty-wishlist.png')}/>
                    <Text style={styles.label}>Your wishlist is empty!</Text>
                    <Text style={styles.labelOffers}>Simply browse and tap on the heart icon </Text>
                    <View style={styles.view1}>
                        <Button rounded style={styles.button} onPress={this.onClick}>
                            <Text style={styles.buttonText}>Continue Shopping</Text>
                        </Button>
                    </View>
                </View>
                <Footer style={styles.footer}>
                    <FooterTabBar activeTabNo={this.state.activeTabNo}/>
                </Footer>
            </Container>
        );
    }
}