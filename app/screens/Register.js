import React, { Component } from 'react';
import { ToastAndroid,StyleSheet,Text,ActivityIndicator,TextInput,Image,TouchableOpacity,AsyncStorage,ToolbarAndroid,Dimensions,View } from 'react-native';
import { Container, Header, Content, Button,Form, Item, Input, Label,Left,Right } from 'native-base';

import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import FBSDK, {LoginManager,AccessToken } from 'react-native-fbsdk';

import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import AppButton from '../components/Button/AppButton';

var {height, width} = Dimensions.get('window');

import styles from '../styles/Register.style';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName:'',
            lastName:'',
            EmailId: '',
            password:'',
            confirmPassword:'',
            contactNo:0,
            gender:'',
            clicked:false,
        }
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('Register'));
    }

    // alert(''+firstName+lastName+EmailId+password+confirmPassword+contactNo+gender);
    onSubmitClick=()=> {
        const { firstName, lastName, EmailId,password,confirmPassword,contactNo,gender } = this.state
        var format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(format.test(EmailId) && firstName!= '' && lastName!='' && password!='' && confirmPassword!='' && contactNo!=''){
            if(confirmPassword===password){
            this.setState({clicked:true});
                var data='pwd='+password+'&email='+EmailId+'&firstname='+firstName+'&lastname='+lastName+'&gender='+gender+'&default_mobile_number='+contactNo;
                
                fetchData.postMaster(urls.register,data).then((responseJson)=>{
                // alert(JSON.stringify(responseJson))
                    if(responseJson.code==0){
                        if(responseJson.model.email!= null){
                            try {
                                AsyncStorage.setItem('EmailId',responseJson.model.email);
                                this.props.navigation.navigate('MainScreen');
                            } catch (error) {
                                alert('Error msg R0'+error);
                                this.setState({clicked:false});
                            }
                        }else{
                            this.setState({clicked:false});
                            ToastAndroid.show('Email address already exist please login to procced', ToastAndroid.SHORT);
                            // this.props.navigation.navigate('MainScreen');
                        }
                    }else{
                        this.setState({clicked:false});
                        ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                    }
                });
            }else{
                ToastAndroid.show('Password should be the Same',ToastAndroid.SHORT)
            }
        }else{
            ToastAndroid.show('Customer data required',ToastAndroid.SHORT)
        }
    };

    onSelect(index, value){
        this.setState({
            gender:value
        })
    };

    oneStepLogin=(emailId,firstname,lastname,g)=>{
        var password = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 8; i++){
            password += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        gender=''
        if(g=='male'){
            gender=1;
        }else{
            gender=2;
        }
        // alert('fbData :: '+emailId,firstname,lastname,gender);

        var format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(format.test(emailId) && firstname!= '' && lastname!='' && gender!='' && password!=''){

            var data='pwd='+password+'&email='+emailId+'&firstname='+firstname+'&lastname='+lastname;
            fetchData.postMaster(urls.register,data).then((responseJson)=>{
                if(responseJson.code==0){
                    // alert('DATA : '+JSON.stringify(responseJson))
                    if(responseJson.model[0]!= null && responseJson.model[0]!= ''){
                        global.userEmailId=responseJson.model.email;
                        try {
                            AsyncStorage.setItem('EmailId',responseJson.model.email);
                            this.getCartItemCount();
                            this.onClickEvent('MainScreen');
                        } catch (error) {
                            ToastAndroid.show('Error Msg'+error,ToastAndroid.SHORT);
                        }
                    }else{
                        ToastAndroid.show('Email address already exist please login to procced', ToastAndroid.SHORT);
                        this.onClickEvent('Login');
                    }
                }else{
                    // alert('data : '+JSON.stringify(responseJson))
                    fetchData.postMaster(urls.facbookLogin,JSON.stringify(data)).then((responseJson)=>{
                        if(responseJson.code==0){
                            this.getCustomerInfo();
                            this.getCartItemCount();
                            this.onClickEvent('MainScreen');
                        }else{
                            ToastAndroid.show('Invalid User', ToastAndroid.SHORT);
                        }
                    });
                }
            });
        }else{
            ToastAndroid.show('please try again ',ToastAndroid.SHORT)
        }
    };


    onFBClick=()=>{
        var that = this;
        LoginManager.logInWithReadPermissions(['email']).then(function(result){
            if(!result.isCancelled){
                AccessToken.getCurrentAccessToken().then((data) => {
                    var url=urls.facebookUrl+data.accessToken;
                    fetchData.getMaster(url).then((responseJson)=>{
                        // alert(JSON.stringify(responseJson))
                        if(JSON.stringify(responseJson)!='{}' && responseJson!=null){
                            var e=responseJson.email;
                            var fn=responseJson.first_name;
                            var ln=responseJson.last_name;
                            // var g=responseJson.gender;
    
                            that.oneStepLogin(e,fn,ln,null);
                        }
                    })
                });
            }
        },function(error){
            ToastAndroid.show('Something went wrong please try again !', ToastAndroid.SHORT); 
        });
    };

    onGoogleClick=()=>{
        GoogleSignin.hasPlayServices({ autoResolve: true });
        GoogleSignin.configure({
            scopes: [urls.googleUrl], 
            webClientId:urls.googleClientId,
        }).then((user)=>{
            GoogleSignin.signIn()
            .then((user) => {
                // alert(JSON.stringify(user))
                var e=user.email;
                // var fn=responseJson.first_name;
                // var ln=responseJson.last_name;
                ToastAndroid.show('Email Id :' +e, ToastAndroid.SHORT);
                this.oneStepLogin(e,null,null,null);
            })
            .catch((err) => {
                alert('Google Error :'+err);
                ToastAndroid.show('Something went wrong please try again later!', ToastAndroid.SHORT); 
            })
            .done();  
        })
        .catch((err) => {
            ToastAndroid.show('Please try again' + err, ToastAndroid.SHORT);
            // alert('Google Error :'+err.code+','+err.message);
        })
    };

    onSocialClick=()=>{
        // GoogleSignin.signOut();
        ToastAndroid.show('This feature is underdevlopment...!', ToastAndroid.SHORT);
    };

    render() {
        return (
            <Container style={styles.container}>
                <Content>
                    <Form>
                        <Text style={styles.title}>Sign up</Text>
                        <View style={styles.inputView}>
                            <Image style={styles.inputIcon} source = {require('../logos/user-icon.png')}/>
                            <Item floatingLabel style={styles.textArea}>
                                <Label style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1),color:'#efac27'}}>First Name
                                </Label>
                                <Input style={styles.inputLabel}
                                onChangeText={(firstName) => this.setState({firstName:firstName})}/>
                            </Item>
                        </View>

                        <View style={styles.inputView}>
                            <Image style={styles.inputIcon} source = {require('../logos/user-icon.png')}/>
                            <Item floatingLabel style={styles.textArea}>
                                <Label style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1),color:'#efac27'}}>Last Name
                                </Label>
                                <Input style={styles.inputLabel}
                                onChangeText={(lastName) => this.setState({lastName:lastName})}
                                />
                            </Item>
                        </View>

                        <View style={styles.inputView}>
                            <Image style={styles.inputIcon} source = {require('../logos/mail-icon.png')}/>
                            <Item floatingLabel style={styles.textArea}>
                                <Label style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1),color:'#efac27'}}>Email Address
                                </Label>
                                <Input style={styles.inputLabel}
                                onChangeText={(EmailId) => this.setState({EmailId:EmailId})}
                                />
                            </Item>
                        </View>

                        <View style={styles.inputView}>
                            <Image style={styles.inputIcon} source = {require('../logos/lock-icon.png')}/>
                            <Item floatingLabel style={styles.textArea}>
                                <Label style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1),color:'#efac27'}}>Password
                                </Label>
                                <Input style={styles.inputLabel}
                                onChangeText={(password) => this.setState({password:password})}
                                maxLength = {8}
                                secureTextEntry={true}
                                />
                            </Item>     
                        </View>
                        
                        <View style={styles.inputView}>
                            <Image style={styles.inputIcon} source = {require('../logos/lock-icon.png')}/>
                            <Item floatingLabel style={styles.textArea}>
                                <Label style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1),color:'#efac27'}}>Confirm Password
                                </Label>
                                <Input style={styles.inputLabel}
                                onChangeText={(confirmPassword) => this.setState({confirmPassword:confirmPassword})}
                                maxLength = {8}
                                secureTextEntry={true}
                                />
                            </Item>
                        </View>
                
                        <View style={styles.inputView}>
                            <Image style={[styles.inputIcon,{resizeMode: 'contain',}]} source = {require('../logos/phone-icon.png')}/>
                            <Item floatingLabel style={styles.textArea}>
                                <Label style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1),color:'#efac27'}}>Contact No
                                </Label>
                                <Input style={styles.inputLabel}
                                onChangeText={(contactNo) => this.setState({contactNo:contactNo})}
                                maxLength = {10}
                                keyboardType = 'numeric'
                                autoCapitalize={'none'}
                                />
                            </Item>
                        </View>

                        <View style={styles.buttonView}>
                            <View>
                                <AppButton buttonTitle={'REGISTER'} clicked={this.state.clicked} onPress={this.onSubmitClick}/>
                            </View>
                        </View>
                    </Form>
                </Content>
        

                <View style={styles.socialIconView}>
                    <View style={styles.socialLabelView}>
                        <Text style={styles.socialLabel}>Or sign up with</Text>
                    </View>
                  
                    <TouchableOpacity onPress={this.onFBClick}>
                        <View style={{marginRight:50}}>
                        <Image style={styles.socialIcon} source = {require('../logos/facebook-icon.png')}/>
                        </View>
                    </TouchableOpacity>
                      
                    <TouchableOpacity onPress={this.onSocialClick}>
                        <View style={{marginRight:50}}>
                            <Image style={styles.socialIcon} source = {require('../logos/twitter-icon.png')}/>
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={this.onGoogleClick}>
                    <View style={{marginRight:10,}}>
                        <Image style={styles.socialIcon} source = {require('../logos/gplus-icon1.png')}/>
                    </View>
                    </TouchableOpacity>
                </View>
            </Container>
        );
    }
}
                        // <View style={styles.inputView}>
                        //     <Image style={[styles.inputIcon,{marginTop:10,resizeMode: 'contain',}]} source = {require('../logos/gender-icon1.png')}/>
                        //     <Item style={styles.textArea}>
                        //         <View style={styles.genderView}>
                        //         <Text style={styles.gender}>Gender</Text>
                        //             <RadioGroup
                        //             style={{flexDirection:'row'}}
                        //             size={responsiveFontSize(2)}
                        //             thickness={2}
                        //             color='#a2a2a2'
                        //             onSelect = {(index, value) => this.onSelect(index, value)}>
                        //                 <RadioButton 
                        //                 value='1'
                        //                 // color='#a2a2a2'
                        //                 >
                        //                     <Text style={styles.genderText}>Male</Text>
                        //                 </RadioButton>
                                       
                        //                 <RadioButton 
                        //                 value='2'
                        //                 // color='#a2a2a2'
                        //                 >
                        //                     <Text style={styles.genderText}>Female</Text>
                        //                 </RadioButton>
                        //             </RadioGroup>
                        //         </View>
                        //     </Item>      
                        // </View>