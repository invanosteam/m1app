import React, {Component} from 'react';
import {Text,View,Button,BackHandler,Alert,StyleSheet,ActivityIndicator,Image,TouchableHighlight,TouchableWithoutFeedback,StatusBar,AsyncStorage,ToastAndroid,ScrollView,Dimensions} from 'react-native';
import {Content,Container,Item,Card,Footer,CardItem,Body,Thumbnail} from 'native-base';

import ImageSlider from 'react-native-image-slider';

import Swiper from 'react-native-swiper';

import urls from '../config/urls';

import color from '../styles/colors';

import styles from '../styles/WelcomeScreen.style';

import staticContent from '../config/staticContent';

import { NavigationActions } from 'react-navigation';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

import FetchData from './FetchData';

const fetchData = new FetchData();

const MainScreen=fetchData.resetIndex('MainScreen');
export default class WelcomeScreen extends Component {
    constructor(){
        super()
        this.state={
            loginskip:'',
            clicked:false,
        }
        this.handleBackButton = this.handleBackButton.bind(this);
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId, null, { debug: true });
        analytics.hit(new PageHit('WelcomeScreen'));
        analytics.addCustomDimension();
    }

    componentDidMount() {
        this.getData();
        fetchData.initGlobal();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton); 
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    };  

    handleBackButton=()=>{
        Alert.alert(
            'Quiting',
            'Want to Exit?',
            [
                {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel'
                },
                { text: 'Exit', onPress: () => BackHandler.exitApp()}
            ],
            { cancelable: false }
        );
        return true;
    }

    async getData(){
        var skip= await AsyncStorage.getItem('Skip');
        this.setState({loginskip:skip});
    };

    getStarted=()=>{
        this.setState({clicked:true});
        
        if(this.state.loginskip=='yes'){
            fetchData.getMaster(urls.getCartInfo).then((responseJson)=>{
                if(responseJson.code==1){
                    global.cartItemCount=responseJson.model.cart_items_count;
                }
                this.setState({clicked:false});
                this.props.navigation.dispatch(MainScreen);
            });
        }else {
            this.setState({clicked:false});
            this.props.navigation.navigate('Login');
        }
    };

    render() {
        return (
            <Container style={styles.container}>
                <View style={styles.view1}>
                    <View style={styles.img}>
                        <Swiper 
                        dot={<View style={[styles.view2,styles.dotColor]}/>}
                        activeDot={<View style={[styles.view2,styles.activeDotColor]}/>}
                        loop={false}>
                            <View>
                                <Image style={styles.img} source = {{ uri:urls.welcomeBannerImages}}/>
                            </View>
                            <View>
                                <Image style={styles.img} source = {{ uri:urls.welcomeBannerImages}}/>
                            </View>
                            <View>
                                <Image style={styles.img} source = {{ uri:urls.welcomeBannerImages}}/>
                            </View>
                        </Swiper>
                    </View>

                    <View style={styles.view4}>
                        <View style={styles.descriptionLabelView}>
                            <Text style={styles.descriptionLabel}>Welcome</Text>
                                <View style={styles.borderBottom}>
                                    <Text style={styles.descriptionLabel}>to </Text>
                                </View>
                            <Text style={styles.boldDescriptionLabel}>Invanos</Text>
                        </View>
                        <View style={styles.view5}>
                            <Text style={styles.descriptionData}>
                            {staticContent.welcomeContent}
                            </Text>
                        </View>
                    </View>
                </View>
                <Footer style={styles.footerBackGround}>
                    <TouchableWithoutFeedback onPress={this.getStarted}>
                        <View style={[styles.footerbutton,color.buttonColor]}>
                            {(this.state.clicked)?
                                <ActivityIndicator size={responsiveFontSize(3)} color='#efac27'/>
                                :
                                <Text style={styles.buttonText}>GET STARTED</Text>
                            }    
                        </View>
                    </TouchableWithoutFeedback>
                </Footer>
            </Container>
        );
    }
}
