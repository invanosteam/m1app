import React, {Component} from 'react';

import {View,Text,Image,Button,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,
	ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,TextInput,ToastAndroid} from 'react-native';
import {Content,Container,Item,Card,CardItem,Body,Thumbnail,Label,Input,Footer,} from 'native-base';

import ActionBar from 'react-native-action-bar';

import FooterTabBar from './FooterTabBar';

var {height, width} = Dimensions.get('window');

import urls from '../config/urls';

import FetchData from './FetchData';

import styles from '../styles/ProfileScreen.style';

const fetchData = new FetchData();

export default class ProfileScreen extends Component {
    constructor(){
        super()
        this.state={
            customerName:'',
            customerEmail:'',
            customerTel:'123456789',
            customerLastName:'',
            activeTabNo:4,
            EditButton:'Edit',
            editable:false,
            changepass:0,
            currentPassword:'',
            newPassword:'',
            count:0
        }
    }

    componentDidMount() {
        this.getData();
    }

    getData(){
        this.setState({
            customerFirstName:global.firstName,
            customerLastName:global.lastName,
            customerEmail:global.userEmailId,
            // customerTel:global.phoneNo,
            count:global.cartItemCount
        });  
    }



    updatePass=()=>{
        if(this.state.newPassword === this.state.confirmPassword){
            var url=urls.updatePassword+'password/'+this.state.currentPassword+'/new_password/'+this.state.confirmPassword
            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.code===0){
                    ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                    this.setState({EditButton:'Edit',editable:false,changepass:0});
                }else{
                    ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                }
            });
        }else{
            ToastAndroid.show('Password does not match the confirm password.', ToastAndroid.SHORT);
        }
    };

    updateInfo=()=>{
        const {customerEmail,customerFirstName,customerLastName,customerTel,EditButton,currentPassword,newPassword}=this.state
        if(EditButton==='Edit'){
            this.setState({
                EditButton:'Updata',
                editable:true,
            });
        }else if(EditButton==='Updata'){
            this.setState({
                EditButton:'Edit',
                editable:false,
            });
            if(customerEmail!='' && customerFirstName!='' && customerLastName!=''){
                var url=urls.updateAccountInfo+'email/'+customerEmail+'/firstname/'+customerFirstName+'/lastname/'+customerLastName;

                fetchData.getMaster(url).then((responseJson)=>{
                    if(responseJson.code===0){
                        if(responseJson.model!=null){
                            ToastAndroid.show('Information updated.', ToastAndroid.SHORT);
                        }
                    }else{
                        ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                    } 
                });
            }
        }
    };
  
    render() {
        return (
            <Container style={styles.container}>
                <Content>
                    <View style={styles.view1}>
                        <TouchableOpacity>
                            <Thumbnail large style={styles.thumbnailImage} source = {require('../logos/male-img.png')}/>
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity onPress={()=>this.updateInfo()}>
                        <View style={styles.view2}>
                            <Text style={styles.text1}>{this.state.EditButton}</Text>
                        </View>
                    </TouchableOpacity>

                    <View>
                        <View style={styles.view3}>
                            <View style={styles.view4}>
                                <View style={styles.view5}>
                                    <Image style={styles.imgSize} source = {require('../logos/userName.png')}/>
                                </View>  
                                <View style={styles.view6}>
                                    <TextInput 
                                        style={styles.textInput1}
                                        defaultValue={this.state.customerFirstName}
                                        underlineColorAndroid={'transparent'} 
                                        editable={this.state.editable}
                                        onChangeText={(firstname) => this.setState({customerFirstName:firstname})}
                                    />
                                    <View style={styles.view7}>
                                        <TextInput 
                                            style={styles.textInput1}
                                            defaultValue={this.state.customerLastName}
                                            underlineColorAndroid={'transparent'} 
                                            editable={this.state.editable}
                                            onChangeText={(lastname) => this.setState({customerLastName:lastname})}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={styles.view3}>
                            <View style={styles.view4}>
                                <View style={styles.view5}>
                                    <Image style={styles.imgSize} source = {require('../logos/userEmail.png')}/>
                                </View>  
                                <View>
                                    <TextInput 
                                        style={styles.customerInfoText}
                                        defaultValue={this.state.customerEmail}
                                        underlineColorAndroid={'transparent'} 
                                        editable={this.state.editable}
                                        onChangeText={(email) => this.setState({customerEmail:email})}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.view3}>
                            <View style={styles.view4}>
                                <View style={styles.view5}>
                                    <Image style={styles.imgSize} source = {require('../logos/userTel.png')}/>
                                </View>  
                                <View>
                                    <TextInput 
                                    style={styles.customerInfoText}
                                    defaultValue={this.state.customerTel}
                                    underlineColorAndroid={'transparent'} 
                                    editable={this.state.editable}
                                    keyboardType={'numeric'}
                                    onChangeText={(phone) => this.setState({customerTel:phone})}
                                    />
                                </View>
                            </View>
                        </View>

                        {(this.state.changepass===0)?
                            <TouchableOpacity onPress={()=>this.setState({changepass:1})}>
                                <View style={styles.view3}>
                                    <View style={styles.view4}>
                                        <View style={styles.view5}>
                                            <Image style={styles.imgSize} source = {require('../logos/userPass.png')}/>
                                        </View>  
                                        <View>
                                            <Text style={styles.changePassText}>Change password</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity> 
                            :
                            <View>
                                <View style={styles.view3}>
                                    <View style={styles.view4}>
                                        <View style={styles.view5}>
                                            <Image style={styles.imgSize} source = {require('../logos/userPass.png')}/>
                                        </View>  
                                        <View>
                                            <Item regular style={styles.passTextArea}>
                                                <Input style={styles.inputLabel}
                                                placeholder='Current Password' 
                                                onChangeText={(currentPassword) => this.setState({currentPassword:currentPassword})}
                                                editable={this.state.editable}
                                                secureTextEntry={true}/>
                                            </Item>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.view3}>
                                    <View style={styles.view4}>
                                        <View style={styles.view5}>
                                            <Image style={styles.imgSize} source = {require('../logos/userPass.png')}/>
                                        </View>  
                                        <View>
                                            <Item regular style={styles.passTextArea}>
                                                <Input style={styles.inputLabel}
                                                placeholder='New Password' 
                                                onChangeText={(newPassword) => this.setState({newPassword:newPassword})}
                                                editable={this.state.editable}
                                                secureTextEntry={true}/>
                                            </Item>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.view3}>
                                    <View style={styles.view4}>
                                        <View style={styles.view5}>
                                            <Image style={styles.imgSize} source = {require('../logos/userPass.png')}/>
                                        </View>  
                                        <View>
                                            <Item regular style={styles.passTextArea}>
                                                <Input style={styles.inputLabel}
                                                placeholder='Confirm Password' 
                                                onChangeText={(confirmPassword) => this.setState({confirmPassword:confirmPassword})}
                                                editable={this.state.editable}
                                                onEndEditing={()=>this.updatePass()}
                                                secureTextEntry={true}/>
                                            </Item>
                                        </View>
                                    </View>
                                </View>
                            </View>   
                        }
                    </View>   
                </Content>

                <Footer style={styles.footer}>
                    <FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>
                </Footer>
            </Container>
        );
    }
}