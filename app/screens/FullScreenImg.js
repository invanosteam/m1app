import React, { Component } from 'react';
import {Text,Button,StyleSheet,View,Image,AsyncStorage,TouchableOpacity,Dimensions,ScrollView,ToastAndroid} from 'react-native';
import {Content,Card,Container,CardItem,Body,Thumbnail} from 'native-base';
import ImageZoom from 'react-native-image-pan-zoom';

import ActionBar from 'react-native-action-bar';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

import urls from '../config/urls';

export default class FullScreenImg extends Component {
    render(){
        const {goBack} = this.props.navigation;
        return(
            <Container>
                <ActionBar
                backgroundColor={'#ffffff'}
                containerStyle={styles.containerStyle}
                titleContainerStyle={styles.titleContainerStyle} 
                leftIconName={'back-icon'}
                onLeftPress={() => goBack()}
                />
                <View style={styles.container}>
                    <ImageZoom
                    cropWidth={width}
                    cropHeight={height}
                    imageWidth={width}
                    imageHeight={height}>
                        <Image 
                        style={styles.image}
                        source={{uri:global.imageUrl}}/>
                    </ImageZoom>
                </View>
            </Container>
        );
    }
}

const styles=StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    containerStyle:{
        paddingLeft:8,
    },
    titleContainerStyle:{
        marginRight:32,
        alignItems:'center',
    },
    image:{
        width:width,
        height:height,
        resizeMode: 'contain',
    },
})
