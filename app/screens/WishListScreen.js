import React, {Component} from 'react';

import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,ToolbarAndroid,
    List,ListView,TouchableOpacity,TouchableWithoutFeedback,AsyncStorage,ToastAndroid} from 'react-native';
import {Content,Container,Item,Button,Card,CardItem,Footer,Body,Thumbnail} from 'native-base';

import FooterTabBar from './FooterTabBar'

import EmptyWishListScreen from './EmptyWishListScreen'

import Loader from './Loader';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import urls from '../config/urls';

import styles from '../styles/WishListScreen.style';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class WishListScreen extends Component {
    constructor(props){
        super(props)
        this.state={
            myWishListData:ds,
            activeTabNo:4,
            call:0,
            isEmpty:false,
            count:0,
            load:false,
        }
    }

    componentDidMount() {
        this.getData();
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('WishListScreen'));
    }

    getData(){
        this.setState({count:global.cartItemCount});
        this.setState({load:true});

        fetchData.getMaster(urls.getWishlist).then((responseJson)=>{
            if(responseJson.code==0 && responseJson.model.items!=''){
                var array =[];
                for (let prop in responseJson.model.items) {
                    array.push(responseJson.model.items[prop]);
                }
                this.setState({
                    myWishListData:ds.cloneWithRows(array),
                    isEmpty:false,
                    load:false,
                    // call:0
                });
            }else{
                this.setState({
                    isEmpty:true,
                    load:false
                    // call:0,
                });
            }
        });
    }


    addtoCart=(rowData)=>{
        this.setState({load:true});

        if(rowData.type_id=='simple' && rowData.has_options==0){
            let url=urls.productAddToCart+rowData.entity_id+'/qty/1';
            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.code == 0){
                // global.cartItemCount=responseJson.model.items_qty;
                    var url=urls.wishListDel+rowData.entity_id
                    fetchData.getMaster(url).then((responseJson)=>{
                        if(responseJson.code == 0){
                            this.getData();
                            this.setState({load:false});
                        }else{
                            this.setState({load:false});
                            ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                        }
                    });
                }else{
                    this.setState({load:false});
                    ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                }
            });

        }else if(rowData.type_id=='configurable'){
            global.isWishListProduct=true;
            global.product_id=rowData.entity_id;
            global.product_type=rowData.type_id;
            this.setState({load:false});
            this.props.openProduct();
        }else if(rowData.type_id=='simple' && rowData.has_options!=0){
            this.setState({load:false});
            global.isWishListProduct=true;
            global.product_id=rowData.entity_id;
            global.product_type='custome';
            this.props.openProduct();
        }

    };

    Remove=(rowData)=>{

        Alert.alert(
            'Remove',
            'Do you Want to Remove this Product?',
            [
                {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel'
                },
                { text: 'Remove', onPress: () =>{
                    // ToastAndroid.show(''+rowData.entity_id, ToastAndroid.SHORT);
                    this.setState({load:true});
                    url=urls.wishListDel+rowData.entity_id;
                    fetchData.getMaster(url).then((responseJson)=>{
                        if(responseJson.code==0){
                            this.getData();
                            ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
                        }else{
                            this.setState({load:false});
                            ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                        }
                    });
                }}
            ],
            { cancelable: false }
        );
    }

    openProduct(rowData){
        if(rowData.product_type=='simple' && rowData.custom_option!=0){
            global.product_type='custome';
        }else{
            global.product_id=rowData.entity_id;
            global.product_type=rowData.product_type;

        }
        this.props.openProduct();
    }


    render() {
        
        if(this.state.isEmpty===true){
            return(<EmptyWishListScreen Click={this.props.onClick}/>);
        }else if(this.state.isEmpty===false){
            return (
                <Container style={styles.container}>
                    <ScrollView>
                        <ListView
                        dataSource={this.state.myWishListData}
                        renderRow={(rowData) =>
                            <View style={styles.view1}>
                                <TouchableWithoutFeedback onPress={()=>this.openProduct(rowData)}>
                                    <View style={styles.view2}>
                                        <View style={styles.view3}>
                                            <Image style={styles.image} source = {{ uri:rowData.image_url}}/>
                                        </View>  
                                        <View style={styles.view4}>
                                            <Text numberOfLines={2} style={styles.text1}>{rowData.name}</Text> 
                                            <View style={styles.view5}>
                                                <Text style={styles.text2}>₹{Math.round(rowData.price)}</Text>
                                                <Text style={styles.text3}>₹{Math.round(rowData.regular_price_with_tax)}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                                <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
                                    <View style={{marginRight:5}}>
                                        <Button rounded bordered style={styles.button} onPress={()=>this.addtoCart(rowData)}>
                                            <Text style={styles.buttonText}>ADD TO BAG</Text>
                                        </Button>
                                    </View>
                                    <View style={{marginLeft:5}}>
                                        <Button rounded bordered style={styles.button} onPress={()=>this.Remove(rowData)}>
                                            <Text style={styles.buttonText}>Remove</Text>
                                        </Button>
                                    </View>
                                </View> 
                            </View>
                        }/>
                    </ScrollView>
                    <Footer style={styles.footer}>
                        <FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>
                    </Footer>
                    <Loader loading={this.state.load}/>
                </Container>
            );
        }
    }
}
