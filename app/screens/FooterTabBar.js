import React, { Component } from 'react';
import {Image,Dimensions,StyleSheet,ToastAndroid,AsyncStorage,View  } from 'react-native';
import {Content,Footer,Text} from 'native-base';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

import styles from '../styles/FooterTabBar.style';

export default class FooterTabBar extends Component{

	render(){
		return(
			<Content>
				<Footer style={{height:responsiveHeight(8.2)}}>
					<BottomNavigation
			        labelColor="white"
			        rippleColor="white"
			        style={{ height:responsiveHeight(8.2),elevation: 2, position: 'absolute', left: 0, bottom: 0, right: 0 }}
			        onTabChange={(newTabIndex) => footerTabAction(newTabIndex)}
					activeTab={this.props.activeTabNo}>
			        <Tab
					    barBackgroundColor="#333333"
					    labelSize={responsiveFontSize(1.6)}
			          	label="Home"
			          	activeLabelColor="#efac27"
			          	activeIcon={<Image style={[styles.iconSize,styles.iconColor]} source = {require('../logos/home-icon1.png')}/>}
			          	icon={<Image style={styles.iconSize} source = {require('../logos/home-icon1.png')}/>}/>
			        <Tab 
			        	barBackgroundColor="#333333"
			        	labelSize={responsiveFontSize(1.6)}
				        label="Hot offer"
			        	activeLabelColor="#efac27"
			        	activeIcon={<Image style={[styles.iconSize,styles.iconColor]} source = {require('../logos/offer-icon1.png')}/>}
				        icon={<Image style={styles.iconSize} source = {require('../logos/offer-icon1.png')}/>}/>

			        <Tab
						count={this.props.count}
						badgefontSize={responsiveFontSize(1.4)}
			        	barBackgroundColor="#333333"
			        	labelSize={responsiveFontSize(1.6)}
			        	label="My Cart"
			        	activeLabelColor="#efac27"
			        	activeIcon={<Image style={[styles.shoppingBag,styles.iconColor]} source = {require('../logos/shopping-bag1.png')}/>}
			        	icon={<Image style={styles.shoppingBag} source = {require('../logos/shopping-bag1.png')}/>}/>

			        <Tab
			        	barBackgroundColor="#333333"
			        	labelSize={responsiveFontSize(1.6)}
			        	label="Search"
			        	activeLabelColor="#efac27"
			        	activeIcon={<Image style={[styles.iconSize,,styles.iconColor]} source = {require('../logos/search-icon1.png')}/>}
			        	icon={<Image style={styles.iconSize} source = {require('../logos/search-icon1.png')}/>}/>
			        <Tab
			          	barBackgroundColor="#333333"
			          	labelSize={responsiveFontSize(1.6)}
			          	label="Profile"
			          	activeLabelColor="#efac27"
			          	activeIcon={<Image style={[styles.iconSize,,styles.iconColor]} source = {require('../logos/account-icon1.png')}/>}
			          	icon={<Image style={styles.iconSize} source = {require('../logos/account-icon1.png')}/>}/>
			      	</BottomNavigation>		      		
			    </Footer>
		    </Content>
		);
	}
}