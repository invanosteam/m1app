import React, {Component} from 'react';

import {View,Text,Image,Button,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import {Content,Container,Item,Card,CardItem,Body,Thumbnail,Footer} from 'native-base';

import ActionBar from 'react-native-action-bar';

import FooterTabBar from './FooterTabBar'

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import urls from '../config/urls';

import styles from '../styles/OrderInfoScreen.style';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class OrderInfoScreen extends Component {
    constructor(){
        super()
        this.state={
            myProductData:ds,
            activeTabNo:4,
            myOrderData:'',
            shippingAmount:0,
            subTotal:0,
            discountAmount:0,
            grandTotal:0,
            count:0,
        }
    }

    componentDidMount() {
        this.getData();
    }
    
    getData(){
        this.setState({count:global.cartItemCount});

        var url=urls.getOrderInfo+global.OrderId;

        fetchData.getMaster(url).then((responseJson)=>{
            if(responseJson.code===0){
                this.setState({
                    myProductData:ds.cloneWithRows(responseJson.model.items),
                    myOrderData:responseJson.model,
                    paymentMethod:responseJson.model.pay_method.title,
                    street:responseJson.model.address.shipping_address.street,
                    city:responseJson.model.address.shipping_address.city,
                    region:responseJson.model.address.shipping_address.region,
                    country:responseJson.model.address.shipping_address.country_id,
                    phone:responseJson.model.address.shipping_address.telephone,
                    postcode:responseJson.model.address.shipping_address.postcode,
                    shippingAmount:responseJson.model.shipping_amount,
                    subTotal:responseJson.model.subtotal,
                    discountAmount:responseJson.model.base_discount_amount,
                    grandTotal:responseJson.model.grand_total,
                }); 
            }
        });
    }


    render() {
        const {goBack} = this.props.navigation;

        return (
            <Container style={styles.container}>
                <ActionBar
                    backgroundColor={'#ffffff'}
                    containerStyle={styles.containerStyle}
                    titleContainerStyle={styles.titleContainerStyle}
                    leftIconName={'back-icon'}
                    onLeftPress={() => goBack()}
                />
                <ScrollView>
                    <ListView
                        dataSource={this.state.myProductData}
                        renderRow={(rowData) =>
                        <View>
                            <Card>
                                <CardItem>
                                    <Image style={styles.imgSize} source = {{ uri:rowData.pic_url}}/>
                                    <View style={styles.view1}>
                                        <View>
                                            <Text style={styles.text1} >{rowData.name}</Text>
                                            <Text style={styles.text2}>₹{Math.round(rowData.price)}</Text>
                                        </View>
                                    </View>
                                </CardItem>
                            </Card>
                        </View>
                    }/>

                    <View style={styles.view2}>
                        <Text style={styles.text3} >ORDER DETAILS</Text>
                        <Card>
                            <CardItem>
                                <View>
                                    <Text style={styles.titleText}>Order Id #{this.state.myOrderData.order_id}</Text>
                                    <Text style={styles.titleText} >Placed On : {this.state.myOrderData.created_at}</Text>
                                </View>
                            </CardItem>
                        </Card>
                    </View>

                    <View>
                        <Text style={styles.text3}>Shipping Details</Text>
                        <Card>
                            <CardItem>
                                <View>
                                    <Text style={styles.titleText}>{this.state.myOrderData.customer_name}</Text>
                                    <Text style={styles.titleText}>{this.state.street}</Text>
                                    <View style={styles.view3}>
                                        <Text style={styles.titleText}>{this.state.city},</Text>
                                        <Text style={styles.titleText}> {this.state.region},</Text>
                                        <Text style={styles.titleText}>{this.state.postcode},</Text>
                                    </View>
                                    <View style={styles.view3}>
                                        <Text style={styles.titleText}> {this.state.country},</Text>
                                        <Text style={styles.titleText}> {this.state.phone}.</Text>
                                    </View>
                                </View>
                            </CardItem>
                        </Card>
                    </View>

                    <View style={styles.view4}>
                        <Text style={styles.text3}>Payment Method</Text>
                        <Card>
                            <CardItem>
                                <View>
                                    <Text style={styles.titleText}>Selected Payment Method : {this.state.paymentMethod}</Text>
                                </View>
                            </CardItem>
                        </Card>
                    </View>


                    <View style={styles.view4}>
                        <Text style={styles.text3}>Payable Amount</Text>
                        <Card>
                            <CardItem>
                                <View style={styles.view5}>
                                    <View style={styles.view6
                                    }>
                                        <Text style={styles.titleText}>Subtotal : </Text>
                                        <View style={styles.view5}>
                                            <Text style={styles.text4}>₹ {Math.round(this.state.subTotal)}</Text>
                                        </View>
                                    </View>

                                    <View style={styles.view6}>
                                        <Text style={styles.titleText}>Shipping Amount : </Text>
                                        <View style={styles.view5}>
                                            <Text style={styles.text4}>₹ {Math.round(this.state.shippingAmount)}</Text>
                                        </View>
                                    </View>

                                    <View style={styles.view6}>
                                        <Text style={styles.titleText}>Discount Amount : </Text>
                                        <View style={styles.view5}>
                                            <Text style={styles.text4}>₹ {Math.round(this.state.discountAmount)}</Text>
                                        </View>
                                    </View>

                                    <View style={styles.view6}>
                                        <Text style={styles.text5}>Total Amount : </Text>
                                        <View style={styles.view5}>
                                            <Text style={styles.text6}>₹ {Math.round(this.state.grandTotal)}</Text>
                                        </View>
                                    </View>
                                </View>
                            </CardItem>
                        </Card>
                    </View>
                </ScrollView>
                <Footer style={{height:responsiveHeight(8.2)}}>
                    <FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>
                </Footer>
            </Container>
        );
    }
}