import React, {Component} from 'react';
import {Text,View,ToastAndroid,ListView,TouchableOpacity,AsyncStorage} from 'react-native';
import { Container, Content,Footer} from 'native-base';

import SearchBar from 'react-native-material-design-searchbar';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import FooterTabBar from './FooterTabBar';

import Loader from './Loader';

import ActionBar from 'react-native-action-bar';

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import urls from '../config/urls';

import FetchData from './FetchData';

import styles from '../styles/SearchScreen.style';

const fetchData = new FetchData();
export default class SearchScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            displayData: '',
            productListData:ds,
            activeTabNo:3,
            count:0
        }
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('SearchScreen'));
    }

    componentDidMount() {
        this.getData();
    }

    getData(){
        this.setState({count:global.cartItemCount});
    }
    
    getSearchData=(data)=>{
        var url=urls.searchUrl+data;
        fetchData.getMaster(url).then((responseJson)=>{
            if (responseJson.productCount!==0) {
                var array = [];
                for (let prop in responseJson.model.items) {
                    array.push(responseJson.model.items[prop]);
                }
                this.setState({productListData:ds.cloneWithRows(array)});
            }else{
                ToastAndroid.show('NoData found !', ToastAndroid.SHORT);
                var array = [{'name':'No Data found'}];
                this.setState({productListData:ds.cloneWithRows(array)});
            }
        });
    };

    render() {
        const {goBack} = this.props.navigation;

        passData=(data)=>{
            global.product_id=data.entity_id;
            if(data.type_id=='simple'&&data.has_options==0){
                global.product_type=data.type_id;
            }else if(data.type_id=='configurable'){
                global.product_type=data.type_id;            
            }else if(data.type_id=='simple' && data.has_options==1){
                global.product_type='custome';
            }
            // alert(global.product_type)
            this.props.navigation.navigate('ProductScreen');




            // // ToastAndroid.show('data:'+data.entity_id, ToastAndroid.SHORT);
            // try {
            //     // AsyncStorage.setItem('ProductId',data.entity_id);
            //     // if(rowData.type_id=='simple'&&rowData.has_options==0){
            //     //   // ToastAndroid.show(''+rowData.type_id, ToastAndroid.SHORT);
            //     //     }else if(rowData.type_id=='configurable'){
            //     //       this.props.navigation.navigate('ConfigProductScreen');
            //     //       // ToastAndroid.show(''+rowData.type_id, ToastAndroid.SHORT);
            //     //     }else if(rowData.type_id=='simple' && rowData.has_options==1){
            //     //       this.props.navigation.navigate('CustomeProductScreen');
            //     //     }
            // }catch (error) {
            //     alert('Error Msg P2 :'+error);
            //     this.setState({load:false});
            // }
        };
  	
        return (
            <Container style={styles.container}>
                <ActionBar
                    backgroundColor={'#ffffff'}
                    containerStyle={styles.containerStyle}
                    titleContainerStyle={styles.titleContainerStyle}
                    leftIconName={'back-icon'}
                    onLeftPress={() => goBack()}
                />

                <SearchBar
                    onSearchChange={(data) =>this.getSearchData(data)}
                    height={60}
                    textStyle={{fontSize:responsiveFontSize(2)}}
                    placeholder={'Search'}
                />

                <ListView
                    dataSource={this.state.productListData}
                    renderRow={(rowData) =>
                    <TouchableOpacity onPress={()=>passData(rowData)}>
                        <View style={styles.view1}>
                            <Text numberOfLines={1} style={styles.text1} >{rowData.name}</Text>
                        </View>
                    </TouchableOpacity>
                }/>
                <Footer style={styles.footer}>
                    <FooterTabBar activeTabNo={this.state.activeTabNo} count={global.cartItemCount}/>  
                </Footer> 

            </Container>
        );
    }
}