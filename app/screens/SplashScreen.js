import React, {Component} from 'react';
import {View,Image} from 'react-native';

import urls from '../config/urls';

import styles from '../styles/SplashScreen.style';

import { NavigationActions } from 'react-navigation';

import FetchData from './FetchData';

const fetchData = new FetchData();

const WelcomeScreen=fetchData.resetIndex('WelcomeScreen');

export default class SplashScreen extends Component {
    componentWillMount() {
        setTimeout(() => {
            this.props.navigation.dispatch(WelcomeScreen);
        }, 2500);
    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.img} source = {{ uri:urls.splashImage}}/>
            </View>
        );
    }
}