import React, {Component} from 'react';

import {View,Text,Image,Button,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,
	ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import {Content,Container,Item,Card,CardItem,Body,Thumbnail,Footer} from 'native-base';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import FooterTabBar from './FooterTabBar';

import EmptyOrderScreen from './EmptyOrderScreen';

var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import urls from '../config/urls';

import styles from '../styles/MyOrderScreen.style';

import FetchData from './FetchData';

const fetchData = new FetchData();

export default class MyOrderScreen extends Component {
    constructor(){
        super()
        this.state={
            myOrderData:ds,
            activeTabNo:4,
            isEmpty:false,
            count:0,
        }
    }

    componentDidMount() {
        this.getData();
    }

    getData(){
        this.setState({count:global.cartItemCount});
        fetchData.getMaster(urls.getOrderList).then((responseJson)=>{
            if(responseJson.code===0 && responseJson.msg===null){
                if(responseJson.model!=''){
                    var array =[];
                    for (let prop in responseJson.model) {
                        array.push(responseJson.model[prop]);
                    }
                    this.setState({
                        isEmpty:false,
                        myOrderData:ds.cloneWithRows(array),
                    }); 
                }else{
                    this.setState({isEmpty:true});
                }
            }else{
                ToastAndroid.show('Please try again getOrderList'+responseJson.msg, ToastAndroid.SHORT);
            }
        });
    }

    OrderClick=(rowData)=>{
        global.OrderId=rowData.order_id;
        this.props.OrderClick();
    };

    render() {
        if(this.state.isEmpty){
            return(<EmptyOrderScreen Click={this.props.onClick}/>);
        }else{
            return (
                <Container style={styles.container}>
                    <ScrollView>
                        <ListView
                        dataSource={this.state.myOrderData}
                        renderRow={(rowData) =>
                            <TouchableOpacity onPress={()=>this.OrderClick(rowData)}>
                                <View style={styles.view1}>
                                    <View style={styles.view2}>
                                        <View>
                                            <Text style={styles.text1}>{rowData.created_at}</Text>
                                            <Text style={styles.text2}>Order id #{rowData.order_id}</Text>
                                        </View>
                                        <View style={styles.view3}>
                                            <Text style={styles.text3}>{rowData.status}</Text>
                                        </View>
                                        <View style={styles.view4}>
                                            <Image style={styles.image} source = {require('../logos/right-icon1.png')}/>
                                        </View>  
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }/>
                    </ScrollView>
                    <Footer style={styles.footer}>
                        <FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>
                    </Footer>
                </Container>
            );
        }
    }
}