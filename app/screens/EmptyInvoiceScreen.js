import React, {Component} from 'react';

import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import { Container, Header, Content, Form,Button,Icon,Item,Footer,Input,Label,Left,Right } from 'native-base';

import FooterTabBar from './FooterTabBar';

var {height, width} = Dimensions.get('window');


export default class EmptyInvoiceScreen extends Component {
   constructor(){
    super()
    this.state={
      activeTabNo:4,
      count:0,
    }
  }

  componentDidMount() {
    this.getCount();
  }

  async getCount(){
    var cartCount= await AsyncStorage.getItem('count');
    this.setState({count:cartCount});
  }  

  render() {
    return (

      <Container style={styles.container}>
      <View style={styles.mainView}>
        <Text style={styles.label}>Nothing here</Text>
        <Image style={styles.logo} source = {require('../logos/emptycart.png')}/>
        <Text style={styles.labelOffers}>Checkout great products & offers</Text>

  
         <View style={{marginTop:10}}>
            <Button rounded style={styles.button} onPress={this.props.Click}>
                <Text style={styles.buttonText}>Shop Now</Text>
            </Button>
          </View>
      </View>
      <Footer style={{height:58}}>
        <FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>
      </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    width:width,
    height:height,
  },
  mainView:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    // paddingTop:80,
  },
  label:{
    marginBottom:10,
    fontSize: 20,
    justifyContent: 'center',
  },
  labelOffers:{
    marginBottom:10,
    fontSize: 18,
    justifyContent: 'center',
  },
  editText:{
    width:250,
    height: 40
  },
  button: {
    marginTop:15,
    marginBottom: 10,
    width: 200,
    height:50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#201e1f'
  },
  buttonText: {
    color: 'white'

  },
  alternativeLayoutButtonContainer: {
    alignItems: 'center',
    paddingTop:20
  },
  logo:{
    height:100,
    width:100,
    marginLeft:50,
    marginRight:50,
    // marginTop: 50,
    marginBottom: 10,
  }

})

