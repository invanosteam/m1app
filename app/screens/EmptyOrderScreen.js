import React, {Component} from 'react';

import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import { Container, Header, Content,Button,Item,Footer } from 'native-base';

import FooterTabBar from './FooterTabBar';

import styles from '../styles/EmptyOrderScreen.style';

var {height, width} = Dimensions.get('window');


export default class EmptyOrderScreen  extends Component {
    constructor(){
        super()
        this.state={
            activeTabNo:4,
        }
    }

    onClick=()=>{
        this.props.Click();
    }

    render() {
        return (
            <Container style={styles.container}>
                <View style={styles.mainView}>
                    <Image style={styles.img} source = {require('../logos/shopping-box.png')}/>
                    <Text style={styles.label}>Your OrderList is empty!</Text>
                    <Text style={styles.labelOffers}>You have not placed any orders</Text>
                    <View style={{marginTop:10}}>
                        <Button rounded style={styles.button} onPress={this.onClick}>
                            <Text style={styles.buttonText}>Continue Shopping</Text>
                        </Button>
                    </View>
                </View>
                <Footer style={styles.footer}>
                    <FooterTabBar activeTabNo={this.state.activeTabNo}/>
                </Footer>
            </Container>
        );
    }
}