import React, { Component } from 'react';
import {View,Text,Image,ScrollView,DrawerLayoutAndroid,Dimensions,BackHandler,Alert,StatusBar,TouchableHighlight,ListView,TouchableOpacity,AsyncStorage,ToastAndroid  } from 'react-native';
import { Container,Card,CardItem, Header,Body, Content, Form,Button,Item,Footer,Input,Segment,Label,Left,Right } from 'native-base';

import ActionBar from 'react-native-action-bar';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import { TabViewAnimated, TabBar, SceneMap ,TabViewPagerScroll} from 'react-native-tab-view';


import Login from './Login';
import Register from './Register';


var {height, width} = Dimensions.get('window');

const initialLayout = {
    height: 0,
    width: Dimensions.get('window').width,
};

export default class LoginScreen extends Component{
    constructor(){
        super()
        this.state={
            index: 0, 
            routes: [
                { key: '0', title: 'Sign in'},
                { key: '1', title: 'Sign up'},
            ],
        }
    };

    _handleIndexChange = index => this.setState({ index });  
 
    _renderHeader = props => <TabBar scrollEnabled={false} style= {{width:width/1.5,marginTop:15, marginBottom:15,
                marginLeft:width/6,borderRadius:10,backgroundColor: '#ffffff',}} labelStyle={{fontFamily:'opensans',fontWeight:'bold',color: '#000000',fontSize:responsiveFontSize(1.8),margin: responsiveHeight(1),}} {...props}/>;
    
    _renderPager = (props) => <TabViewPagerScroll {...props} swipeEnabled = {false} animationEnabled = {true}/>;

    _renderScene = ({ route }) => {
        switch (route.key) {
            case '0':
                return <Login/>;
            case '1':
                return <Register/>;
        }
    };
        
    render(){

        return(
            <Container>
                <ActionBar
                    backgroundColor={'#ffffff'}
                    title={'Notice Board'}
                    titleStyle={{textAlign:'center',color:'#000000',fontSize:20,}}
                    leftIconName={'menu-icon'}
                    // onLeftPress={() => this.openDrawer()}
                />

                <TabViewAnimated
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderPager={this._renderPager}
                    renderHeader={this._renderHeader}
                    onIndexChange={this._handleIndexChange}
                    initialLayout={initialLayout}
                />
                
            </Container>
          

        );
    }
} 
