import React, { Component } from 'react';
import {View,Text,Image,Button,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,Picker,CheckBox,
	ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid  } from 'react-native';
import { Container, Header, Content, Form, Item, Input,Footer, Label,Left,Right } from 'native-base';
var {height, width} = Dimensions.get('window');
import Loader from './Loader';

export default class SetShippingAddress extends Component{
	 constructor(props) {
   super(props)

   this.state = {
     email:'',
      firstName:'',
      middleName:'',
      lastName:'',
      company:'',
      address:'',
      street:'',
      city:'',
      currentState:'',
      zip:'',
      country:'',
      contactNo:'',
      fax:0,
      value: false,
      isForShipping:0,
      qty:'',
      countryList:[],
      stateList:[],
      call:0,
      load:true,
}
}

  componentDidMount() {
    this.getData();    
  }
  async getData(){
    var EmailId= await AsyncStorage.getItem('EmailId');
    this.setState({email:EmailId});
    if(EmailId!=null){
    ToastAndroid.show('emailId'+EmailId,ToastAndroid.SHORT);
    this.setState({email:EmailId,isEnable:false});
    }else{
      this.setState({isEnable:true})
    }
    fetch('http://stageapp.invanos.net/xapi/checkout/getcountries')
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.model!==null){
        // ToastAndroid.show('responseJson:'+responseJson.msg,ToastAndroid.SHORT);
        this.setState({
          countryList:responseJson.model.countries,
          load:false,
        })
      }
    })
    .catch((error) => {
      console.error(error);
    });
}

 // ToastAndroid.show('Welcome '+ firstName+','+middleName+','+lastName+','+company+','+address+','+street+','+city+','+currentState+','
 //   +country+','+zip+','+contactNo+','+fax, ToastAndroid.SHORT);

 

 nextScreen=()=>{
    this.props.navigation.navigate('ShippingMethod');
 };

 render(){

     let cDataList=this.state.countryList.map((item,index)=>{
      return(
        <Picker.Item key={index} value={item.country_id} label={item.name} />
        )
    });

   let sDataList=this.state.stateList.map((item,index)=>{
      return(
        <Picker.Item key={index} value={item.region_id} label={item.name}/>
        )
    });    


   currentState=(country)=>{
    this.setState({
      country: country,
      // load:true,
    });
    // ToastAndroid.show('currentStateWelcome '+country, ToastAndroid.SHORT);
    fetch('http://stageapp.invanos.net/xapi/checkout/getRegions/country_id/'+country)
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.model!==null && responseJson.code!==2){
      // ToastAndroid.show('responseJson:'+responseJson.msg,ToastAndroid.SHORT);
      this.setState({
        stateList:responseJson.model.regions,
        load:false,
      })
      } 
    })
    .catch((error) => {
      console.error(error);
    });
   };

   passData=(rowData)=>{
    this.setState({ShippingMethodCode:rowData.code})
    ToastAndroid.show('Selected ShippingMethod :'+rowData.method_title, ToastAndroid.SHORT);
  };
 	return(
    <Container style={styles.container}>
      <ScrollView>

        <Content style={styles.subContainer}>
         
          <View style={styles.rowView}>
            <Item floatingLabel style={{marginTop:10,width:170}}>
              <Label >First Name</Label>
              <Input
              onChangeText={(firstName) => this.setState({firstName:firstName})}
              />
            </Item>

            <Item floatingLabel style={{marginTop:10,width:150}}>
              <Label>Middle Name</Label>
              <Input 
              onChangeText={(middleName) => this.setState({middleName:middleName})}
              />
            </Item>        
          </View>

          <Item floatingLabel style={{marginTop:10,width:150}}>
            <Label>Last Name</Label>
            <Input 
            onChangeText={(lastName) => this.setState({lastName:lastName})}
            />
          </Item>

          <Item floatingLabel style={{marginTop:10,width:250}}>
            <Label>Email Address</Label>
            <Input 
            editable={this.state.isEnable}
            onChangeText={(email) => this.setState({email:email})}
            />
          </Item>

          <Item floatingLabel style={{marginTop:10,width:200}}>
            <Label>Company</Label>
            <Input 
            onChangeText={(company) => this.setState({company:company})}
            />
          </Item>

          <Item floatingLabel style={{marginTop:10,width:300}}>
            <Label>Address</Label>
            <Input 
            onChangeText={(address) => this.setState({address:address})}
            />
          </Item>

            <Item floatingLabel style={{marginTop:10,width:300}}>
            <Label>Street Address</Label>
            <Input 
            onChangeText={(street) => this.setState({street:street})}
            />
          </Item>

           <Item floatingLabel style={{marginTop:10,width:300}}>
            <Label>Contact No</Label>
            <Input 
            onChangeText={(contactNo) => this.setState({contactNo:contactNo})}
            maxLength = {10}
            keyboardType = 'numeric'
            autoCapitalize={'none'}
            />
          </Item>

          <View style={styles.rowView}>
            <View style={{marginTop:10}}>
              <Label>Country</Label>
              <Picker style={styles.picker}
              selectedValue={this.state.country}
              onValueChange={(country) => currentState(country)}>
              <Picker.Item label="Your country"/>
              {cDataList}
              </Picker>
            </View>
            
            <View style={{marginTop:10}}>
              <Label>State</Label>
              <Picker style={styles.picker}
              selectedValue={this.state.currentState}
              onValueChange={(currentS) => this.setState({currentState:currentS})}>
              <Picker.Item label="Choose State"/>
              {sDataList}
              </Picker>
            </View>
          </View>

          <View style={styles.rowView}>
            <View style={{marginTop:10}}>
              <Label>City</Label>
              <Picker style={styles.picker}
              onValueChange={(itemValue, itemIndex) => this.setState({city:itemValue})}
              selectedValue={this.state.city}>
              <Picker.Item label="City" value="City"/>
              <Picker.Item label="Mumbai" value="Mumbai"/>
              <Picker.Item label="Delhi" value="Delhi"/>
              <Picker.Item label="Bangalore" value="Bangalore"/>
              <Picker.Item label="Bangalore" value="Bangalore"/>
              <Picker.Item label="Ahmedabad" value="Ahmedabad"/>
              <Picker.Item label="Chennai" value="Chennai"/>
              <Picker.Item label="Kolkata" value="Kolkata"/>
              <Picker.Item label="Surat" value="Surat"/>
              <Picker.Item label="Pune" value="Pune"/>
              <Picker.Item label="Jaipur" value="Jaipur"/>
              </Picker>
            </View>

            <Item floatingLabel style={{width:180,marginTop:15}}>
              <Label>Zip Code</Label>
              <Input
               maxLength = {8}
               keyboardType = 'numeric'
               onChangeText={(zip) => this.setState({zip:zip})}
              />
            </Item>
          </View>

          <Item floatingLabel>
            <Label>Fax</Label>
            <Input
             maxLength = {8}
             keyboardType = 'numeric'
             onChangeText={(fax) => this.setState({fax:fax})}
            />
          </Item>

          <View  style={styles.checkBox}>
          <CheckBox
          label='Same for Billing?'
          checkedImage={require('../logos/checked.png')}
          uncheckedImage={require('../logos/unchecked.png')}
          onChange={(checked) =>this.checkBoxData(checked)}
          checked={this.state.value}/>
          </View>
        </Content>

          <View style={{backgroundColor:'#f1f1f1'}}>
              <Text style={styles.deliveryText}>Delivery</Text>

              <View style={styles.deliveryView}>
                <ListView
                horizontal={true}
                dataSource={this.state.shippingMethods}
                renderRow={(rowData) =>
                  <Button style={styles.deliveryButton} onPress={()=>passData(rowData)}>
                  <View style={styles.deliveryButtonView}> 
                    <Text style={styles.deliveryButtonText}>₹ {Math.round(rowData.price)}</Text>
                    <Text>{rowData.method_title}</Text>
                  </View>
                  </Button> 
                }/>
              </View>
          </View>
          </ScrollView>

          <Footer style={styles.footerBackGround}>
          <TouchableOpacity onPress={this.setShipping}>
            <View style={styles.footerButton}>
            <View style={styles.buttonTextView}>
              <Text style={styles.buttonText}>CONTINUE TO PAYMENT</Text>
            </View>
            <View style={{width:25,marginRight:10}}>
            <View style={{justifyContent: 'flex-end',alignItems:'flex-end'}}>
                <Image style={{height:18,width:18,resizeMode: 'contain'}} source = {require('../logos/forward-icon.png')}/>
            </View>
            </View>

            </View>
          </TouchableOpacity>
          </Footer>

          <Loader loading={this.state.load}/>



    </Container>
    );
	}
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    width:width,
    height:height,
    backgroundColor: '#ffffff',
  },
  button:{
    marginBottom: 30,
    width: 100,
    marginLeft:25,
    marginRight:25,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  buttonText: {
    padding: 10,
    color: 'white'
  },
  alternativeLayoutButtonContainer: {
    alignItems: 'center',
    paddingTop:20
  }

})
