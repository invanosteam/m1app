import React, {Component} from 'react';

import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,ToolbarAndroid,List,ListView,Dimensions,TouchableWithoutFeedback,AsyncStorage,ToastAndroid} from 'react-native';
import { Container,Card,CardItem, Header,Body, Content,ListItem, Form,Button, Item,Footer,Input,Segment,Label,Left,Right } from 'native-base';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

import FetchData from './FetchData';

const fetchData = new FetchData();

import styles from '../styles/OrderconfirmScreen.style';

import urls from '../config/urls';

export default class OrderconfirmScreen extends Component {
    constructor(){
        super()
        this.state={
            orderId:0,
            myData:[],
            grandTotal:'',
        }
    }

    componentDidMount() {
        this.getData();
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('OrderconfirmScreen'));
    }

    getData(){
        if(global.order!=null && global.order!= ''){
            this.setState({orderId:'#'+global.order});
        }
        fetchData.initGlobal();
    }

    continueShopping=()=>{
        this.props.navigation.navigate('MainScreen');
    };

    trackYorOrder=()=>{
        ToastAndroid.show('Sorry this feature is underdevelopment try in next update', ToastAndroid.SHORT);
        // this.props.navigation.navigate('MainScreen');
    };
  
    render() {
        return (
            <Container style={styles.container}>
                <View style={styles.view1}>
                    <View>
                        <Text style={styles.text1}>THANKYOU</Text>
                        <Text style={styles.text2}>FOR YOUR ORDER</Text>
                        <View style={styles.view2}>
                            <Text style={styles.text3}>Order number :  </Text>
                            <Text style={styles.text4}>{this.state.orderId}</Text>
                        </View>
                    </View>

                    <View style={styles.view3}>
                        <Image 
                            style={styles.image1} 
                            source = {{ uri:urls.OrderConfirmImage}}/>
                    </View>
                </View>

                <View>
                    <Text style={styles.text5}>ESTIMATED DELIVERY</Text>
                    <Text style={styles.text6}>Monday July 1,2018</Text>
                    <View style={styles.buttonView}>
                        <View>
                            <Button rounded style={styles.buttonTrack} onPress={this.trackYorOrder}>
                                <Text style={styles.buttonTextTrack}>TRACK YOUR ORDER HERE</Text>
                            </Button>
                        </View>
                    </View>
                </View>

                <View style={styles.view4}>
                    <Text style={styles.text7}>or</Text>
                </View>      

                <Footer style={styles.footerBackGround}>
                    <TouchableWithoutFeedback onPress={this.continueShopping}>
                    {(this.state.clicked)?
                        <ActivityIndicator size={30} color='#efac27'/>
                        :
                        <View style={styles.footerButton}>
                            <View style={styles.footerButtonView}>
                                <Text style={styles.buttonText}>CONTINUE SHOPPING</Text>
                            </View>
                        </View>
                    }
                    </TouchableWithoutFeedback>
                </Footer>
            </Container>
        );
    }
}
                            // <View style={styles.imageView}>
                            //     <View style={styles.imageView1}>
                            //         <Image style={styles.image2} source = {require('../logos/forward-icon.png')}/>
                            //     </View>
                            // </View>