import React, {Component} from 'react';

import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import { Container, Header, Content,Button,Item,Footer } from 'native-base';

import FooterTabBar from './FooterTabBar';

var {height, width} = Dimensions.get('window');

import styles from '../styles/EmptySavedAddressScreen.style';

export default class EmptySavedAddressScreen  extends Component {
    constructor(){
        super()
        this.state={
            activeTabNo:4,
        }
    }

    onClick=()=>{
        this.props.onClick();
    };

    render() {
        return (
            <Container style={styles.container}>
                <View style={styles.mainView}>
                    <Image style={styles.img} source = {require('../logos/address-img.png')}/>
                    <Text style={styles.label}>No address found !</Text>
                    <View style={styles.view1}>
                        <Button rounded style={styles.button} onPress={this.onClick}>
                            <Text style={styles.buttonText}>Add Address</Text>
                        </Button>
                    </View>
                </View>
                <Footer style={styles.footer}>
                    <FooterTabBar activeTabNo={this.state.activeTabNo}/>
                </Footer>
            </Container>
        );
    }
}