import React, {Component} from 'react';
import {View,Text,StyleSheet,Dimensions,ToastAndroid} from 'react-native';
import {Content,Container,Item,Label,Input} from 'native-base';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import AppButton from '../components/Button/AppButton';

import ActionBar from 'react-native-action-bar';

var {height, width} = Dimensions.get('window');

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();

export default class ForgotPassword extends Component {
    constructor(){
        super()
        this.state={
            EmailId:'',
            clicked:false,
        }
    }

    onSubmitClick=()=>{
        const {EmailId} = this.state
        this.setState({clicked:true});
        var format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(format.test(EmailId)){
            var data='email='+EmailId
            fetchData.postMaster(urls.forgotPassword,data).then((responseJson)=>{
                // alert(JSON.stringify(responseJson))
                if(responseJson.code===0){
                    ToastAndroid.show(''+responseJson.msg+' please reset your password from web.', ToastAndroid.SHORT);

                }else if(responseJson.code===1 ) {
                    ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                    
                }else if(responseJson.code===2 ) {
                    ToastAndroid.show('You have tried many times please try again late!', ToastAndroid.SHORT);
                    
                }else if(responseJson.code===6) {
                    ToastAndroid.show(''+responseJson.msg+' please try again with correct email Address', ToastAndroid.SHORT);
                }
                this.setState({clicked:false});
            });
        }else{
            ToastAndroid.show('Invalid Email Address', ToastAndroid.SHORT);
            this.setState({clicked:false});
        }
    };

render() {
    const {goBack} = this.props.navigation;

    return (
        <Container style={styles.container}>
            <ActionBar
                backgroundColor={'#ffffff'}
                containerStyle={styles.containerStyle}
                titleContainerStyle={styles.titleContainerStyle} 
                leftIconName={'back-icon'}
                onLeftPress={() => goBack()}
            />
            <Content>
                <View style={styles.view1}>
                    <Item floatingLabel style={styles.userTextArea}>
                        <Label style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}>Email Address</Label>
                        <Input style={styles.inputLabel}
                        onChangeText={(EmailId) => this.setState({EmailId:EmailId})}/>
                    </Item>
                </View>
        
                <View style={styles.buttonView}>
                    <View>
                        <AppButton buttonTitle={'Submit'} clicked={this.state.clicked} onPress={this.onSubmitClick}/>
                    </View>
                </View>
            </Content>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#ffffff',
    },
    containerStyle:{
        paddingLeft:8,
    },
    titleContainerStyle:{
        marginRight:32,
        alignItems:'center',
    },
    view1:{
        marginTop:20,
        alignItems:'center',
    },
    userTextArea:{
        borderBottomColor:'#201e1f',
        width:responsiveWidth(85),
        height:responsiveHeight(8),
    },
    inputLabel:{
        color:'#201e1f',
        fontSize:responsiveFontSize(1.6),
        marginLeft:2,
    },
    buttonView:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
        marginTop:15,
    },
})