import React ,{Component} from 'react';
import {View,Text,Image,ActivityIndicator,Alert,RefreshControl,ToastAndroid,ScrollView,DrawerLayoutAndroid,BackHandler,TouchableHighlight,TouchableWithoutFeedback,Dimensions,AsyncStorage,ToolbarAndroid,TouchableOpacity,StyleSheet,List,ListView} from 'react-native';
import {Content,Container,Item,Card,CardItem,Body,Footer,Thumbnail} from 'native-base';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'

import CheckBox from 'react-native-checkbox';

import GridView from 'react-native-easy-gridview';

import ActionBar from 'react-native-action-bar';

import FooterTabBar from './FooterTabBar';

import Loader from './Loader';

import LabelSelect from 'react-native-label-select';

import ActionSheet from 'react-native-actionsheet';

import {Actions} from 'react-native-router-flux'

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

const loading = (
  <View style={{alignItems: 'center', justifyContent: 'center' }}>
    <ActivityIndicator size={40} color='#201e1f' />
  </View>
)

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import styles from '../styles/ProductList.style';

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();

export default class ProductList extends Component{
	constructor(){
		super()
		this.state={
		    productListData:ds,
		    load:true,
		    count:0,
		    isEmpty:false,
		}
	}
	componentDidMount() {
		this.getData();
		// alert(this.props.navigation.state.routeName) 
	}

    componentWillMount() {
        const analytics = new Analytics(urls.AppId, null, { debug: true });
        analytics.hit(new PageHit('ProductList'));
    }

   	componentWillUnmount(){
   		const analytics = new Analytics(urls.AppId, null, { debug: true });
        analytics.hit(new PageHit('MainScreen'));
   	}

	getData(){
		this.setState({count:global.cartItemCount});
		// alert(global.firstName)
	  	if(global.category_id===null){
	  		global.category_id=4;
	  	}

	  	var url;
	  	if(global.filterUrl!=null){
	  		url=global.filterUrl
	  	}else{
	  	 	url=urls.getCategory+global.category_id
	  	}

	  	// alert(url)

	  	fetchData.getMaster(url).then((responseJson)=>{
	  		// alert(responseJson.length)
	  		if(responseJson!=null && responseJson!= '' && responseJson.length!=0){
		  		var array = [];
				for (let prop in responseJson) {
					array.push(responseJson[prop]);
				}
		        this.setState({
		        	productListData:ds.cloneWithRows(array),
		        	load:false,
		        });
	  		}else{
	  			this.setState({
	  				isEmpty:true,
	  				load:false,
	  			})
	  		}
	  	})
	}

    passData=(rowData)=>{

    	global.product_id=rowData.entity_id;
		if(rowData.type_id=='simple'&&rowData.has_options==0){
    		global.product_type=rowData.type_id;
        }else if(rowData.type_id=='configurable'){
        	global.product_type=rowData.type_id;        	
        }else if(rowData.type_id=='simple' && rowData.has_options==1){
        	global.product_type='custome';
        }
        global.isUpdate=false;
		this.onClickEvent('ProductScreen');
	};

    _renderRow=(rowData)=>{
    	return(
			<TouchableOpacity onPress={()=>this.passData(rowData)}>
				<View style={styles.container}>
					<View style={styles.imgView}>
						<Image style={styles.imgSize} source = {{ uri:rowData.image_url_thumb}}/>
					</View>
					<View style={styles.nameView}>
			        	<Text numberOfLines={1} style={styles.nameText}>{rowData.name}</Text>
					</View>
					<View style={styles.priceView}>
						<Text numberOfLines={1} style={styles.finalPrice}>₹ {Math.round(rowData.final_price)}</Text>
						<Text numberOfLines={1} style={styles.priceText}>₹ {Math.round(rowData.price)}</Text>
							<View style={styles.checkBoxView}>
								<CheckBox
									checkboxStyle={{width:responsiveHeight(3.5),height:responsiveHeight(3.5)}}
									labelStyle={{color:'white'}}
						          	uncheckedImage={require('../logos/wish-icon.png')}
						          	checkedImage={require('../logos/wish-icon1.png')}
						          	onChange={(data) =>checkData(data,rowData)}
						          	checked={this.state.value}
					          	/>
							</View>
					</View>
				</View>
			</TouchableOpacity>
		)
    }

   	onClickEvent(screenName){
    	this.props.navigation.navigate(screenName);
	};

	render(){
		checkData=(data,rowData)=>{
			var url;
			if(data===false){
				url=urls.wishListAdd+rowData.entity_id

			}else{
				url=urls.wishListDel+rowData.entity_id
			}

			fetchData.getMaster(url).then((responseJson)=>{
				if(responseJson.code==0 && responseJson.msg != null){
					ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
				}else if(responseJson.code==5) {
					Alert.alert(
			            'Sign in',
			            'Register with us and enjoy more feature!',
			            [
			                {
			                    text: 'May be later',
			                    onPress: () => {},
			                    style: 'cancel'
			                },
			                { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
			            ],
			            { cancelable: false }
			        );
				}
			});
		};

		const {goBack} = this.props.navigation;

		return(
			<Container>
		        <ActionBar
		            backgroundColor={'#ffffff'}
		            containerStyle={styles.actionbarContainer}
		            titleContainerStyle={styles.actionbarTitleContainer}
		            leftIconName={'back-icon'}
		            onLeftPress={() => this.onClickEvent('MainScreen')}
		            rightIcons={[
		                {
		                    name: 'filter-icon',
		                    onPress: () => this.onClickEvent('ProductFilterScreen'),
		                },
		            ]}
	            />
				{(this.state.isEmpty)?
					<View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
						<Text style={{fontSize:responsiveFontSize(1.8),color:'#000000'}}>No data found</Text>
						<Text style={{fontSize:responsiveFontSize(1.6)}}>Checkout some other catagory !</Text>
					</View>
					:
				    <ScrollView>
				    	<GridView
					        dataSource={this.state.productListData}
					        numberOfItemsPerRow={2}
					        removeClippedSubviews={false}
					        initialListSize={1}
					        pageSize={2}
					        renderRow={(rowData)=> this._renderRow(rowData)}
				        />
					</ScrollView>
		        }
		        <Footer style={{height:responsiveHeight(8.2)}}>
		        	<FooterTabBar count={this.state.count}/>	
			    </Footer> 

			    <Loader loading={this.state.load}/>
			</Container>
		);
  	}
}