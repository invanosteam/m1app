import React, { Component } from 'react';
import { ToastAndroid,StyleSheet,ActivityIndicator,BackHandler,Alert,Text,ScrollView,TouchableWithoutFeedback,TextInput,Image,AsyncStorage,TouchableOpacity,ToolbarAndroid,View,Dimensions } from 'react-native';
import { Container, Header, Content, Form,Button,Icon,Item,Input,Label,Left,Right } from 'native-base';

import FBSDK, {LoginManager,AccessToken } from 'react-native-fbsdk';

import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import { NavigationActions } from 'react-navigation';

import ActionBar from 'react-native-action-bar';

import Loader from './Loader';

import AppButton from '../components/Button/AppButton';

var {height, width} = Dimensions.get('window');

import styles from '../styles/Login.style';

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class Login extends Component {
    constructor(props) {
    super(props)
        this.state = {
            username: '',
            password:'',
            hidePassword:true,
            count:0,
            accessToken:'',
            clicked:false,
        }
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('Login'));
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    };  

    handleBackButton=()=>{
        Alert.alert(
            'Quiting',
            'Want to Exit?',
            [
                {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel'
                },
                { text: 'Exit', onPress: () => BackHandler.exitApp()}
            ],
            { cancelable: false }
        );
        return true;
    }

    userLogin=()=>{
        const { username,password } = this.state
        var format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(username!='' && password!=''){
            this.setState({clicked:true});
            if(format.test(username)){
                var url=urls.logIn;
                var data={
                    username:username,
                    password:password,
                }
                fetchData.postMaster(url,JSON.stringify(data)).then((responseJson)=>{
                    if(responseJson.code==0){
                        this.getCustomerInfo();
                        this.getCartItemCount();

                    }else if(responseJson.code==1) {
                        this.setState({clicked:false});
                        ToastAndroid.show(responseJson.msg+' Please try again with correct one', ToastAndroid.SHORT);
                    }
                });
            }else{
                this.setState({clicked:false});
                ToastAndroid.show('Invalid UserId', ToastAndroid.SHORT);
            }
        }else{
          ToastAndroid.show('UserId and password required', ToastAndroid.SHORT);
        }
    };

    getCartItemCount(){
        fetchData.getMaster(urls.getCartInfo).then((responseJson)=>{
            if(responseJson.code==1 && responseJson.msg==null){
                global.cartItemCount=responseJson.model.cart_items_count;
            }
        });
    }

    getCustomerInfo(){
        fetchData.getMaster(urls.getCustomerStatus).then((responseJson)=>{
            if(responseJson.model!=null&&responseJson.code==0){
                global.userEmailId=responseJson.model.email;
                try {
                    AsyncStorage.setItem('EmailId',responseJson.model.email);
                    AsyncStorage.setItem('Skip','yes');
                    this.onClickEvent('MainScreen');
                } catch (error) {
                    this.setState({clicked:false});
                    ToastAndroid.show(''+error, ToastAndroid.SHORT);
                }
            }else if(responseJson.code==5){
                this.setState({clicked:false});
                ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
            }
        });
    }

    onClickEvent(screenName){
        this.props.navigation.navigate(screenName);
    }

    onRegisterCilick=()=>{
        this.onClickEvent('Register');
    };

    oneStepLogin=(emailId,firstname,lastname,g)=>{
        var password = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 8; i++){
            password += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        gender=''
        if(g=='male'){
            gender=1;
        }else{
            gender=2;
        }
        // alert('fbData :: '+emailId,firstname,lastname,gender);

        var format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(format.test(emailId) && firstname!= '' && lastname!='' && gender!='' && password!=''){

            var data='pwd='+password+'&email='+emailId+'&firstname='+firstname+'&lastname='+lastname;
            fetchData.postMaster(urls.register,data).then((responseJson)=>{
                if(responseJson.code==0){
                    // alert('DATA : '+JSON.stringify(responseJson))
                    if(responseJson.model[0]!= null && responseJson.model[0]!= ''){
                        global.userEmailId=responseJson.model.email;
                        try {
                            AsyncStorage.setItem('EmailId',responseJson.model.email);
                            this.getCartItemCount();
                            this.onClickEvent('MainScreen');
                        } catch (error) {
                            ToastAndroid.show('Error Msg'+error,ToastAndroid.SHORT);
                        }
                    }else{
                        ToastAndroid.show('Email address already exist please login to procced', ToastAndroid.SHORT);
                        this.onClickEvent('Login');
                    }
                }else{
                    // alert('data : '+JSON.stringify(responseJson))
                    fetchData.postMaster(urls.facbookLogin,JSON.stringify(data)).then((responseJson)=>{
                        if(responseJson.code==0){
                            this.getCustomerInfo();
                            this.getCartItemCount();
                            this.onClickEvent('MainScreen');
                        }else{
                            ToastAndroid.show('Invalid User', ToastAndroid.SHORT);
                        }
                    });
                }
            });
        }else{
            ToastAndroid.show('please try again ',ToastAndroid.SHORT)
        }
    };

    onFBClick=()=>{
        var that = this;
        LoginManager.logInWithReadPermissions(['email']).then(function(result){
            if(result.isCancelled){
                alert('isCancelled : '+result.isCancelled)           
            }else{    
                AccessToken.getCurrentAccessToken().then((data) => {  
                    // urls.facebookUrl+data.accessToken;
                    var url='https://graph.facebook.com/me?fields=email,first_name,last_name&access_token='+data.accessToken
                    fetchData.getMaster(url).then((responseJson)=>{
                        alert('fetchData : '+JSON.stringify(responseJson))
                        if(JSON.stringify(responseJson)!='{}' && responseJson!=null){
                            var e=responseJson.email;
                            var fn=responseJson.first_name;
                            var ln=responseJson.last_name;
                            // var g=responseJson.gender;
                            that.oneStepLogin(e,fn,ln,null);
                        }
                    })
                },function(error){
                    alert('AccessTokenError : '+JSON.stringify(error))
                    // ToastAndroid.show('Something went wrong please try again !', ToastAndroid.SHORT); 
                })
            }
        },function(error){
            alert('LoginManagerError : '+JSON.stringify(error))
            // ToastAndroid.show('Something went wrong please try again !', ToastAndroid.SHORT); 
        });
    };

    onGoogleClick=()=>{

        GoogleSignin.hasPlayServices({ autoResolve: true });
        GoogleSignin.configure({
            scopes: [urls.googleUrl], 
            webClientId:urls.googleClientId,
        }).then((user)=>{
            // alert('user : '+user)
            if(user){
                GoogleSignin.signIn()
                .then((user) => {
                    // alert(JSON.stringify(user))

                    var e=user.email;
                    // var fn=responseJson.first_name;
                    // var ln=responseJson.last_name;
                    ToastAndroid.show('Email Id :' +e, ToastAndroid.SHORT);
                    this.oneStepLogin(e,null,null,null);
                })
                .catch((err) => {
                    alert('Google Error :'+err);
                    ToastAndroid.show('Something went wrong please try again later!', ToastAndroid.SHORT); 
                })
                .done();  
            }
        })
        .catch((err) => {
            ToastAndroid.show('Please try again' + err, ToastAndroid.SHORT);
            // alert('Google Error :'+err.code+','+err.message);
        })
    };

    onSocialClick=()=>{
        ToastAndroid.show('This feature is underdevlopment...!', ToastAndroid.SHORT);
    };

    forgotPassword=()=>{
        this.onClickEvent('ForgotPassword');
    };

    skip=()=>{
        try {
            AsyncStorage.setItem('Skip','yes');
            this.onClickEvent('MainScreen');
        } catch (error) {
            ToastAndroid.show(''+error, ToastAndroid.SHORT);
        }
    };

    changePwdType = () => {
        if(this.state.hidePassword) {
            this.setState({
                hidePassword: false
            });
        }else{
            this.setState({
                hidePassword: true
            });
        }
    };

    render() {
        return(
            <Container style={styles.container}>
                <ScrollView>
                    <View style={styles.skipView}>
                        <View style={styles.titleView}>
                            <Image style={styles.AppIcon} source = {require('../logos/applogo.jpg')}/>
                            <Text style={styles.title}>Sign in</Text>
                        </View>
                        <View style={styles.skiplabel}>
                            <TouchableOpacity onPress={this.skip}>
                                <Text style={{fontSize:responsiveFontSize(1.6)}}>Skip</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    
                    <Form>
                        <View style={styles.inputView}>
                            <Image style={styles.inputIcon} source = {require('../logos/user-icon.png')}/>
                            <Item floatingLabel style={styles.userTextArea}>
                                <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1),color:'#efac27'}}>
                                    Username</Label>
                                <Input style={styles.inputLabel}
                                    keyboardType = 'email-address'
                                    onChangeText={(username) => this.setState({username:username})}/>
                            </Item>
                        </View>

                        <View style={styles.inputView}>
                            <Image style={styles.inputIcon1} source = {require('../logos/lock-icon.png')}/>
                            <Item floatingLabel style={styles.userTextArea}>
                                <Label 
                                style={{fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1),color:'#efac27'}}>
                                    Password
                                </Label>
                                <Input style={styles.inputLabel}
                                    onChangeText={(password) => this.setState({password:password})}
                                    secureTextEntry={this.state.hidePassword}/>
                            </Item>
                                <TouchableWithoutFeedback onPress={this.changePwdType}>
                                    {(this.state.hidePassword)?
                                        <Image style={styles.passwordEyeIcon} source = {require('../logos/hideeye-icon.png')}/>
                                        :
                                        <Image style={styles.passwordEyeIcon} source = {require('../logos/showeye-icon.png')}/>
                                    }
                                </TouchableWithoutFeedback>
                        </View>
                    </Form>
                            

                    <View style={styles.forgotView}>
                        <TouchableOpacity onPress={this.forgotPassword}>
                            <Text style={styles.label}>Forgot Password?</Text>
                        </TouchableOpacity>
                    </View>


                    <View style={styles.buttonView}>
                        <View>
                            <AppButton buttonTitle={'LOGIN'} clicked={this.state.clicked} onPress={this.userLogin}/>
                        </View>
                    </View>
                </ScrollView>


                <View style={styles.socialIconView}>
                    <View style={styles.socialLabelView}>
                        <Text style={styles.socialLabel}>Or sign up with</Text>
                    </View>

                    <TouchableOpacity onPress={this.onFBClick}>
                        <View style={{marginRight:50,}}>
                            <Image style={styles.socialIcon} source = {require('../logos/facebook-icon.png')}/>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.onSocialClick}>
                        <View style={{marginRight:50}}>
                            <Image style={styles.socialIcon} source = {require('../logos/twitter-icon.png')}/>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.onGoogleClick}>
                        <View style={{marginRight:20,}}>
                            <Image style={styles.socialIcon} source = {require('../logos/gplus-icon1.png')}/>
                        </View>
                    </TouchableOpacity>
                </View>
            </Container>
        );
    }
}