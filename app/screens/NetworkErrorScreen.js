import React, {Component} from 'react';

import {View,Text,Image,ScrollView,ActivityIndicator,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import { Container, Content,Button,Footer } from 'native-base';

import FooterTabBar from './FooterTabBar';

var {height, width} = Dimensions.get('window');


export default class NetworkErrorScreen extends Component {
    constructor(){
        super()
        this.state={
            activeTabNo:0,
            clicked:false,
        }
    }
    onClick(){
        this.setState({clicked:true});
    }
    render() {
        return (
            <Container style={styles.container}>
                <View style={styles.mainView}>
                    <Image style={styles.logo} source = {require('../logos/no-network.png')}/>
                    <Text style={styles.labelOffers}>Please check your internet</Text>

                    <View style={{marginTop:10}}>
                        <Button rounded style={styles.button} onPress={()=>this.onClick()}>
                            {(this.state.clicked)?
                                <View style={{alignItems: 'center', justifyContent: 'center' }}>
                                    <ActivityIndicator size={30} color='#ffffff' />
                                </View>
                                :
                                <Text style={styles.buttonText}>Reaload</Text>
                            }
                        </Button>
                    </View>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    width:width,
    height:height,
  },
  mainView:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    // paddingTop:80,
  },
  label:{
    marginBottom:10,
    fontSize: 20,
    justifyContent: 'center',
  },
  labelOffers:{
    marginBottom:10,
    fontSize: 18,
    justifyContent: 'center',
  },
  editText:{
    width:250,
    height: 40
  },
  button: {
    marginTop:15,
    marginBottom: 10,
    width: 200,
    height:50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#201e1f'
  },
  buttonText: {
    color: 'white'

  },
  alternativeLayoutButtonContainer: {
    alignItems: 'center',
    paddingTop:20
  },
  logo:{
    height:250,
    width:250,
    marginLeft:50,
    marginRight:50,
    // marginTop: 50,
    marginBottom: 10,
  }

})

