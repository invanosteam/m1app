import React ,{Component} from 'react';
import {Text,View,Image,ScrollView,ListView,Dimensions,AsyncStorage,Picker,ToastAndroid } from 'react-native'
import {Content,Container,Item,Card,Footer,Button,List,Input,Icon,ListItem,CardItem,Body,Thumbnail} from 'native-base';
import ActionBar from 'react-native-action-bar';

var {height, width} = Dimensions.get('window');

import { NavigationActions } from 'react-navigation';

export default class FetchData extends Component{
    constructor(){
        super()
        this.state={
            count:0
        }
    }

    initGlobal(){
        global.cartItemCount=0;
        global.MyProfileView=0;
        global.filterUrl=null;
        global.firstName=null
        global.category_id=null;
        global.product_id=null;
        global.isUpdate=false;
        global.updateQty=0;
        global.OrderId=null;
    };

    resetIndex(routeName){
        const WelcomeScreen = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: routeName})
            ]
        })
        return WelcomeScreen
    }

    getMaster(url){
        return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
           return responseJson
        })
        .catch((error) => {
            return error
        });
    };

    postMaster(url,data){
        return fetch(url, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
            },
            body:data
        })
        .then((response) => response.json())
        .then((responseJson) =>{
            return responseJson
        })
        .catch((error) => {
            return error
        });
    }
}