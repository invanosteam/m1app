import React ,{Component} from 'react';
import {View,Text,Image,ToastAndroid,ScrollView,Alert,ActivityIndicator,Keyboard,TouchableOpacity,ListView,Button,AsyncStorage,Dimensions,ToolbarAndroid,Picker,TouchableHighlight,StyleSheet,TouchableWithoutFeedback } from 'react-native';
import {Content,Container,Footer,Item,Label,Card,Input,CardItem,Body,Thumbnail} from 'native-base';
import CheckBox from 'react-native-checkbox';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import RadioGroup from 'react-native-custom-radio-group1';

import SelectMultiple from 'react-native-select-multiple';

import ActionBar from 'react-native-action-bar';

import ImageSlider from 'react-native-image-slider';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

import Loader from './Loader';

import urls from '../config/urls';

import styles from '../styles/ProductScreen.style';

import FetchData from './FetchData';

const fetchData = new FetchData();

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class ProductScreen extends Component{
    constructor(){
        super()
        this.state={
            qty:1,
            clicked:false,
            entityId:0,
            name:'',
            price:0,
            regularPrice:0,
            shortDescription:'',
            productimage:'',
            superAttribute:'',
            load:true,
            count:0,
            relatedProducts:ds,
            productImages:ds,
            selectedOption:'',
            array:[],
            configData:[],
            customOptions:[],
            customOptionsId:[],
            customOptionsValue:[],
            customField:'',
            customFieldId:0,
            checkedValue:[],
            customArea:'',
            customAreaId:0,
            customDropDownValue:'',
            customDropDownId:0,
            radioValue:'',
            radioId:0,
            multipleSelectId:'',
            multipleSelectedValue: [],
        }
    }
    componentDidMount() {
        this.getData();
    }


    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('ProductScreen'));
        analytics.addCustomDimension()
    }

    componentWillUnmount(){
        const analytics = new Analytics(urls.AppId, null, { debug: true });
        analytics.hit(new PageHit('ProductList'));
    }

    getData(){
        this.setState({count:global.cartItemCount});
        var url=urls.productDetail+global.product_id;
        if(global.isUpdate && global.updateQty!=0){
            this.setState({qty:global.updateQty});    
        }

        fetchData.getMaster(url).then((responseJson)=>{
            if(responseJson.model!=null){
            // alert(JSON.stringify(responseJson.model.mediaGallery))  
                if(responseJson.model.attribute_options!=null){
                    var collection =[];
                    for (let prop in responseJson.model.attribute_options.collection) {
                        collection.push(responseJson.model.attribute_options.collection[prop]);
                    }

                    var labelsKey=[];
                    for (let prop in responseJson.model.attribute_options.labels_key.Color) {
                        labelsKey.push(responseJson.model.attribute_options.labels_key.Color[prop]);
                    }

                    var array1=[];
                    if(labelsKey.length === collection.length){
                        for (var i = 0; i < labelsKey.length; i++) {
                            array1.push({'label':labelsKey[i],'value':collection[i]})
                        }
                    }
                    
                    this.setState({
                        superAttribute:responseJson.model.attribute_options.label.Color,
                        radioData:array1,
                    }) 
                } 

                if(responseJson.model.custom_options!=null){
                    // alert(JSON.stringify(responseJson.model.custom_options))
                    var customOptionsArray = [];
                    for (let prop in responseJson.model.custom_options) {
                        customOptionsArray.push(responseJson.model.custom_options[prop]);
                    }         
                    this.setState({
                        customOptions:customOptionsArray,
                    })  
                }

                var regularPrice=Math.round(responseJson.model.regular_price_with_tax);
                var price=Math.round(responseJson.model.price);
                global.imageUrl=responseJson.model.mediaGallery[0];

                if(responseJson.model.related!=null && responseJson.model.related!= ''){
                    for (let prop in responseJson.model.related) {
                        this.state.array.push(responseJson.model.related[prop]);
                    }
                 
                    this.setState({
                        relatedProducts:ds.cloneWithRows(this.state.array),
                    });
                }

                this.setState({
                    load:false,
                    entityId:responseJson.model.entity_id,
                    name:responseJson.model.name,
                    price:'₹ '+price,
                    regularPrice:'₹ '+regularPrice,
                    shortDescription:responseJson.model.short_description,
                    productImages:ds.cloneWithRows(responseJson.model.mediaGallery),
                    productimage:responseJson.model.mediaGallery[0],
                });


            }
        })
    }

    changeImage=(rowData)=>{
        this.setState({productimage:rowData})
        global.imageUrl=rowData;
    }

    onClickEvent(screenName){
        this.props.navigation.navigate(screenName);
    };

    addToCart=()=>{
        Keyboard.dismiss();
        this.setState({clicked:true});

        var url;
        if(global.product_type=='simple'){
            url=urls.productAddToCart+this.state.entityId+'/qty/'+this.state.qty;
        }else if(global.product_type=='configurable'){
            url=urls.productAddToCart+this.state.entityId+'/qty/'+this.state.qty+'?'+'super_attribute['+this.state.superAttribute+']='+this.state.selectedOption;
           
        }else if(global.product_type=='custome'){
            multiselect=[]
            if(this.state.multipleSelectedValue!=''){
                for (var i = 0; i < this.state.multipleSelectedValue.length; i++) {
                    multiselect.push('&options['+this.state.multipleSelectId+'][]='+this.state.multipleSelectedValue[i].value);
                } 
            }
            var multipleselectData=multiselect.join('');

            checkbox=[]
            if(this.state.checkedValue!=''){
                for (var i = 0; i < this.state.checkedValue.length; i++) {
                    checkbox.push('&options['+this.state.checkboxId+'][]='+this.state.checkedValue[i].value);
                } 
            }
            var checkBoxData=checkbox.join('');

            url=urls.productAddToCart+this.state.entityId+'/qty/'+this.state.qty+'?options['+this.state.customFieldId+']='+this.state.customField+'&options['+this.state.customAreaId+']='+this.state.customArea+multipleselectData+checkBoxData+'&options['+this.state.radioId+']='+this.state.radioValue+'&options['+this.state.customDropDownId+']='+this.state.customDropDownValue;
        }

        fetchData.getMaster(url).then((responseJson)=>{
            if(responseJson.code==0){
                if(global.isWishListProduct==true){
                    var wishUrl=urls.wishListDel+global.product_id
                    fetchData.getMaster(wishUrl).then((responseJson)=>{
                        if(responseJson.code == 0){
                            global.isWishListProduct=false;
                            this.setState({clicked:false});
                            this.onClickEvent('CartScreen');
                        }else{
                            this.setState({clicked:false});
                            alert(responseJson.msg);
                        }
                    });
                }else{
                    this.setState({clicked:false,});
                    this.onClickEvent('CartScreen');
                }
            }else{
                alert(responseJson.msg);
                this.setState({clicked:false});
            }
        })
    };

    updateCart=()=>{
        this.setState({clicked:true});
        var url;
        if(global.product_type=='simple'){
            var data='cart['+global.itemId+'][qty]='+this.state.qty;

            fetchData.postMaster(urls.cartUpdate,data).then((responseJson)=>{
                if(responseJson.code==0){
                    global.updateQty=0;
                    this.setState({clicked:false});
                    this.onClickEvent('CartScreen');
                }else{
                    this.setState({clicked:false});
                    ToastAndroid.show(''+responseJson.msg,ToastAndroid.SHORT);
                }
            })

        }else{
            if(global.product_type=='configurable'){
                url=urls.productAddToCart+this.state.entityId+'/qty/'+this.state.qty+'?'+'super_attribute['+this.state.superAttribute+']='+this.state.selectedOption;
               
            }else if(global.product_type=='custome'){
                multiselect=[]
                if(this.state.multipleSelectedValue!=''){
                    for (var i = 0; i < this.state.multipleSelectedValue.length; i++) {
                        multiselect.push('&options['+this.state.multipleSelectId+'][]='+this.state.multipleSelectedValue[i].value);
                    } 
                }
                var multipleselectData=multiselect.join('');

                checkbox=[]
                if(this.state.checkedValue!=''){
                    for (var i = 0; i < this.state.checkedValue.length; i++) {
                        checkbox.push('&options['+this.state.checkboxId+'][]='+this.state.checkedValue[i].value);
                    } 
                }
                var checkBoxData=checkbox.join('');

                url=urls.productAddToCart+this.state.entityId+'/qty/'+this.state.qty+'?options['+this.state.customFieldId+']='+this.state.customField+'&options['+this.state.customAreaId+']='+this.state.customArea+multipleselectData+checkBoxData+'&options['+this.state.radioId+']='+this.state.radioValue+'&options['+this.state.customDropDownId+']='+this.state.customDropDownValue;
            }

            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.code==0){
                    this.setState({
                        clicked:false,
                    });
                    global.updateQty=0;
                    this.onClickEvent('CartScreen');
                }else{
                    alert(responseJson.msg);
                    this.setState({clicked:false});
                }
            })
        }
    };

    addToWishList=()=>{
        var url=urls.getCustomerStatus;
        fetchData.getMaster(url).then((responseJson)=>{
            if(responseJson.code==0){
                global.MyProfileView=2;
                this.onClickEvent('MyProfileTab');
            }else if(responseJson.code==5){
                Alert.alert(
                    'Sign in',
                    'Register with us and enjoy more feature!',
                    [
                        {
                            text: 'May be later',
                            onPress: () => {},
                            style: 'cancel'
                        },
                        { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
                    ],
                    { cancelable: false }
                );
            }
        });
    };

    passData=(rowData)=>{
        // alert(JSON.stringify(rowData));

        global.product_id=rowData.entity_id;
        if(rowData.type_id=='simple'&&rowData.has_options==0){
            global.product_type=rowData.type_id;
        }else if(rowData.type_id=='configurable'){
            global.product_type=rowData.type_id;            
        }else if(rowData.type_id=='simple' && rowData.has_options==1){
            global.product_type='custome';
        }
        this.setState({
            load:true,
            array:[],
        })
            this.getData();
        // setTimeout(() => {

        // }, 2500);
    };

    checkData=(data,rowData)=>{
            var url;
            if(data===false){
                url=urls.wishListAdd+rowData.entity_id

            }else{
                url=urls.wishListDel+rowData.entity_id
            }

            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.code==0&&responseJson.msg!=null){
                    this.setState({checked:true})
                    ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
                }else if(responseJson.code==5) {
                    Alert.alert(
                        'Sign in',
                        'Register with us and enjoy more feature!',
                        [
                            {
                                text: 'May be later',
                                onPress: () => {this.setState({checked:false})},
                                style: 'cancel'
                            },
                            { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
                        ],
                        { cancelable: false }
                    );
                }
            })
        };

    render(){
        const {goBack} = this.props.navigation;

        cartClick=()=>{
            this.onClickEvent('CartScreen');
        };

        onSelectionsChange = (multipleSelectedValue,item) => {
            this.setState({multipleSelectId:item.option_id,multipleSelectedValue})
        };

        onSelectionsChangeCheckBox = (checkedValue,item) => {
            this.setState({checkboxId:item.option_id,checkedValue});
        };

        let customOptionView=this.state.customOptions.map(function(item,index){
            if(item.custom_option_type==='field'){
                return(
                    <View style={{alignItems:'center',justifyContent:'center',marginTop:5}} key={index}>
                        <Item floatingLabel style={styles.textArea}>
                            <Label style={{marginLeft:5,fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}>
                                {item.custom_option_title}
                            </Label>
                            <Input 
                                style={styles.inputLabel}
                                onChangeText={(field) => this.setState({customField:field,customFieldId:item.option_id})}
                            />
                        </Item>
                    </View>
                )
            }


            if(item.custom_option_type==='area'){
                return(
                    <View key={index} style={{alignItems:'center',justifyContent:'center',marginTop:10}}>
                        <Item floatingLabel style={styles.textArea}>
                            <Label style={{marginLeft:5,fontSize:responsiveFontSize(1.4),paddingTop:responsiveHeight(1)}}>
                                {item.custom_option_title}
                            </Label>
                            <Input style={styles.inputLabel}
                                multiline={true}
                                numberOfLines={4}
                                onChangeText={(area) => this.setState({customArea:area,customAreaId:item.option_id})}
                            />
                        </Item>
                    </View>
                )
            }

            if(item.custom_option_type==='checkbox'){
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                    array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                    data[i]={label:array[i].title ,value:array[i].option_type_id}
                }

                return(
                    <View style={{marginLeft:10,marginRight:10,marginTop:10}} key={index}>
                        <View>
                            <SelectMultiple
                                labelStyle={{fontSize:responsiveFontSize(1.4)}}
                                checkboxStyle={{height:responsiveHeight(3),width:responsiveHeight(3)}}
                                items={data}
                                selectedItems={this.state.checkedValue}
                                onSelectionsChange={(value)=>onSelectionsChangeCheckBox(value,item)} 
                            />
                        </View>
                    </View>
                )
            }

            if(item.custom_option_type==='drop_down'){
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                    array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                    data[i]={label:array[i].title ,value:array[i].option_type_id}
                }

                let cDataList1=data.map((item,index)=>{
                    return(
                        <Picker.Item key={index} label={item.label} value={item.value}/>
                    )
                });

                return(
                    <View style={{alignItems:'center', justifyContent:'center',marginTop:5}} key={index}>
                        <View style={{width:responsiveWidth(93),}}> 
                            <Picker 
                                style={{width:responsiveWidth(93)}}
                                selectedValue={this.state.customDropDownValue}
                                onValueChange={(itemValue) => this.setState({customDropDownValue: itemValue,customDropDownId:item.option_id})}>
                                {cDataList1}
                            </Picker>
                        </View>
                    </View>
                )
            }

            if(item.custom_option_type==='radio'){
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                    array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                    data[i]={label:array[i].title ,value:array[i].option_type_id}
                }
                return(
                    <View style={{borderWidth:0.4,marginTop:10,padding:10}} key={index}>
                        <RadioForm
                            radio_props={data}
                            animation={false}
                            formHorizontal={true}
                            labelStyle={{fontSize:responsiveFontSize(1.4)}}
                            // style={{marginLeft:80}}
                            onPress={(value) =>this.setState({radioValue:value,radioId:item.option_id})}
                        />
                    </View>
                )
            }

            if(item.custom_option_type==='multiple'){                
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                    array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                    data[i]={label:array[i].title ,value:array[i].option_type_id}
                }

                return(
                    <View style={{marginLeft:10,marginRight:10,marginTop:10}} key={index}>
                        <SelectMultiple
                            labelStyle={{fontSize:responsiveFontSize(1.4)}}
                            checkboxStyle={{height:responsiveHeight(3),width:responsiveHeight(3)}}
                            items={data}
                            selectedItems={this.state.multipleSelectedValue}
                            onSelectionsChange={(value)=>onSelectionsChange(value,item)}
                        />
                    </View>
                )
            }

        },this)

    	return(
    		<Container key={this.state.entityId} style={styles.container}>
                <ActionBar
                backgroundColor={'#ffffff'}
                containerStyle={styles.actionbarContainer}
                titleContainerStyle={styles.actionbarTitleContainer}
                rightTouchableChildStyle={{width:35}}
                leftIconName={'back-icon'}
                onLeftPress={() =>  goBack()}
                rightIcons={[
                    {
                        name: 'wish-icon',
                        onPress: () => this.addToWishList(),
                    },
                    {
                        name: 'cart-icon',
                        badge: this.state.count,
                        onPress: () => cartClick(),
                    },
                ]}/>
    			<ScrollView>
      				{(!this.state.load)?
                        <View style={styles.imgAlign}>
                            <TouchableOpacity onPress={()=>this.onClickEvent('FullScreenImg')}>
                                <Image style={styles.img} source = {{ uri:this.state.productimage}}/>
                            </TouchableOpacity>
                            <ListView
                                horizontal={true}
                                // style={{}}
                                dataSource={this.state.productImages}
                                renderRow={(rowData) => 
                                    <TouchableOpacity onPress={()=>this.changeImage(rowData)}>
                                        <View style={styles.view1}>
                                            <Image style={styles.smallImage} source = {{ uri:rowData}}/>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        :
                        null
                    }
                    {(!this.state.load)?
          				<View style={styles.contentAlign}>
          		            <Text style={styles.nameText}>{this.state.name}</Text>
          				</View>
                        :
                        null
                    }

                    {(!this.state.load)?
                        <View>
                            <View style={styles.priceView}>
                                <Text style={styles.priceText}>{this.state.price}</Text>
                                <Text style={styles.regularPrice}>{this.state.regularPrice}</Text>
                            </View>

                            {(global.product_type=='configurable')?
                                <View style={styles.colorOptions}>
                                    <RadioGroup 
                                        buttonContainerStyle={styles.radioButtonContainer}
                                        buttonTextStyle={styles.radioButtonText}
                                        radioGroupList={this.state.radioData}
                                        onChange={(selected)=> this.setState({selectedOption:selected})}
                                    />
                                </View>
                                :
                                null
                            }
                            
                            <View>
                                <Text style={styles.descriptionTitle}>Description</Text>
                                <Text style={styles.descriptionText}>{this.state.shortDescription}</Text>
                            </View>
                           
                            {(global.product_type=='custome')?
                                <View>
                                    <Text style={styles.descriptionTitle}>Custom Options</Text>
                                    {customOptionView}
                                </View>
                                :
                                null
                            }
                        </View>
                        :
                        null
                    }
                    {(!this.state.load && (this.state.array).length>0)?
                        <View style={{marginTop:10,margin:5,borderColor:'#000000',borderBottomWidth:0.5,borderRightWidth:0.5,}}>
                            <View style={styles.contentAlign}>
                                <Text style={{fontSize:responsiveFontSize(2),color:'#efac27',fontWeight:'bold'}}>Similar Products</Text>
                            </View>
                            <ListView
                                horizontal={true}
                                style={{flex:1,}}
                                dataSource={this.state.relatedProducts}
                                renderRow={(rowData) => 
                                    <TouchableOpacity onPress={()=>this.passData(rowData)}>
                                        <View style={styles.containerRelated}>
                                            <View style={styles.imgView}>
                                                <Image style={styles.imgSize} source = {{ uri:rowData.image_url}}/>
                                            </View>
                                            <View style={styles.nameViewRelated}>
                                                <Text numberOfLines={1} style={styles.nameTextRelated}>{rowData.name}</Text>
                                            </View>
                                            <View style={styles.priceViewRelated}>
                                                <Text numberOfLines={1} style={styles.priceTextRelated}>₹ {Math.round(rowData.price)}</Text>
                                                <View style={styles.checkBoxView}>
                                                    <CheckBox
                                                        checkboxStyle={{width:responsiveHeight(3.5),height:responsiveHeight(3.5)}}
                                                        checked={this.state.checked}
                                                        labelStyle={{color:'white'}}
                                                        uncheckedImage={require('../logos/wish-icon.png')}
                                                        checkedImage={require('../logos/wish-icon1.png')}
                                                        onChange={(data) =>this.checkData(data,rowData)}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        :
                        null
                    }
                    {(!this.state.load && (this.state.array).length>0)?
                        <View style={{margin:5,borderBottomColor:'#000000',borderBottomWidth:0.5,borderRightWidth:0.5,}}>
                            <View style={styles.contentAlign}>
                                <Text style={{fontSize:responsiveFontSize(2),color:'#efac27',fontWeight:'bold'}}>You might be interested in</Text>
                            </View>
                            <ListView
                                horizontal={true}
                                style={{flex:1,}}
                                dataSource={this.state.relatedProducts}
                                renderRow={(rowData) => 
                                    <TouchableOpacity onPress={()=>this.passData(rowData)}>
                                        <View style={styles.containerRelated}>
                                            <View style={styles.imgView}>
                                                <Image style={styles.imgSize} source = {{ uri:rowData.image_url}}/>
                                            </View>
                                            <View style={styles.nameViewRelated}>
                                                <Text numberOfLines={1} style={styles.nameTextRelated}>{rowData.name}</Text>
                                            </View>
                                            <View style={styles.priceViewRelated}>
                                                <Text numberOfLines={1} style={styles.priceTextRelated}>₹ {Math.round(rowData.price)}</Text>
                                                <View style={styles.checkBoxView}>
                                                    <CheckBox
                                                        checkboxStyle={{width:responsiveHeight(3.5),height:responsiveHeight(3.5)}}
                                                        checked={this.state.checked}
                                                        labelStyle={{color:'white'}}
                                                        uncheckedImage={require('../logos/wish-icon.png')}
                                                        checkedImage={require('../logos/wish-icon1.png')}
                                                        onChange={(data) =>this.checkData(data,rowData)}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        :
                        null
                    }

                </ScrollView>
    			<Footer style={styles.footer}>
                    <View style={styles.qtybtn}>
        				<Text style={styles.qtyText}>Qty :</Text>
        				<Picker 
                        style={styles.picker}
                        mode='dropdown'
        				selectedValue={this.state.qty}
        				onValueChange={(itemValue) => this.setState({qty: itemValue})}>
            				<Picker.Item label="1" value = {1}/>
            				<Picker.Item label="2" value = {2}/>
            				<Picker.Item label="3" value = {3}/>
            				<Picker.Item label="4" value = {4}/>
            				<Picker.Item label="5" value = {5} />
                            <Picker.Item label="6" value = {6}/>
                            <Picker.Item label="7" value = {7}/>
                            <Picker.Item label="8" value = {8}/>
                            <Picker.Item label="9" value = {9}/>
        				</Picker>
                    </View>

                    {(!global.isUpdate)?
                        <TouchableWithoutFeedback onPress={this.addToCart}>
                                <View style={styles.footerbutton}>
                                {(this.state.clicked)?
                                    <ActivityIndicator size={responsiveFontSize(3)} color='#efac27'/>
                                    :
                                    <Text style={styles.buttonText}>ADD TO CART</Text>
                                }    
                                </View>
                        </TouchableWithoutFeedback>
                        :
                        <TouchableWithoutFeedback onPress={this.updateCart}>
                                <View style={styles.footerbutton}>
                                {(this.state.clicked)?
                                    <ActivityIndicator size={responsiveFontSize(3)} color='#efac27'/>
                                    :
                                    <Text style={styles.buttonText}>Update</Text>
                                }    
                                </View>
                        </TouchableWithoutFeedback>
                    }
    			</Footer>
                <Loader loading={this.state.load}/>
    		</Container>
        );
    }
}