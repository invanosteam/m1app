import React, {Component} from 'react';

import {View,Text,Image,ScrollView,DrawerLayoutAndroid,ActivityIndicator,TouchableHighlight,StyleSheet,ToolbarAndroid,List,ListView,Dimensions,TouchableWithoutFeedback,AsyncStorage,ToastAndroid} from 'react-native';

import { Container,Card,CardItem, Header,Body, Content,ListItem, Form,Button, Item,Footer,Input,Segment,Label,Left,Right } from 'native-base';

import Loader from './Loader';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import styles from '../styles/OrderPlace.style';

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class OrderPlace extends Component {
    constructor(){
        super()
        this.state={
            paymentMethod:'',
            CartproductListData:ds,
            firstName:'',
            lastName:'',
            address:'',
            street:'',
            city:'',
            postcode:'',
            region:'',
            country:'',
            telephone:'',
            grandTotal:'',
            discountAmount:'',
            subtotal:'',
            shippingAmount:'',
            load:true,
            clicked:false,
        }
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('OrderPlace'));
    }

    componentDidMount() {
        this.getData();
        this.getAddress();
    }

    // componentDidUpdate(){
    //     this.getData()        
    // }

    getData(){
        this.setState({paymentMethod:global.paymentMethod});

        fetchData.getMaster(urls.getCartInfo).then((responseJson)=>{
            if(responseJson.msg==null){
                this.setState({
                    CartproductListData:ds.cloneWithRows(responseJson.model.cart_items),
                    load:false,
                });
            }else if(responseJson.msg!=null){
                var array = [];
            }
        });

    };

    getAddress(){
        fetchData.getMaster(urls.getAddressByQuote).then((responseJson)=>{
            if(responseJson.model!=null && responseJson.code==0 ){
                // alert(responseJson.model.shipping_address.street[0])
                this.setState({
                    firstName:responseJson.model.shipping_address.firstname,
                    lastName:responseJson.model.shipping_address.lastname,
                    address:responseJson.model.shipping_address.street[0],
                    street:responseJson.model.shipping_address.street[1],
                    city:responseJson.model.shipping_address.city,
                    postcode:responseJson.model.shipping_address.postcode,
                    region:responseJson.model.shipping_address.region,
                    country:responseJson.model.shipping_address.country,
                    telephone:responseJson.model.shipping_address.telephone,
                    grandTotal:'₹ '+Math.round(responseJson.model.shipping_address.grand_total),
                    subtotal:'₹ '+Math.round(responseJson.model.shipping_address.subtotal),
                    discountAmount:'₹ '+Math.round(responseJson.model.shipping_address.discount_amount),
                    shippingAmount:'₹ '+Math.round(responseJson.model.shipping_address.shipping_amount),
                });
            }
        });
    }

    placeOrder=()=>{
        this.setState({clicked:true});
        var url=urls.createOrder;
        data='method='+this.state.paymentMethod;
        fetchData.postMaster(url,data).then((responseJson)=>{
            if(responseJson.code==0 && responseJson.model!=null){
                global.order=responseJson.model.increment_id;
                this.setState({clicked:false});
                this.props.navigation.navigate('OrderconfirmScreen');
            }else if(responseJson.code==1){
                alert('ErrorMsg: '+responseJson.msg);
                this.setState({clicked:false});
            }
        })
    };


    _renderRow=(rowData)=>{
        var array = [];
        for (let prop in rowData.custom_option) {
            array.push(rowData.custom_option[prop]);
        }
        
        let att=array.map(function(item,index){
            return(
                <View key={index}>
                    <Text style={styles.text8}>{item.label} : {item.value}</Text>
                </View>
            )
        })
        return (
            <View style={styles.view14}>
                <Image style={styles.productImg} source = {{ uri:rowData.thumbnail_pic_url}}/>
                <View style={styles.productContentView}>
                    <View style={styles.productNameandPriceView}>
                        <View>
                            <Text style={styles.productName}>{rowData.item_title}</Text>
                        </View>

                        <View style={styles.view15}>  
                            {att}
                            <Text style={styles.text9}>Qty : {rowData.qty}</Text>
                        </View>

                        <View style={styles.view6}>
                            <Text style={styles.priceText}>₹ {Math.round(rowData.item_price)}</Text>
                        </View> 
                    </View>     
                </View>
            </View>
        );
    };

    editAddress(){
        this.setState({
            firstName:'',
            lastName:'',
            address:'',
            street:'',
            city:'',
            postcode:'',
            region:'',
            country:'',
            telephone:'',
            grandTotal:'',
            subtotal:'',
            discountAmount:'',
            shippingAmount:'',
        })
        this.props.navigation.navigate('Checkout');
    }

    editPaymentMethod(){
        this.props.navigation.navigate('PaymentMethod');   
    };
  
    render() {
        this.getAddress();

        return (
            <Container style={styles.container}>
                <View>
                    <View style={styles.view1}>
                        <View style={styles.view2}>
                            <Text style={styles.text1}>Shipping to</Text>
                        </View>
                        <TouchableWithoutFeedback onPress={()=>this.editAddress()}>
                            <View style={styles.view3}>
                                <View style={styles.view4}>
                                    <Text style={styles.text2}>Edit</Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    <View style={styles.view5}>
                        <View style={styles.view6}>
                            <Text style={styles.text3}>{this.state.firstName} </Text>
                            <Text style={styles.text4}>{this.state.lastName}</Text>
                        </View>
                        <View>
                            <Text style={styles.text3}>{this.state.address},</Text>
                            <Text style={styles.text4}>{this.state.street}</Text>
                        </View>
                        <View style={styles.view7}>
                            <Text style={styles.text3}>{this.state.city},</Text>
                            <Text style={styles.text4}>{this.state.postcode}.</Text>
                            <Text style={styles.text4}>{this.state.region}.</Text>
                            <Text style={styles.text4}>{this.state.country}.</Text>
                            <Text numberOfLines={2} style={styles.text4}>{this.state.telephone}.</Text>
                        </View>
                    </View>
                </View>


                <View style={styles.view8}>
                    <View style={styles.view2}>
                        <Text style={styles.text1}>Your Order</Text>
                    </View>
                </View>
                    <ScrollView style={styles.scrollView1}>
                        <ListView
                        dataSource={this.state.CartproductListData}
                        renderRow={(rowData) => this._renderRow(rowData)}
                        />
                    </ScrollView>
                    <View>
                        <View style={styles.view9}>
                            <View style={styles.view2}>
                                <Text style={styles.text1}>Payment summary</Text>
                            </View>
                            <TouchableWithoutFeedback onPress={()=>this.editPaymentMethod()}>
                                <View style={styles.view3}>
                                    <View style={styles.view4}>
                                        <Text style={styles.text2}>Edit</Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>

                        <View style={styles.view10}>
                            <View style={styles.view8}>
                                <View style={styles.view2}>
                                    <Text style={styles.text3}>Order total</Text>
                                </View>
                            <View>
                                <View style={styles.view11}>
                                    <Text style={styles.text5}>{this.state.subtotal}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.view9}>
                            <View style={styles.view2}>
                                <Text style={styles.text3}>Delivery charge</Text>
                            </View>
                            <View>
                                <View style={styles.view11}>
                                    <Text style={styles.text5}>{this.state.shippingAmount}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.view9}>
                            <View style={styles.view2}>
                                <Text style={styles.text3}>Discount Amount</Text>
                            </View>
                            <View>
                                <View style={styles.view11}>
                                    <Text style={styles.text5}>{this.state.discountAmount}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.view12}>
                            <View style={styles.view2}>
                                <Text style={styles.text6}>Total Amount</Text>
                            </View>
                            <View>
                                <View style={styles.view11}>
                                    <Text style={styles.text7}>{this.state.grandTotal}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>

                <Footer style={styles.footerBackGround}>
                    <TouchableWithoutFeedback onPress={this.placeOrder}>
                         {(this.state.clicked)?
                            <ActivityIndicator size={responsiveFontSize(2.5)} color='#efac27'/>
                            :
                            <View style={styles.footerButton}>
                                <View style={styles.view13}>
                                    <Text style={styles.buttonText}>ORDER NOW</Text>
                                </View>
                                <View style={styles.view3}>
                                    <View style={styles.view4}>
                                        <Image style={styles.footerIcon} source = {require('../logos/forward-icon.png')}/>
                                    </View>
                                </View>
                            </View>
                        }
                    </TouchableWithoutFeedback>
                </Footer>
                <Loader loading={this.state.load}/>
            </Container>
        );
    }
}