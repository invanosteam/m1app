import React, {Component} from 'react';

import {View,Text,Image,Button,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,
	ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import {Content,Container,Item,Card,CardItem,Body,Thumbnail,Footer} from 'native-base';

import FooterTabBar from './FooterTabBar';

import EmptySavedAddressScreen from './EmptySavedAddressScreen';


var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import urls from '../config/urls';

import styles from '../styles/MySavedAddress.style';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class MySavedAddress extends Component {
    constructor(props){
        super(props)
        this.state={
            myData:[],
            myAddressData:ds,
            activeTabNo:4,
            subTotal:0,
            shippingAmount:0,
            totalQty:0,
            isEmpty:false,
            count:0,
        }
    }
    componentDidMount() {
        this.getUserOrderInfo();
    }

    getUserOrderInfo(){
        this.setState({count:global.cartItemCount});

        fetchData.getMaster(urls.getAddressList).then((responseJson)=>{
            if(responseJson.code===0 ){
                if(responseJson.model!=''){
                    var array =[];
                    for (let prop in responseJson.model) {
                        array.push(responseJson.model[prop]);
                    }
                    this.setState({
                        isEmpty:false,
                        myAddressData:ds.cloneWithRows(array),
                    }); 
                }else{
                    this.setState({isEmpty:true});
                }
            }else{
                ToastAndroid.show('Please try again getOrderList'+responseJson.msg, ToastAndroid.SHORT);
            }
        })
    }


    editAddress=(rowData)=>{
        // this.props.navigation.navigate('')
        ToastAndroid.show(""+rowData.entity_id,ToastAndroid.SHORT);     
    };

    render() {
        if(this.state.isEmpty){
            return(<EmptySavedAddressScreen onClick={this.props.onClick}/>);
        }else{
            return (
                <Container style={styles.container}>
                    <ScrollView>
                      <ListView
                        dataSource={this.state.myAddressData}
                        renderRow={(rowData) =>
                            <View style={styles.view1}>
                                <View style={styles.view2}>
                                    <View style={styles.view3} >
                                        <Text style={styles.text2} >{rowData.name}</Text>
                                        <View style={styles.view4}>
                                            <Text style={styles.text1}>{rowData.street[0]} ,{rowData.street[1]}</Text>
                                            <Text style={styles.text1}>{rowData.city}-{rowData.postcode} ,{rowData.region} ,{rowData.country} ,{rowData.telephone}.</Text>
                                        </View>
                                    </View>
                                    <TouchableOpacity onPress={()=>this.editAddress(rowData)} >
                                        <View style={styles.view5}>
                                            <Text style={styles.text3}>Edit</Text>
                                        </View>  
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }/>
                    </ScrollView>
                    <Footer style={styles.footer}>
                        <FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>
                    </Footer>
              </Container>
            );
        }
    }
}
