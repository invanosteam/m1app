import React, { Component } from 'react';
import {View,Text,Image,ScrollView,ActivityIndicator,DrawerLayoutAndroid,Keyboard,TouchableHighlight,StyleSheet,Dimensions,
  ToolbarAndroid,List,ListView,TouchableWithoutFeedback,AsyncStorage,ToastAndroid  } from 'react-native';
import {Content,Card,CardItem,Button,Container,Footer,Item,Icon,Label,Input,Body, Right,Thumbnail} from 'native-base';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import Loader from './Loader';

import RadioButton from 'radio-button-react-native';

var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import styles from '../styles/PaymentMethod.style';

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class PaymentMethod extends Component {
    constructor(){
        super()
        this.state={
            paymentMethods:ds,
            name:'',
            rowData:[],
            id:'',
            grandTotal:0,
            discountAmount:0,
            load:true,
            clicked:false,
            PromoCode:'',
            radioValue:'',
        }
    }

    componentDidMount() {
        this.getData();
    };

    getData(){
        
        fetchData.getMaster(urls.getPaymentList).then((responseJson)=>{
            if(responseJson.model!==null){
                var array =[];
                for (let prop in responseJson.model) {
                    array.push(responseJson.model[prop]);
                }
                this.setState({
                    paymentMethods:ds.cloneWithRows(array),
                    load:false,
                })
            }
        });

        fetchData.getMaster(urls.getAddressByQuote).then((responseJson)=>{
             if(responseJson.model!==null && responseJson.code!==1){
                this.setState({
                    grandTotal:'₹ '+Math.round(responseJson.model.shipping_address.grand_total),
                    discountAmount:'₹ '+Math.round(responseJson.model.shipping_address.discount_amount),
                });
            }
        });
    };


    setPaymentMethod=()=>{
        this.setState({clicked:true,});
        if(this.state.paymentMethodCode!=null){
            var url=urls.setPayMethod;
            var data='payment[method]='+this.state.paymentMethodCode;

            fetchData.postMaster(url,data).then((responseJson)=>{
                if(responseJson.msg===this.state.paymentMethodCode){
                    this.setState({clicked:false});

                    this.props.navigation.navigate('OrderPlace');
                }else{
                    alert('error::'+responseJson.msg);
                    this.setState({clicked:false});
                }
            });

        }else{
            ToastAndroid.show('Please select valid payment method..!', ToastAndroid.SHORT);
            this.setState({clicked:false,});
        }
    }

    usePromoCode=()=>{
        Keyboard.dismiss();
        this.setState({load:true,});

        var url=urls.applyCoupon;
        var data='coupon_code='+this.state.PromoCode;
        fetchData.postMaster(url,data).then((responseJson)=>{
            if(responseJson.code===0){
                this.getData();
                alert(responseJson.msg)
                this.setState({load:false,});
            }else{
                alert(responseJson.msg)
                this.setState({load:false});
            }
        });
    };

    render(){
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('PaymentMethod'));

        setPaymentData=(rowData)=>{
            this.setState({
                paymentMethodCode:rowData.code,
                radioValue:rowData.code
            })
            global.paymentMethodCode=rowData.code
        };

        return(
            <Container style={styles.container}>
                <View style={styles.promoBg}>
                    <Text style={styles.promoTitle}>Promo code</Text>
                    <View style={styles.promoButtonView}>
                        <View style={styles.promoInputView}>
                            <Item fixedLabel>
                                <Input 
                                style={styles.promoInput}
                                placeholder='#1234'
                                onChangeText={(promocode) => this.setState({PromoCode:promocode})}
                                />
                            </Item>
                        </View>
                        <Button style={styles.promoButton} onPress={this.usePromoCode}>
                            <Text style={styles.promoButtonText}>APPLY</Text>
                        </Button>
                    </View>
                </View>

                <ScrollView>
                    <ListView
                    dataSource={this.state.paymentMethods}
                    renderRow={(rowData) =>
                        <TouchableWithoutFeedback onPress={()=>setPaymentData(rowData)}>
                            <View style={styles.titleView}>
                                <Text style={styles.titleText}>{rowData.title}</Text>
                                <View style={styles.radioView}>
                                    <RadioButton
                                        currentValue={this.state.radioValue}
                                        value={rowData.code}
                                        onPress={()=>setPaymentData(rowData)}
                                        outerCircleWidth={2}
                                        outerCircleSize={20}
                                        innerCircleSize={12}
                                    />
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    }/>
                </ScrollView>

                <View style={styles.discountTotalView}>
                    <Text style={styles.discountTotalAmountText}>Discount Amount</Text>
                    <Text style={styles.discountTotalPriceText}>{this.state.discountAmount}</Text>
                </View>


                <View style={styles.totalView}>
                    <Text style={styles.totalAmountText}>Total Amount</Text>
                    <Text style={styles.totalPriceText}>{this.state.grandTotal}</Text>
                </View>

                <Footer style={styles.footerBackGround}>
                    <TouchableWithoutFeedback onPress={this.setPaymentMethod}>
                        {(this.state.clicked)?
                            <ActivityIndicator size={responsiveFontSize(2.5)} color='#efac27'/>
                            :
                            <View style={styles.footerButton}>
                                <View style={styles.buttonTextView}>
                                    <Text style={styles.buttonText}>CONTINUE TO CONFIRMATION</Text>
                                </View>
                                <View style={styles.buttonImgViewContainer}>
                                    <View style={styles.buttonImgView}>
                                        <Image style={styles.buttonImg} source = {require('../logos/forward-icon.png')}/>
                                    </View>
                                </View>
                            </View>
                        }
                    </TouchableWithoutFeedback>
                </Footer>
                <Loader loading={this.state.load}/>
            </Container>
        );
    }
}