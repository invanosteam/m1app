import React, {Component} from 'react';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import MaskTabBar from 'react-native-scrollable-tab-view-mask-bar';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import ProfileScreen from'./ProfileScreen';
import MyOrderScreen from'./MyOrderScreen';
import OrderInfoScreen from'./OrderInfoScreen';
import WishListScreen from'./WishListScreen';
import MySavedAddress from'./MySavedAddress';

export default class MyProfileTab extends Component {
    constructor(){
        super()
        this.state={
            initialPage:0,
        }
    }

    onClick=(screenName)=>{
        this.props.navigation.navigate('MainScreen');
    };

    OrderClick=()=>{
        this.props.navigation.navigate('OrderInfoScreen');
    };

    openProduct=()=>{
        this.props.navigation.navigate('ProductScreen');  
    }

    render() {
        return (
            <ScrollableTabView 
                renderTabBar={() => <MaskTabBar textStyle={{fontSize:responsiveFontSize(1.6)}} someProp={'here'} showMask={true} maskMode='light' />}
                tabBarBackgroundColor='#ffffff'
                initialPage={global.MyProfileView}
                tabBarActiveTextColor='#201e1f'
                tabBarInactiveTextColor='#969696'
                tabBarUnderlineStyle={{backgroundColor:'#efac27'}}>
                    <ProfileScreen tabLabel="Profile" />
                    <MyOrderScreen tabLabel="My order" OrderClick={this.OrderClick} onClick={this.onClick}/>
                    <WishListScreen tabLabel="Wishlist"  onClick={this.onClick} openProduct={this.openProduct}/>
                    <MySavedAddress tabLabel="Saved address" onClick={this.onClick}/>
            </ScrollableTabView>
        );
    }
}