import React from 'react';
import { ToastAndroid, Button,StyleSheet, Text, TextInput,Image, TouchableOpacity,ToolbarAndroid,View,Dimensions } from 'react-native';
import { StackNavigator,TabNavigator ,DrawerNavigator} from 'react-navigation';

var {height, width} = Dimensions.get('window'); 
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


import SplashScreen from '../screens/SplashScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import Login from '../screens/Login';
import Register from '../screens/Register';
import ForgotPassword from '../screens/ForgotPassword';

import MainScreen from '../screens/MainScreen';
import ProductList from'../screens/ProductList';
import ProductFilterScreen from'../screens/ProductFilterScreen';

import ProductScreen from'../screens/ProductScreen';

import FullScreenImg from '../screens/FullScreenImg';
import CartScreen from '../screens/CartScreen';

import AddressScreen from '../screens/AddressScreen';
import SavedAddressScreen from '../screens/SavedAddressScreen';

import PaymentMethod from '../screens/PaymentMethod';
import OrderPlace from '../screens/OrderPlace';
import OrderconfirmScreen from '../screens/OrderconfirmScreen';

import ProfileScreen from'../screens/ProfileScreen';
import SearchScreen from '../screens/SearchScreen';
import MyOrderScreen from'../screens/MyOrderScreen';
import OrderInfoScreen from'../screens/OrderInfoScreen';
import EmptyOrderScreen from'../screens/EmptyOrderScreen';
import WishListScreen from'../screens/WishListScreen';
import EmptyWishListScreen from'../screens/EmptyWishListScreen';
import MySavedAddress from'../screens/MySavedAddress';
import EmptySavedAddressScreen from'../screens/EmptySavedAddressScreen';

import InvoiceScreen from'../screens/InvoiceScreen';
import EmptyInvoiceScreen from '../screens/EmptyInvoiceScreen';
import ShipmentScreen from'../screens/ShipmentScreen';
import EmptyShipmentScreen from '../screens/EmptyShipmentScreen';
import CreditMemoScreen from'../screens/CreditMemoScreen';
import EmptyCreditMemoScreen from '../screens/EmptyCreditMemoScreen';

import FooterTabBar from '../screens/FooterTabBar';

import NetworkErrorScreen from '../screens/NetworkErrorScreen';

import MyProfileTab from'../screens/MyProfileTab';

// import CustomeProductScreen from'../screens/CustomeProductScreen';
// import ConfigProductScreen from '../screens/ConfigProductScreen';
// import EmptyCartScreen from '../screens/EmptyCartScreen';
// import ProductUpdateScreen from'../screens/ProductUpdateScreen';
// import ConfigProductUpdateScreen from '../screens/ConfigProductUpdateScreen';
// import CustomeProductUpdateScreen from'../screens/CustomeProductUpdateScreen';
// import SetShippingAddress from '../screens/SetShippingAddress';
// import LoginScreen from '../screens/LoginScreen';
// import SubSubCategoryList from '../screens/SubSubCategoryList';
// import WishList from'../screens/WishList';
// import MyAccountScreen from'../screens/MyAccountScreen';
// import HomeScreen from '../screens/HomeScreen';
// import CategoryList from'../screens/CategoryList';


export const TestScreen = StackNavigator({
    ForgotPassword: {
        screen: ForgotPassword,
        navigationOptions: {
            header:null
        },
    },
});

export const LoginTab = TabNavigator({
    Login:{
        screen:Login,
        navigationOptions: {
            tabBarLabel: 'Sign in',
            header: null
        },
    },
    Register:{
        screen:Register,
        navigationOptions:{
            tabBarLabel:'Sign Up',
            header: null
        },
    },
},{
    tabBarVisible:false,
    tabBarOptions: {
        labelStyle: {
            fontSize: responsiveFontSize(1.6),
            fontWeight: 'bold',
            color:'#201e1f',
        },
        indicatorStyle:{
            width:responsiveWidth(10),
            backgroundColor:'#efac27',
            marginLeft:responsiveWidth(20),
        },
        style: {
            backgroundColor: '#ffffff',
        },
    }

});

// export const MyAccountTab = TabNavigator({
//     ProfileScreen: {
//         screen: ProfileScreen,
//         navigationOptions: {
//             title: 'Profile',
//         },
//     },
//     MyOrderScreen:{
//         screen:MyOrderScreen,
//         navigationOptions:{
//             title:'My order',
//         },
//     },
//     WishListScreen: {
//         screen: WishListScreen,
//         navigationOptions: {
//             title: 'Wishlist',
//         },
//     },
//     MySavedAddress: {
//         screen: MySavedAddress,
//         navigationOptions: {
//             title: 'Saved Address',
//         },
//     },
// },{
//     tabBarOptions: {
//         upperCaseLabel :false,
//         labelStyle: {
//             fontSize: 16,
//             fontWeight: 'bold',
//             color:'#201e1f',
//         },
//         indicatorStyle:{
//             width:40,
//             backgroundColor:'#efac27',
//             marginLeft:width/9,
//         },
//         style: {
//             backgroundColor: '#ffffff',
//         },
//     }
// });

export const Address= TabNavigator({
    AddressScreen: {
        screen: AddressScreen,
        navigationOptions: {
            title: 'New Address',
            header:null,
        },
    },
    SavedAddressScreen: {
        screen: SavedAddressScreen,
        navigationOptions: {
            title: 'Saved Address',
            header:null,
        },
    },
},{
    swipeEnabled: false,
    lazyLoad: true,
    animationEnabled: false,
    tabBarOptions: {
        upperCaseLabel :false,
        labelStyle: {
            fontSize: responsiveFontSize(1.6),
            fontWeight: 'bold',
            color:'#201e1f',
        },
        indicatorStyle:{
            // width:responsiveWidth(5),
            backgroundColor:'#efac27',
            // marginLeft:width/9,
            // marginLeft:responsiveWidth(20),
        },
        tabStyle:{
            // width:width/3,
        },
        style: {
            // width:width/1.5,
            // marginTop:15,
            // marginBottom:15,
            // marginLeft:width/6,
            // borderRadius:10,
            backgroundColor: '#ffffff',
        },
    }
});

export const Checkout = TabNavigator({
    Address: {
        screen: Address,
        navigationOptions:{
            title: 'Shipping',
            header:null,
        },
    },
    PaymentMethod: {
        screen: PaymentMethod,
        navigationOptions: {
            title: 'Payment',
            header:null,
        },
    },
    OrderPlace: {
        screen: OrderPlace,
        navigationOptions: {
            title: 'Confirmation',
        },
    },
},{
    swipeEnabled: false,

    lazyLoad: true,
    animationEnabled: false,
    tabBarOptions: {
        // onTabPress: () => {},
        upperCaseLabel :false,
        labelStyle: {
            fontSize: responsiveFontSize(1.6),
            fontWeight: 'bold',
            color:'#201e1f',
        },
        indicatorStyle:{
            backgroundColor:'#efac27',
        },
        style: {
            backgroundColor:'#ffffff',
        },
    }
});

export const Root = StackNavigator({
    SplashScreen: {
        screen: SplashScreen,
        navigationOptions: {
            // title: 'SplashScreen',
            header: null
        },
    },
    WelcomeScreen: {
        screen: WelcomeScreen,
        navigationOptions: {
            // title: 'WelcomeScreen',
            header: null
            },
    },
    MainScreen:{
        screen:MainScreen,
        navigationOptions:{
            // title:'MainScreen',
            header:null
        },
    },
    LoginTab: {
        screen: LoginTab,
    },
    ForgotPassword:{
        screen:ForgotPassword,
        navigationOptions:{
            // title:'ForgotPassword',
            header: null
        },
    },
    FooterTabBar: {
        screen: FooterTabBar,
        navigationOptions: {
            header: null
        },
    },
    SearchScreen:{
        screen:SearchScreen,
        navigationOptions:{
            header: null,
        },
    },
    ProductList: {
        screen: ProductList,
        navigationOptions: {
            // title: 'ProductList',
            header:null
        },
    },
    ProductFilterScreen:{
        screen:ProductFilterScreen,
        navigationOptions:{
            // title:'ProductFilterScreen',
            header:null
        },
    },
    ProductScreen: {
        screen: ProductScreen,
        navigationOptions:{
            // title: 'ProductScreen',
            header:null
        },
    },
    // ConfigProductScreen: {
    //     screen: ConfigProductScreen,
    //     navigationOptions: {
    //         // title: 'ConfigProductScreen',
    //         header:null
    //     },
    // },
    // CustomeProductScreen: {
    //     screen: CustomeProductScreen,
    //     navigationOptions: {
    //         header:null
    //     },
    // },
    // ProductUpdateScreen: {
    //     screen: ProductUpdateScreen,
    //     navigationOptions: {
    //         header:null
    //     },
    // },
    // EmptyCartScreen: {
        // screen: EmptyCartScreen,
        // navigationOptions: {
            // title: 'EmptyCartScreen',
        // },
    // },
    CartScreen: {
        screen: CartScreen,
        navigationOptions: {
            header:null
        },
    },
    FullScreenImg: {
        screen: FullScreenImg,
        navigationOptions: {
            header:null
        },
    },
    Checkout: {
        screen: Checkout,
        navigationOptions: {
            header:null
        },
    },
    MyProfileTab: {
        screen: MyProfileTab,
        navigationOptions: {
            header:null
        },
    },
    // ProfileScreen: {
    //     screen: ProfileScreen,
    //     navigationOptions: {
    //         title: 'Profile',
    //     },
    // },
    // MyOrderScreen:{
    //     screen:MyOrderScreen,
    //     navigationOptions:{
    //         title:'My order',
    //     },
    // },
    // WishListScreen: {
    //     screen: WishListScreen,
    //     navigationOptions: {
    //         header:null
    //     },
    // },
    // MySavedAddress: {
    //     screen: MySavedAddress,
    //     navigationOptions: {
    //         title: 'Saved Address',
    //     },
    // },
    EmptyOrderScreen: {
        screen: EmptyOrderScreen,
        navigationOptions: {
            header:null
        },
    },
    EmptySavedAddressScreen: {
        screen: EmptySavedAddressScreen,
        navigationOptions: {
            header:null
        },
    },
    EmptyWishListScreen: {
        screen: EmptyWishListScreen,
        navigationOptions: {
            header:null
        },
    },
    InvoiceScreen: {
        screen: InvoiceScreen,
        navigationOptions: {
            header:null
        },
    },
    ShipmentScreen: {
        screen: ShipmentScreen,
        navigationOptions: {
            header:null
        },
    },
    CreditMemoScreen: {
        screen: ShipmentScreen,
        navigationOptions: {
            header:null
        },
    },
    EmptyCreditMemoScreen: {
        screen: EmptyCreditMemoScreen,
        navigationOptions: {
        header:null
        },
    },
    EmptyShipmentScreen: {
        screen: EmptyShipmentScreen,
        navigationOptions: {
            header:null
        },
    },
    EmptyInvoiceScreen: {
        screen: EmptyInvoiceScreen,
        navigationOptions: {
            header:null
        },
    },
    OrderInfoScreen: {
        screen: OrderInfoScreen,
        navigationOptions: {
            // title: 'Order Information',
            header:null
        },
    },
    OrderconfirmScreen:{
        screen: OrderconfirmScreen,
        navigationOptions: {
            // title: 'OrderconfirmScreen',
            header:null
        },
    },
}, {
  // mode: 'modal',
});



// export const SplashStack = StackNavigator({

//   SplashScreen: {
//     screen: SplashScreen,
//     navigationOptions: {
//     title: 'SplashScreen',
//     header: null
//     },
//   },
//   WelcomeScreen: {
//     screen: WelcomeScreen,
//     navigationOptions: {
//     title: 'WelcomeScreen',
//     header: null
//     },
//   },
//   Login:{
//     screen:Login,
//     navigationOptions: {
//     title: 'Sign in',
//     headerLeft: null
//     },
//   },
//    Register:{
//     screen:Register,
//     navigationOptions:{
//     title:'Sign Up',
//     },
//   },
//   ForgotPassword:{
//     screen:ForgotPassword,
//     navigationOptions:{
//     title:'ForgotPassword',
//     },
//   },
//    MainScreen:{
//     screen:MainScreen,
//     navigationOptions:({navigation})=>({
//     title:'MainScreen',
//   })
//   },
//    SubCategoryList:{
//     screen:SubCategoryList,
//     navigationOptions:{
//     title:'SubCategoryList',
//     },
//   },
//   SubSubCategoryList:{
//     screen:SubSubCategoryList,
//     navigationOptions:{
//     title:'SubSubCategoryList',
//     },
//   },
//   //  CategoryList:{
//   //   screen:CategoryList,
//   //   navigationOptions:{
//   //   title:'CategoryList',
//   //   },
//   // },
//    HomeScreen:{
//     screen:HomeScreen,
//     navigationOptions:{
//     title:'HomeScreen',
//     },
//   },
//   ProductList: {
//     screen: ProductList,
//     navigationOptions: {
//     title: 'ProductList',
//     },
//   },
//   ProductScreen: {
//     screen: ProductScreen,
//     navigationOptions:({navigation})=> ({
//     title: 'ProductScreen',
//     })
//   },
//   ConfigProductScreen: {
//     screen: ConfigProductScreen,
//     navigationOptions: {
//     title: 'ConfigProductScreen',
//     },
//   },
//   EmptyCartScreen: {
//     screen: EmptyCartScreen,
//     navigationOptions: {
//     title: 'EmptyCartScreen',
//     },
//   },
//   CartScreen: {
//     screen: CartScreen,
//     navigationOptions: {
//     title: 'CartScreen',
//     },
//   },
//   FullScreenImg: {
//     screen: FullScreenImg,
//     navigationOptions: {
//     title: 'FullScreenImg',
//     },
//   },
//   AddressScreen: {
//     screen: AddressScreen,
//     navigationOptions: {
//     title: 'AddressScreen',
//     },
//   },
//   // ShippingMethod: {
//   //   screen: ShippingMethod,
//   //   navigationOptions: {
//   //   title: 'ShippingMethod',
//   //   },
//   // },
//   PaymentMethod: {
//     screen: PaymentMethod,
//     navigationOptions: {
//     title: 'PaymentMethod',
//     },
//   },
//   OrderPlace: {
//     screen: OrderPlace,
//     navigationOptions: {
//     title: 'OrderPlace',
//     },
//   },
//   MyAccountScreen: {
//     screen: MyAccountScreen,
//     navigationOptions: {
//     title: 'MyAccountScreen',
//     },
//   },
//   //  SecondScreen:{
//   //   screen:SecondScreen,
//   //   navigationOptions:{
//   //   title:'SecondScreen',
//   //   },
//   // },
// });





// export const DrawerDemo = DrawerNavigator({

//   SplashScreen: {
//     screen: SplashScreen,
//     navigationOptions: {
//     title: 'SplashScreen',
//     },
//   },
//   Login:{
//     screen:Login,
//     navigationOptions: {
//     title: 'Login',
//     headerLeft: null
//     },
//   },
//    Register:{
//     screen:Register,
//     navigationOptions:{
//     title:'Register',
//     },
//   },
//    MainScreen:{
//     screen:MainScreen,
//     navigationOptions:{
//     title:'MainScreen',
//     headerLeft: null
//     },
//   },
//   //  CategoryList:{
//   //   screen:CategoryList,
//   //   navigationOptions:{
//   //   title:'CategoryList',
//   //   },
//   // },
//    HomeScreen:{
//     screen:HomeScreen,
//     navigationOptions:{
//     title:'HomeScreen',
//     },
//   },
//   ProductScreen: {
//     screen: ProductScreen,
//     navigationOptions: {
//     title: 'ProductScreen',
//     },
//   },
//   FullScreenImg: {
//     screen: FullScreenImg,
//     navigationOptions: {
//     title: 'FullScreenImg',
//     },
//   },
//    SecondScreen:{
//     screen:SecondScreen,
//     navigationOptions:{
//     title:'SecondScreen',
//     },
//   },
// },
// {
//   initialRouterName:'MainScreen',
//   drawerPosition:'left',
//   drawerWidth:250
// });

