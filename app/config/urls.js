
const coreUrl = 'http://stageapp.lazyjojo.com';
const baseUrl = coreUrl+'/xapi/';
const version = 'v1';
const bannerImage = coreUrl+'/home-banner'; 

const AppId='UA-110543062-1';

export default urls={
	baseUrl:baseUrl,
	AppId:AppId,

	logIn:baseUrl+'customer/login',
	facbookLogin:baseUrl+'customer/facebookLogin',
	logOut:baseUrl+'customer/logout',
	register:baseUrl+'customer/register',
	updateAccountInfo:baseUrl+'customer/updateAccount/',
	updatePassword:baseUrl+'customer/updatePassword/',
	getCustomerStatus:baseUrl+'customer/status',
	forgotPassword:baseUrl+'customer/forgotpwd',

	getWishlist:baseUrl+'wishlist/getWishlist',
	wishListAdd:baseUrl+'wishlist/add/product_id/',
	wishListDel:baseUrl+'wishlist/del/product_id/',

	searchUrl:baseUrl+'search/index/q/',

	createOrder:baseUrl+'order/createOrder',
	getOrderList:baseUrl+'order/getOrderList',
	getOrderInfo:baseUrl+'order/getOrder/order_id/',
	
	productDetail:baseUrl+'products/getProductDetail/product_id/',

	productFilter:baseUrl+'categories/getActiveFilters/category_id/',
	getFilteredProduct:baseUrl+'categories/getfiltereddata/id/',
	getCategoryTree:baseUrl+'categories/getCategoryTree',
	getCategory:baseUrl+'categories/getproducts/id/',

	getAddressList:baseUrl+'address/getAddressList',
	getSingleAddress:baseUrl+'address/getAddress/address_id/',
	


	getCountry:baseUrl+'checkout/getcountries',
	getRegions:baseUrl+'checkout/getRegions/country_id/',
	getShippingMethod:baseUrl+'checkout/getShippingMethodsList',
	setShippingMethod:baseUrl+'checkout/setShippingMethod',
	setBillingAddress:baseUrl+'checkout/setBilling',
	setShippingAddress:baseUrl+'checkout/setShipping',
	getPaymentList:baseUrl+'checkout/getPayMethodsList',
	getAddressByQuote:baseUrl+'checkout/getAddressByQuote',
	setPayMethod:baseUrl+'checkout/setPayMethod',

	applyCoupon:baseUrl+'cart/useCoupon',
	productAddToCart:baseUrl+'cart/add/product_id/',
	getCartInfo:baseUrl+'cart/getCartInfo',
	getCartTotal:baseUrl+'cart/getCartTotal',
	cartUpdate:baseUrl+'cart/update',
	removeProduct:baseUrl+'cart/removecart/cart_item_id/',

	facebookUrl:'https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,gender&access_token=',
	googleClientId:'604308829863-kdl8blil7ookp44pv24fk6g484pp3het.apps.googleusercontent.com',
	googleUrl:'https://www.googleapis.com/auth/drive.readonly',


	splashImage:coreUrl+'/splash-invanos.jpg',
	welcomeBannerImages:coreUrl+'/welcome-banner.jpg',
	MainScreenBanner1:coreUrl+'/mainbanner.jpg',
	MainScreenBanner2:coreUrl+'/kid.jpg',
	MainScreenBanner3:coreUrl+'/womens.jpg',
	MainScreenBanner4:coreUrl+'/mens.jpg',
	MainScreenBanner5:coreUrl+'/summer-dress.jpg',
	MainScreenBanner6:bannerImage+'6.jpg',
	MainScreenBanner7:bannerImage+'7.jpg',
	MainScreenBanner8:coreUrl+'/western-wear.jpg',
	MainScreenBanner9:bannerImage+'9.jpg',
	MainScreenBanner10:coreUrl+'/mans-shoes.jpg',
	MainScreenBanner11:coreUrl+'/fitness-discount.jpg',
	OrderConfirmImage:coreUrl+'/order-img.png',
};

