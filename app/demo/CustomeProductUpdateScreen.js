import React ,{Component} from 'react';
import {View,Text,Image,ToastAndroid,ScrollView,Button,AsyncStorage,Dimensions,ToolbarAndroid,Picker,TouchableHighlight,StyleSheet } from 'react-native';
import {Content,Container,Footer,Item,Card,Input,Label,CardItem,Body,Thumbnail} from 'native-base';

import Loader from './Loader';
import ActionBar from 'react-native-action-bar';
import SelectMultiple from 'react-native-select-multiple';

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

// import RadioButton from 'radio-button-react-native';



var {height, width} = Dimensions.get('window');

export default class CustomeProductUpdateScreen extends Component{
  constructor(){
    super()
    this.state={
      myData:[],
      qty:1,
      DropDown:'',
      color:'',
      entityId:0,
      name:'',
      price:0,
      regularPrice:0,
      shortDescription:'',
      load:true,
      customOptions:[],
      customOptionsId:[],
      customOptionsValue:[],
      selectedValue: [],
      multipleSelectId:'',
      checkedValue:[],
      checkboxId:'',
      opt:1,
      radioValue:0,
      radioId:'',
      customField:'',
      customFieldId:0,
      customArea:'',
      customAreaId:0,
      dropDownValue:'',
      customDropDownId:0,
      count:0,
      cartItemId:0,
    }
  }
  componentDidMount() {
  	this.getData();
  }
  async getData(){
  	var productId = await AsyncStorage.getItem('ProductId');
    var cartCount = await AsyncStorage.getItem('count');
    var ItemId = await AsyncStorage.getItem('ItemId');
    // var ProductQty = await AsyncStorage.getItem('ProductQty');

    this.setState({
      count:cartCount,
      cartItemId:ItemId,
      // qty:ProductQty,
    });

    // ToastAndroid.show(''+productId, ToastAndroid.SHORT);
    let url='http://stageapp.invanos.net/xapi/products/getProductDetail/product_id/'+productId;
    return fetch(url)
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.model!==null){

        var array = [];
        for (let prop in responseJson.model.custom_options) {
          array.push(responseJson.model.custom_options[prop]);
        }

        this.setState({
          load:false,
          entityId:responseJson.model.entity_id,
          name:responseJson.model.name,
          imageUrl:responseJson.model.image_url,
          price:responseJson.model.price,
          regularPrice:responseJson.model.regular_price_with_tax,
          shortDescription:responseJson.model.short_description,
          customOptions:array,
        });

      }else{
        this.setState({load:false});
      }
    })
    .catch((error) => {
      this.setState({load:false});
      alert('ErrorMsg P0:'+error);
    });
  }
  onImgClick=()=>{
  	try {
  		AsyncStorage.setItem('imgUrl',this.state.imageUrl);
      this.props.navigation.navigate('FullScreenImg');
    } catch (error) {
      ToastAndroid.show(''+error, ToastAndroid.SHORT);
    }
  };
  onClick=()=>{
  	this.props.navigation.navigate('CartScreen');
  };

  updateCart=()=>{

    multiselect=[]
    if(this.state.selectedValue!=''){
      for (var i = 0; i < this.state.selectedValue.length; i++) {
        multiselect.push('&options['+this.state.multipleSelectId+'][]='+this.state.selectedValue[i].value);
      } 
    }
    let multipleselectData=multiselect.join('');

    checkbox=[]
    if(this.state.checkedValue!=''){
      for (var i = 0; i < this.state.checkedValue.length; i++) {
        checkbox.push('&options['+this.state.checkboxId+'][]='+this.state.checkedValue[i].value);
      } 
    }
    let checkBoxData=checkbox.join('');

      // alert(this.state.dropDownValue+this.state.customDropDownId);
    // ToastAndroid.show('Welcome'+this.state.multipleSelectId+','+array, ToastAndroid.SHORT);

  // ToastAndroid.show('require Data: '+this.state.customFieldId+this.state.customField+this.state.customAreaId+this.state.customArea, ToastAndroid.SHORT);
    
    this.setState({load:true});
      
      fetch('http://stageapp.invanos.net/xapi/cart/update', {
        method: 'POST',
        headers: {
        'Accept': '*/*',
        'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body:'cart['+this.state.cartItemId+'][qty]='+this.state.qty+'&cart['+this.state.cartItemId+'][custom_option][13]=hello'
      })
      .then((response) => response.json())
      .then((responseData) =>{
        if(responseData.code===0 && responseData.msg==='update carts success'){
          this.props.navigation.navigate('CartScreen');
          this.setState({call:1,load:false});
          ToastAndroid.show(''+responseData.msg,ToastAndroid.SHORT);
        }
      })
      .catch((error) => {
        alert('ErrorMsg C5 :'+error);
        this.setState({load:false});
      });
    
  };

  onSelect(index, value){
      this.setState({
      color:value
    })
  }

  onSelectionsChange = (selectedValue,item) => {
    this.setState({multipleSelectId:item.option_id,selectedValue})
    // if(selectedValue!=''){
    //   for (var i = 0; i < selectedValue.length; i++) {
    //   // ToastAndroid.show('Welcome '+selectedValue[i].value, ToastAndroid.SHORT);
    //   } 
    // }
  };

  onSelectionsChangeCheckBox = (checkedValue,item) => {
    this.setState({checkboxId:item.option_id,checkedValue});
  };

  onSelectionsChangeRadio=(radioValue,item) => {
    this.setState({radioId:item.option_id,radioValue});
  };

  addToWishList=()=>{
    global.MyProfileView=2;
    this.props.navigation.navigate('MyProfileTab');
  };

render(){
  const {goBack} = this.props.navigation;
 
  let att=this.state.customOptions.map(function(item,index){
    // ToastAndroid.show('Data :: '+item , ToastAndroid.SHORT);

    if(item.custom_option_type==='field'){
        return(
          <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
            <Item floatingLabel style={styles.textArea}>
              <Label>
              <Text style={styles.label}>{item.custom_option_title}</Text>
              </Label>
              <Input style={styles.inputLabel}
              onChangeText={(field) => this.setState({customField:field,customFieldId:item.option_id})}
              // onEndEditing={()=> this.setState({customFieldId:item.option_id,})}
              />
            </Item>
          </View>
        )
    }else{
      if(item.custom_option_type==='checkbox'){
        data=[]
        var array = [];
        for (let prop in item.custom_option_value) {
          array.push(item.custom_option_value[prop]);
        }

        for (var i = 0; i < array.length; i++) {
          data[i]={label:array[i].title ,value:array[i].option_type_id}
        }

        return(
          <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
            <SelectMultiple
              items={data}
              selectedItems={this.state.checkedValue}
              onSelectionsChange={(value)=>this.onSelectionsChangeCheckBox(value,item)} />
          </View>
        )
      }else{
        if(item.custom_option_type==='area'){
          return(
            <View key={index} style={{borderWidth:1,paddingLeft:10,paddingRight:10}}>
              <Item floatingLabel style={styles.textArea}>
              <Label>
              <Text style={styles.label}>{item.custom_option_title}</Text>
              </Label>
              <Input style={styles.inputLabel}
              multiline={true}
              numberOfLines={4}
              onChangeText={(area) => this.setState({customArea:area,customAreaId:item.option_id})}
              // onEndEditing={()=> this.setState({customAreaId:item.option_id})}
              />
              </Item>
            </View>
          )
        }else{
          if(item.custom_option_type==='drop_down'){
            // ToastAndroid.show('custom_option_type '+item.custom_option_type, ToastAndroid.SHORT);
            data=[]
            var array = [];
            for (let prop in item.custom_option_value) {
              array.push(item.custom_option_value[prop]);
            }

            for (var i = 0; i < array.length; i++) {
              data[i]={label:array[i].title ,value:array[i].option_type_id}
            }
          
            let cDataList1=data.map((item,index)=>{
              // ToastAndroid.show('length'+item.value, ToastAndroid.SHORT);
              return(
                <Picker.Item key={index} label={item.label} value={item.value}/>
                )
            });
            return(
            <View style={{borderWidth:1,flexDirection:'row',alignItems:'center',paddingLeft:10,paddingRight:10}} key={index}>
              <Text style={{textAlign:'center'}}>Drop Down :</Text>
              <Picker style={{width:250}}
              selectedValue={this.state.dropDownValue}
              onValueChange={(itemValue) => this.setState({dropDownValue: itemValue,customDropDownId:item.option_id})}>
              {cDataList1}
              </Picker>
            </View>
            )
          }else{
            if(item.custom_option_type==='radio'){
              data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                  array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                  data[i]={label:array[i].title ,value:array[i].option_type_id}
                }
              return(
                <View style={{borderWidth:1,padding:10}} key={index}>
                <RadioForm
                  radio_props={data}
                  animation={true}
                  formHorizontal={true}
                  labelStyle={{fontSize:16}}
                  // style={{marginLeft:80}}
                  onPress={(value) =>this.setState({radioId:item.option_id,radioValue:value})}/>
                </View>
              )

            }else{
              if(item.custom_option_type==='multiple'){                
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                  array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                  data[i]={label:array[i].title ,value:array[i].option_type_id}
                }

                return(
                <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
                 <SelectMultiple
                  items={data}
                  selectedItems={this.state.selectedValue}
                  onSelectionsChange={(value)=>this.onSelectionsChange(value,item)} />
                </View>
                )
              }else{
                // if(item.custom_option_type==='date'){
                //   return(
                //   <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
                    
                //   </View>
                //   )
                // }else{
                //   // if(item.custom_option_type==='date_time'){
                //   //   return(
                //   //   <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
                      
                //   //   </View>
                //   //   )
                //   // }else{
                //   //   // if(item.custom_option_type==='time'){
                //   //   //   return(
                //   //   //   <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
                        
                //   //   //   </View>
                //   //   //   )
                //   //   // }else{

                //   //   // }
                //   // }
                // }
              }
            }
          }
        }
      }
    }


  },this);


   
  // goBack=()=>{
  //     this.props.navigation.navigate('ProductList');
  //   };

    cartClick=()=>{
      this.setState({load:true});
      return fetch('http://stageapp.invanos.net/xapi/cart/getCartInfo')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({load:false});
        // ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
        if(responseJson.code === 5 &&responseJson.msg ==='Cart is empty!'){
          this.props.navigation.navigate('EmptyCartScreen');
        }
        else if(responseJson.code===1 && responseJson.msg===null){
          this.props.navigation.navigate('CartScreen');
        }
      })
      .catch((error) => {
        alert('ErrorMsg P2 :'+error);
        this.setState({load:false});
      });
    };

	return(
		<Container key={this.state.entityId} style={styles.container}>
    <ActionBar
      backgroundColor={'#ffffff'}
      // title={'Shopcart'}
      titleStyle={{textAlign:'center',color:'#00acec',fontSize:20}}
      containerStyle={styles.bar}
      leftIconName={'back-icon'}
      onLeftPress={() =>  goBack()}
      rightIcons={[
          {
              name: 'wish-icon',
              onPress: () => this.addToWishList(),
          },
          {
              name: 'cart-icon',
              badge: this.state.count,
              onPress: () => cartClick(),
          },
      ]}
      />
			<Content>
				<ScrollView>
  				<TouchableHighlight
  				style={styles.imgAlign}
  		 		onPress={this.onImgClick}>
  					<Image style={{width: width, height: 280,}} source = {{ uri:this.state.imageUrl}}/>
  				</TouchableHighlight>

  				<View style={styles.contentAlign}>
  		      <Text style={styles.nameText}>{this.state.name}</Text>
  				</View>

          <View style={styles.priceView}>
  					<Text style={styles.priceText}>₹ {Math.round(this.state.price)}</Text>
            <Text style={{marginLeft:10, fontSize:16,textDecorationLine: 'line-through'}}>₹ {Math.round(this.state.regularPrice)}</Text>
  				</View>

          <View>
            <Text style={styles.descriptionTitle}>Description</Text>
            <Text style={styles.descriptionText}>{this.state.shortDescription}</Text>
          </View>
      {att}
        </ScrollView>
      </Content>




			<Footer style={styles.footer}>
        <View style={styles.qtybtn}>
					<Text style={styles.qtyText}>Qty :</Text>
					<Picker style={styles.picker}
					selectedValue={this.state.qty}
					onValueChange={(itemValue, itemIndex) => this.setState({qty: itemValue})}>

					<Picker.Item label="1" value="1"/>
					<Picker.Item label="2" value="2"/>
					<Picker.Item label="3" value="3" />
					<Picker.Item label="4" value="4"/>
					<Picker.Item label="5" value="5" />
					</Picker>
        </View>

  

			<TouchableHighlight onPress={this.updateCart} >
				<View style={styles.footerbutton}>
				    <Text style={styles.buttonText}>UPDATE</Text>
				</View>
			</TouchableHighlight>
			</Footer>
      <Loader loading={this.state.load}/>
		</Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    height:height,
    width:width,
  	backgroundColor: '#ffffff',
  },
  checkBox:{
    marginLeft:15,
    marginTop:25
  },
  imgAlign:{
    alignItems: 'center',
  },
  productTitle:{
  	 textAlign: 'center',
  },
  footer:{
    backgroundColor:'#00acec',
    height:60
  },
  picker:{
  	flex: 1,
  	marginRight:5,
  	color:'white',
  },
  qtyText:{
  	marginLeft:40,
    color: 'white',
    fontSize:16,
  },
  qtybtn:{
  	width: 150,
    height:60,
    flexDirection: 'row',
    backgroundColor:'#00acec',
    alignItems: 'center',
  },
  footerbutton: {
    width: width/1.5,
    height:60,
    backgroundColor:'#00acec',
    alignItems: 'center',
  },
  buttonText: {
    textAlign: 'center',
    marginTop:18,
    color: 'white',
    fontSize:16,
  },
  contentAlign:{
    alignItems: 'center',
  },
  priceView:{
    marginTop:10,
    flex:1,
    alignItems: 'center',
    justifyContent:'center',
    flexDirection: 'row',
  },
  nameText:{
  	color:'#2a2a2a',
  	marginTop:15,
  	fontSize:20,
  },
  priceText:{
  	color:'#00acec',
    fontWeight:'bold',
    fontSize:26, 
  },
  descriptionTitle:{
  	color:'#373737',
  	fontSize:18,
  	marginLeft:18,
  	marginTop:10,
  	marginBottom:10,
  },
  descriptionText:{
  	color:'#6a6a6a',
  	fontSize:16,
  	marginLeft:10,
  	marginBottom:10,
    textAlign: 'center',
  },
  colorOptions:{
  	flexDirection: 'row',
  	justifyContent: 'center'
  },
})

