// import React, {Component} from 'react';

// import {Text,Button,View,} from 'react-native';

// class SecondScreen extends Component {
  
//   render() {
//     return (

//       <View style={{flex: 1,
//         alignItems: 'center',
//         justifyContent: 'center'}}>
//         <Text>Chat with </Text>

//       </View>
//     );
//   }
// }

// export default SecondScreen;


import React, { Component } from 'react';
import { ListView } from 'react-native';
import { Container, Header, Content, Button, Icon, List, ListItem, Text } from 'native-base';
// import {Icon} from 'react-native-material-design';
const datas = [
  'Simon Mignolet',
  'Nathaniel Clyne',
  'Dejan Lovren',
  'Mama Sakho',
  'Alberto Moreno',
  'Emre Can',
  'Joe Allen',
  'Phil Coutinho',
];
export default class SecondScreen extends Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      basic: true,
      listViewData: datas,
    };
  }
  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.listViewData];
    newData.splice(rowId, 1);
    this.setState({ listViewData: newData });
  }
  render() {
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    return (
      <Container>
        <Header />
        <Content>
          <List
            dataSource={this.ds.cloneWithRows(this.state.listViewData)}
            renderRow={data =>
              <ListItem>
                <Text> {data} </Text>
              </ListItem>}
            renderLeftHiddenRow={data =>
              <Button full onPress={() => alert(data)}>
                <Icon active name="md-edit" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="md-trash"/>
              </Button>}
            leftOpenValue={75}
            rightOpenValue={-75}
          />
        </Content>
      </Container>
    );
  }
}



// import React, { Component } from 'react';
// import { Container, Header, Left, Body, Right, Button, Icon, Spinner,Segment, Content, Text } from 'native-base';
// export default class SecondScreen extends Component {
//   render() {
//     return (
//        <Container>
//         <Header>
//           <Left>
//             <Button transparent>
//               <Icon name="arrow-back" />
//             </Button>
//           </Left>
//           <Right>
//             <Button transparent>
//               <Icon name="search" />
//             </Button>
//           </Right>
//         </Header>
//           <Body>
//             <Segment>
//               <Button><Text>sign Up</Text></Button>
//               <Button><Text>Sign in</Text></Button>
//             </Segment>
//           </Body>
//          <Spinner 

//          color='#00acec'/>
//       </Container>
//     );
//   }
// }
