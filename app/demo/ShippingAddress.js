import React, { Component } from 'react';
import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,
	ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid,Checkbox,Picker  } from 'react-native';
import { Container, Header, Content, Form,Button, Item,Footer,Input,Segment,Label,Left,Right } from 'native-base';

import CheckBox from 'react-native-checkbox';

export default class ShippingAddress extends Component{
	 constructor(props) {
   super(props)

   this.state = {
    email:'',
   	firstName:'',
    middleName:'',
    lastName:'',
    company:'',
    address:'',
    street:'',
    city:'',
    currentState:'',
    zip:'',
    country:'',
    contactNo:'',
    fax:'',
    value: true,
    isForShipping:1,
    qty:'',
}
}


  componentDidMount() {
    this.getData();    
  }
  async getData(){
    var EmailId= await AsyncStorage.getItem('EmailId');
    this.setState({email:EmailId});
    ToastAndroid.show('emailId'+EmailId,ToastAndroid.SHORT);
}

 // ToastAndroid.show('Welcome '+ firstName+','+middleName+','+lastName+','+company+','+address+','+street+','+city+','+currentState+','
 //   +country+','+zip+','+contactNo+','+fax, ToastAndroid.SHORT);

checkBoxData=(data)=>{

  if(data==true){
    this.setState({value:false,isForShipping:0})
    return (<View>
      <Text> Return Data</Text>
    </View>);
  }else{
    this.setState({value:true,isForShipping:1})
  }
 }
 
 setAddress=()=>{
  const { firstName,middleName,lastName,company,address,street,city,currentState,country,zip,contactNo,fax,email,isForShipping } = this.state

    ToastAndroid.show(''+this.state.isForShipping, ToastAndroid.SHORT)

  if(firstName!='' && lastName!='' && address!='' && street!='' && city!='' && currentState!='' && country!='' && zip!='' && contactNo!=''){
    ToastAndroid.show('not null '+email+','+ city+','+country, ToastAndroid.SHORT);

  fetch('http://stageapp.invanos.net/xapi/checkout/setShipping', {
    method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
       },
       body:'shipping[email]='+email+'&shipping[address_id]=null&shipping[firstname]='+firstName+'&shipping[middlename]='+middleName+'&shipping[lastname]='+lastName+'&shipping[company]='+company+'&shipping[street][]='+address+'&shipping[street][]='+street+'&shipping[city]='+city+'&shipping[region_id]=null&shipping[region]='+currentState+'&shipping[postcode]='+zip+'&shipping[country_id]='+country+'&shipping[telephone]='+contactNo+'&shipping[fax]='+fax+'&shipping[save_in_address_book]=1&shipping[use_for_shipping]='+isForShipping+'&shipping_address_id:null'

    }).then((response) => response.json())
      .then((responseData) => {

        if(responseData.code==0){
          alert('msg : '+responseData.msg);
            if(isForShipping===0){
                this.props.navigation.navigate('SetShippingAddress');
            }else{
                this.props.navigation.navigate('ShippingMethod');
            }
        }else if(responseData.code===1){
                  alert('errormsg : '+responseData.error);
        }
      }).done();
  }else{
        ToastAndroid.show('Address data is Empty', ToastAndroid.SHORT);
  }
 };
 nextScreen=()=>{
    this.props.navigation.navigate('ShippingMethod');
 };

 render(){
 	return(
 		<Container style={styles.container}>
        <Content>
          <Form>
          <View style={styles.rowView}>
            <Item floatingLabel style={{width:170}}>
              <Label>First Name</Label>
              <Input
              onChangeText={(firstName) => this.setState({firstName:firstName})}
              />
            </Item>

            <Item floatingLabel style={{width:150}}>
              <Label>Middle Name/Initial</Label>
              <Input 
              onChangeText={(middleName) => this.setState({middleName:middleName})}
              />
            </Item>        
          </View>

            <Item floatingLabel style={{width:150}}>
              <Label>Last Name</Label>
              <Input 
              onChangeText={(lastName) => this.setState({lastName:lastName})}
              />
            </Item>

            <Item floatingLabel style={{width:200}}>
              <Label>Company</Label>
              <Input 
              onChangeText={(company) => this.setState({company:company})}
              />
            </Item>

            <Item floatingLabel style={{width:300}}>
              <Label>Address</Label>
              <Input 
              onChangeText={(address) => this.setState({address:address})}
              />
            </Item>

              <Item floatingLabel style={{width:300}}>
              <Label>Street Address</Label>
              <Input 
              onChangeText={(street) => this.setState({street:street})}
              />
            </Item>

             <Item floatingLabel>
              <Label>Contact No</Label>
              <Input 
              onChangeText={(contactNo) => this.setState({contactNo:contactNo})}
              maxLength = {10}
              keyboardType = 'numeric'
              autoCapitalize={'none'}
              />
            </Item>

            <View style={styles.rowView}>
              <View style={{marginLeft:15,marginTop:10}}>
                <Label>City</Label>
                <Picker style={styles.picker}
                onValueChange={(itemValue, itemIndex) => this.setState({city:itemValue})}
                selectedValue={this.state.city}>
                <Picker.Item label="Mumbai" value="Mumbai"/>
                <Picker.Item label="Delhi" value="Delhi"/>
                <Picker.Item label="Bangalore" value="Bangalore"/>
                <Picker.Item label="Bangalore" value="Bangalore"/>
                <Picker.Item label="Ahmedabad" value="Ahmedabad"/>
                <Picker.Item label="Chennai" value="Chennai"/>
                <Picker.Item label="Kolkata" value="Kolkata"/>
                <Picker.Item label="Surat" value="Surat"/>
                <Picker.Item label="Pune" value="Pune"/>
                <Picker.Item label="Jaipur" value="Jaipur"/>
                </Picker>
              </View>

              <View style={{marginLeft:15,marginTop:10}}>
                <Label>State/Province</Label>
                <Picker style={styles.picker}
                onValueChange={(itemValue, itemIndex) => this.setState({currentState: itemValue})}
                selectedValue={this.state.currentState}>
                <Picker.Item label="Maharashtra" value="Maharashtra"/>
                <Picker.Item label="Delhi" value="Delhi"/>
                <Picker.Item label="Karnataka" value="Karnataka" />
                <Picker.Item label="Telangana" value="Telangana"/>
                <Picker.Item label="Gujarat" value="Gujarat" />
                </Picker>
              </View>
            </View>

            <View style={styles.rowView}>
              <View style={{marginLeft:15,marginTop:10}}>
                  <Label>Country</Label>
                  <Picker style={styles.picker}
                  onValueChange={(itemValue, itemIndex) => this.setState({country: itemValue})}
                  selectedValue={this.state.country}>
                  <Picker.Item label="India" value="IN"/>
                  <Picker.Item label="Afghanistan" value="AF"/>
                  <Picker.Item label="Armenia" value="AR"/>
                  <Picker.Item label="Azerbaijan" value="AZ" />
                  <Picker.Item label="Israel" value="IS" />
                  <Picker.Item label="Iran" value="IR" />
                  </Picker>
                </View>

              <Item floatingLabel style={{width:150}}>
                <Label>Zip/Postal Code</Label>
                <Input
                 maxLength = {8}
                 keyboardType = 'numeric'
                 onChangeText={(zip) => this.setState({zip:zip})}
                />
              </Item>
            </View>

            <Item floatingLabel>
              <Label>Fax</Label>
              <Input
               maxLength = {8}
               keyboardType = 'numeric'
               onChangeText={(fax) => this.setState({fax:fax})}
              />
            </Item>

            <View  style={styles.checkBox}>
            <CheckBox
            label='Same for Shipping?'
            checkedImage={require('../logos/checked.png')}
            uncheckedImage={require('../logos/unchecked.png')}
            onChange={(checked) =>this.checkBoxData(checked)}
            checked={this.state.value}/>
            </View>

          </Form>
        </Content>

          <Label>Delivery</Label>
          <Footer>
          <TouchableOpacity onPress={this.setAddress}>
          <View style={styles.footerbutton}>
          <Text style={styles.buttonText}>CONTINUE TO PAYMENT</Text>
          </View>
          </TouchableOpacity>
          </Footer>
      </Container>
			);
	}
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
  },
  rowView:{
    flexDirection: 'row',
  },
  picker:{
    width:170,
  },
  checkBox:{
    marginLeft:15,
    marginTop:25
  },
   footerbutton: {
    width: 300,
    marginTop:10,
    marginLeft:35,
    marginRight:35,
    alignItems: 'center',
  },
  button:{
    marginBottom: 30,
    width: 100,
    marginLeft:25,
    marginRight:25,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  buttonText: {
    padding: 10,
    color: 'white'
  },
})