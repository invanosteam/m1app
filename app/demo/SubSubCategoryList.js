import React, { Component } from 'react';
import {
  Text,View,Button,Image,
  ScrollView,List,ListView,
  TouchableOpacity,
  AsyncStorage,
  ToolbarAndroid,
  ToastAndroid 
} from 'react-native';
import {Content,Card,CardItem,Icon,Body, Right,Thumbnail} from 'native-base';

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class SubSubCategoryList extends Component {

  constructor(){
    super()
    this.state={
      myData:[],
      subSubCategorydata:ds,
      name:'',
    }
  }

  componentDidMount() {
        this.getData();
  }

    async getData(){
    var subCat= await AsyncStorage.getItem('subSubCategory').
    then((response) => JSON.parse(response)).
    then((responseJson) =>{
       var array =[];
          for (let prop in responseJson) {
             array.push(responseJson[prop]);
          }

        this.setState({ 
          subSubCategorydata:ds.cloneWithRows(array)
          // subCategory: responseJson 
        }) 
    }).
    catch((error)=>{
            ToastAndroid.show(''+error, ToastAndroid.SHORT);
         });

        // ToastAndroid.show(''+this.state.subCategory, ToastAndroid.SHORT);
  }
  nextScreen=()=>{
    this.props.navigation.navigate('ProductList');
    // ToastAndroid.show('Welcome ', ToastAndroid.SHORT);
  };

  render() {
        passData=(rowData)=>{
          if(rowData.category_id!=null){
            try {
              AsyncStorage.setItem('id',JSON.stringify(rowData.category_id));

              // ToastAndroid.show(''+rowData, ToastAndroid.SHORT);
            } catch (error) {
                  ToastAndroid.show(''+error, ToastAndroid.SHORT);
            }
            this.props.navigation.navigate('ProductList');
          }else{
            try {
              AsyncStorage.setItem('id',rowData.entity_id);

              // ToastAndroid.show(''+rowData, ToastAndroid.SHORT);
            } catch (error) {
                  ToastAndroid.show(''+error, ToastAndroid.SHORT);
            }
            this.props.navigation.navigate('ProductList');
          }
      };

     // let att=this.state.subCategory.map(function(item,index){
     //        // ToastAndroid.show(''+item.name, ToastAndroid.SHORT);
     //        return(
     //           <View key={index}>
     //              <TouchableOpacity onPress={this.nextScreen}>
     //                  <Card>
     //                    <CardItem>
     //                      <Body>
     //                        <Text>{item.name}</Text>
     //                        <Text>{item.category_id}</Text>
     //                      </Body>
     //                      <Right>
     //                        <Image source = {require('../logos/arrowicon.png')}/>
     //                      </Right>
     //                   </CardItem>
     //                  </Card>
     //              </TouchableOpacity>
     //              </View>
                
     //          )
     //     },this);
     return (
     <Content>
     <ListView
          dataSource={this.state.subSubCategorydata}
          renderRow={(rowData) =>
          <TouchableOpacity onPress={()=>passData(rowData)}>
           <Card>
            <CardItem>
              <Body>
                <Text>{rowData.name}</Text>
                <Text>{rowData.category_id}</Text>

              </Body>
              <Right>
                <Image source = {require('../logos/arrowicon.png')}/>
              </Right>
           </CardItem>
          </Card>

          </TouchableOpacity>
        }
      />
      
            <Button onPress={this.nextScreen} title="Click"/>

      </Content>
     );
   }
}



