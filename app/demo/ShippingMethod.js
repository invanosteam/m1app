import React, { Component } from 'react';
import {View,Text,Button,ScrollView,TouchableHighlight,StyleSheet,Dimensions,ListView,TouchableOpacity,ToastAndroid  } from 'react-native';
import {Content,Card,CardItem,Item,Icon,Body,Footer,Container, Right,Thumbnail} from 'native-base';

var {height, width} = Dimensions.get('window');


var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});


export default class ShippingMethod extends Component {
	 constructor(){
      super()
      this.state={
        myData:[],
        shippingMethods:ds,
        name:'',
        rowData:[],
        id:''
      }
    }
    componentDidMount() {
      this.getShippingMethod();
    }
    getShippingMethod(){
          return fetch('http://stageapp.invanos.net/xapi/checkout/getShippingMethodsList')
          .then((response) => response.json())
          .then((responseJson) => {

          var array =[];
          for (let prop in responseJson.model) {
             array.push(responseJson.model[prop]);
          }
          
          var array1 =[];
          for (var key in array) {
          let value = array[key];
          array1.push(value[0]);
          }
          // alert(''+responseJson.msg);

          this.setState({
            shippingMethods:ds.cloneWithRows(array1)
          })
        })
        .catch((error) => {
          console.error(error);
          ToastAndroid.show('Welcome '+error, ToastAndroid.SHORT);
        });
    }


  render(){
     passData=(rowData)=>{
      ToastAndroid.show('divyesh::'+rowData.method, ToastAndroid.SHORT);

      return fetch('http://stageapp.invanos.net/xapi/checkout/setShippingMethod', {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
       },
       body:'shipping_method='+rowData.code
     }).then((response) => response.json())
      .then((responseJson) => {
          // ToastAndroid.show('response :'+responseJson.msg, ToastAndroid.SHORT);

        if(responseJson.msg==='save shipping method success!'){
        // alert('setShippingMethod::'+responseJson.msg);
        this.props.navigation.navigate('PaymentMethod');
      }else{
        alert('error::'+responseJson.msg);
      }
    }).catch((error) => {
      console.error(error);
      ToastAndroid.show('ErrorMsg '+error, ToastAndroid.SHORT);
    });
  };
  onClick=()=>{
    this.props.navigation.navigate('ProductScreen');
  };
 
    return(
      <Container style={styles.container}>
      <ScrollView>
      <ListView
        dataSource={this.state.shippingMethods}
        renderRow={(rowData) =>
          <TouchableOpacity onPress={()=>passData(rowData)}>
          <Card>
            <CardItem>
            <Text >{rowData.method_title}</Text>
            </CardItem> 
          </Card>
          </TouchableOpacity>
        }/>
      </ScrollView>
      <Footer style={{backgroundColor:'#00acec'}}>
        <TouchableOpacity onPress={this.setAddress}>
          <View style={styles.footerButton}>
            <Text style={styles.buttonText}>CONTINUE TO PAYMENT</Text>
          </View>
        </TouchableOpacity>
      </Footer>
      </Container>
      );
  }
}
const styles = StyleSheet.create({
  container: {
    width:width,
    height:height,
    backgroundColor:'#ffffff',
  },
  footerButton: {
    width: 300,
    marginTop:10,
    marginLeft:35,
    marginRight:35,
    alignItems: 'center',
  },
  buttonText: {
    padding: 10,
    color: 'white'
  },
  editTextContainer:{
    padding: 10
  },
  editText:{
    width:250,
    height: 40
  },
  button: {
    marginBottom: 30,
    width: 180,
    marginLeft:25,
    marginRight:25,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  buttonText: {
    padding: 10,
    color: 'white'
  },
  alternativeLayoutButtonContainer: {
    alignItems: 'center',
    paddingTop:20
  }

})

