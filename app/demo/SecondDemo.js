import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,Button,View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class SecondDemo extends Component {
  static navigationOptions={
    tabBarLabel:'SecondDemo',
    drawerIcon:({tintColor})=>{
      return( 
        <MaterialIcons 
        name="change-history"
        size={24}
        style={{color:tintColor}}
        >
        </MaterialIcons>
        );
    }
  }

  render() {
    return (
      <View style={
        {
          flex:1,
          justifyContent:'center',
          alignItems:'center'
        }
      }>
        <Text style={{fontSize:30 , color:'green'}}>
          Welcome to React Native!
        </Text>
        <Button
        onPress={()=>this.props.navigation.navigate('DrawerOpen')}
        title="Open DrawerNAvigator"
        />
      </View>
    );
  }
}
