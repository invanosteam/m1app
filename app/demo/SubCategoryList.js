import React, { Component } from 'react';
import {
  Text,View,Button,Image,
  ScrollView,List,ListView,
  TouchableOpacity,
  AsyncStorage,
  ToolbarAndroid,
  ToastAndroid 
} from 'react-native';
import {Content,Card,CardItem,Icon,Body, Right,Thumbnail} from 'native-base';

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class SubCategoryList extends Component {

  constructor(){
    super()
    this.state={
      myData:[],
      subCategorydata:ds,
      name:'',
    }
  }

  componentDidMount() {
        this.getData();
  }

    async getData(){
    var subCat= await AsyncStorage.getItem('subCategory').
    then((response) => JSON.parse(response)).
    then((responseJson) =>{
       var array =[];
          for (let prop in responseJson) {
             array.push(responseJson[prop]);
          }

        this.setState({ 
          subCategorydata:ds.cloneWithRows(array)
          // subCategory: responseJson 
        }) 
    }).
    catch((error)=>{
            ToastAndroid.show(''+error, ToastAndroid.SHORT);
         });

        // ToastAndroid.show(''+this.state.subCategory, ToastAndroid.SHORT);
  }

  nextScreen=()=>{
    this.props.navigation.navigate('ProductList');
    ToastAndroid.show('Welcome ', ToastAndroid.SHORT);
  };

  render() {
        passData=(rowData)=>{
          if(rowData.children!=='undefined' && rowData.children.length > 0){
            
                // ToastAndroid.show('Clicked-SubCategoryList'+JSON.stringify(rowData.children), ToastAndroid.SHORT);
              try {
                AsyncStorage.setItem('subSubCategory',JSON.stringify(rowData.children));

              } catch (error) {
                    ToastAndroid.show(''+error, ToastAndroid.SHORT);
              }

    this.props.navigation.navigate('SubSubCategoryList');
        
      }else{
         try {
              AsyncStorage.setItem('id',rowData.category_id);

            } catch (error) {
                  ToastAndroid.show(''+error, ToastAndroid.SHORT);
            }
              // ToastAndroid.show('Clicked-ProductList'+rowData.category_id, ToastAndroid.SHORT);
        this.props.navigation.navigate('ProductList');
      }
      };

     // let att=this.state.subCategory.map(function(item,index){
     //        // ToastAndroid.show(''+item.name, ToastAndroid.SHORT);
     //        return(

     //           <View key={index}>
     //              <TouchableOpacity onPress={this.nextScreen}>
     //                  <Card>
     //                    <CardItem>
     //                      <Body>
     //                        <Text>{item.name}</Text>
     //                        <Text>{item.category_id}</Text>
     //                      </Body>
     //                      <Right>
     //                        <Image source = {require('../logos/arrowicon.png')}/>
     //                      </Right>
     //                   </CardItem>
     //                  </Card>
     //              </TouchableOpacity>
     //              </View>
                
     //          )
     //     },this);
     return (
     <Content>

          <ListView
          dataSource={this.state.subCategorydata}
          renderRow={(rowData) =>
          <TouchableOpacity onPress={()=>passData(rowData)}>
           <Card>
            <CardItem>
              <Body>
                <Text>{rowData.name}</Text>
                <Text>{rowData.category_id}</Text>

              </Body>
              <Right>
                <Image source = {require('../logos/arrowicon.png')}/>
              </Right>
           </CardItem>
          </Card>

          </TouchableOpacity>
        }
      />
      
            <Button onPress={this.nextScreen} title="Click"/>

      </Content>
     );
   }
}

     // {att}


