
import React, { Component } from 'react';
import {View,Text,Image,TouchableOpacity,ToastAndroid,AsyncStorage } from 'react-native';

export default class WishList extends Component{
    constructor(){
        super()
        this.state={
            pid:''
        }
    }

componentDidMount() {
    this.getData();
}
async getData(){
    var productId= await AsyncStorage.getItem('ProductId');
    this.setState({pid:productId});
    // ToastAndroid.show(''+this.state.pid, ToastAndroid.SHORT);
}
render() {
    // const { navigate } = this.props.navigation;
    return (
        <View style={{flexDirection: 'row'}}>
        <TouchableOpacity onPress={() => {
            fetch('http://stageapp.invanos.net/xapi/Wishlist/add/product_id/'+this.state.pid)
            .then((response) => response.json())
            .then((responseJson) => {
                if(responseJson.code==0&&responseJson.msg!==null){
                    ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
                }else if(responseJson.code==5) {
                    alert(responseJson.msg);
                }
            })
            .catch((error) => {
              ToastAndroid.show('Welcome '+error, ToastAndroid.SHORT);
          })
        }}>
        <Image style={{marginRight:10}} source={require('../logos/wish-icon.png')}/>
        </TouchableOpacity>

      <TouchableOpacity>
        <Image style={{marginRight:10}} source={require('../logos/cart-icon.png')}/>
      </TouchableOpacity>
      </View>
// onPress={() =>  navigate('CartScreen')}
      );
    }
}