import React, { Component } from 'react'
import { AsyncStorage, Text, View, TextInput, StyleSheet } from 'react-native'

class AsyncStorageExample extends Component {
   state = {
      'name': '',
      'pass':''
   }
   componentDidMount = () => {

    AsyncStorage.getItem('name').then((value) =>
        this.setState({ 'name': value })) 
}

   setName = (value) => {
      AsyncStorage.setItem('name', value);
      this.setState({ 'name': value });
   }

     setPass = (value) => {
      AsyncStorage.setItem('pass', value);
      this.setState({ 'pass': value });
   }


   render() {
      return (
         <View style = {styles.container}>
            <TextInput style = {styles.textInput} autoCapitalize = 'none' 
               onChangeText = {this.setName}/>

             <TextInput style = {styles.textInput} autoCapitalize = 'none' 
               onChangeText = {this.setPass}/>   
            <Text>
               {this.state.name}
            </Text>
             <Text>
               {this.state.pass}
            </Text>
         </View>
      )
   }
};

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      alignItems: 'center',
      marginTop: 50
   },
   textInput: {
      margin: 15,
      height: 35,
      borderWidth: 1,
   }
})


export default AsyncStorageExample