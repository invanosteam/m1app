import React ,{Component} from 'react';
import {View,Text,Image,ToastAndroid,ScrollView,Button,TouchableHighlight,Dimensions,AsyncStorage,Picker,ToolbarAndroid,TouchableOpacity,StyleSheet } from 'react-native';
import {Content,Container,Footer,Item,Label,Card,CardItem,Body,Thumbnail} from 'native-base';
import ActionBar from 'react-native-action-bar';
import RadioButton from 'radio-button-react-native';
import RadioGroup from 'react-native-custom-radio-group1';



import Loader from './Loader';

var {height, width} = Dimensions.get('window');


const imgUrl='http://stageapp.invanos.net/productimg.jpeg';

export default class ConfigProductUpdateScreen extends Component{
	constructor(){
		super()
		this.state={
	      myData:[],
	      configData:[],
	      qty:1,
	      color:'',
	      entityId:0,
	      name:'',
	      price:0,
	      regularPrice:0,
	      shortDescription:'',
	      superAttribute:'',
	      selectedOption:'',
	      load:true,
	      count:0,
	      cartItemId:0,
	  }
	}
	componentDidMount() {
		this.getData();
	}
	async getData(){

		var productId = await AsyncStorage.getItem('ProductId');
		var cartCount = await AsyncStorage.getItem('count');
		var ItemId = await AsyncStorage.getItem('ItemId');
		// var ProductQty = await AsyncStorage.getItem('ProductQty');

	    
	   	this.setState({
	   		count:cartCount,
	        cartItemId:ItemId,
	        // qty:ProductQty,
	   	});

	  	let url='http://stageapp.invanos.net/xapi/products/getProductDetail/product_id/'+productId;
	    return fetch(url)
	    .then((response) => response.json())
	  	.then((responseJson) => {
	  		this.setState({
	  			entityId:responseJson.model.entity_id,
		    	name:responseJson.model.name,
		    	imageUrl:responseJson.model.image_url,
		    	price:responseJson.model.price,
		    	regularPrice:responseJson.model.regular_price_with_tax,
		        shortDescription:responseJson.model.short_description,
		        superAttribute:responseJson.model.attribute_options.label.Color,
		    	load:false,
		    });

	        // ToastAndroid.show('superAttribute :: '+responseJson.model.attribute_options.label.Color, ToastAndroid.SHORT);
	       	
	       	var Collection =[];
	        for (let prop in responseJson.model.attribute_options.collection) {
	        	Collection.push(responseJson.model.attribute_options.collection[prop]);
	        }
	        var labelsKey=[];
	        for (let prop in responseJson.model.attribute_options.labels_key.Color) {
	        		labelsKey.push(responseJson.model.attribute_options.labels_key.Color[prop]);
	        }

	       	var array1 =[];
	        array1=[
	        {'label':labelsKey[0],'value':Collection[0]},
	        {'label':labelsKey[1],'value':Collection[1]},
	        ];


	        this.setState({
	        	configData:labelsKey,
	        	radioData:array1
	        });
	        // alert(''+this.state.configData);
	    })
	    .catch((error) => {
	    	alert('ErrorMsg C0:'+error);
	    	this.setState({load:false});
	    });
	}

	onImgClick=()=>{
	  	try {
			AsyncStorage.setItem('imgUrl',this.state.imageUrl);
	    	this.props.navigation.navigate('FullScreenImg');
	    } catch (error) {
	        ToastAndroid.show(''+error, ToastAndroid.SHORT);
	    }
	};
	onClick=()=>{
		this.props.navigation.navigate('CartScreen');
	};

	updateCart=()=>{
		this.setState({load:true});

		// http://stageapp.invanos.net/xapi/cart/add/product_id/'+this.state.cartItemId+'/qty/'+this.state.qty+'?'+'super_attribute['+this.state.superAttribute+']='+this.state.selectedOption;
		// http://stageapp.invanos.net/xapi/cart/update?cart[621][qty]=10

		fetch('http://stageapp.invanos.net/xapi/cart/update', {
        method: 'POST',
        headers: {
        'Accept': '*/*',
        'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
	      },
	      body:'cart['+this.state.cartItemId+'][qty]='+this.state.qty+'&cart['+this.state.cartItemId+'][custom_option][13]=hello'
	    })
	    .then((response) => response.json())
	    .then((responseData) =>{
	      if(responseData.code===0 && responseData.msg==='update carts success'){
	        this.props.navigation.navigate('CartScreen');
	        this.setState({call:1,load:false});
	        ToastAndroid.show(''+responseData.msg,ToastAndroid.SHORT);
	      }
	    })
	    .catch((error) => {
	      alert('ErrorMsg C5 :'+error);
	      this.setState({load:false});
	    });
	};

	onSelect(index, value){
		ToastAndroid.show('Radio Value'+value, ToastAndroid.SHORT);
	    this.setState({
	    	gender:value
	    })
	};
	handleOnPress(value){
		this.setState({radioValue:value});
		ToastAndroid.show('selected Value is : '+value, ToastAndroid.SHORT);
    }
	render(){
		const {goBack} = this.props.navigation;
		// goBack=()=>{
		// 	this.props.navigation.navigate('ProductList');
		// };

		cartClick=()=>{
			this.setState({load:false});
			return fetch('http://stageapp.invanos.net/xapi/cart/getCartInfo')
		  	.then((response) => response.json())
		  	.then((responseJson) => {
		  		if(responseJson.code === 5 &&responseJson.msg ==='Cart is empty!'){
		  			this.props.navigation.navigate('EmptyCartScreen');
	      		}
		      	else if(responseJson.code===1 && responseJson.msg===null){
		      		this.props.navigation.navigate('CartScreen');
		      	}
		    })
		    .catch((error) => {
		    	alert('ErrorMsg C2:'+error);
		    	this.setState({load:false});
		    });
		};
		configSelect=(selected)=>{
			// ToastAndroid.show(''+selected, ToastAndroid.SHORT);
			this.setState({selectedOption:selected});
		};

		addToWishList=()=>{
			return fetch('http://stageapp.invanos.net/xapi/Wishlist/add/product_id/'+this.state.entityId)
			.then((response) => response.json())
			.then((responseJson) => {
				if(responseJson.code==0&&responseJson.msg!==null){
					ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
				}else if(responseJson.code==5) {
					alert(responseJson.msg);
				}
			})
			.catch((error) => {
				alert('ErrorMsg C3:'+error);
				ToastAndroid.show('Welcome '+error, ToastAndroid.SHORT);
			});
		};

		// contents = this.state.configData.map(function (item,index) {
		// 	// ToastAndroid.show('Welcome 123 '+index, ToastAndroid.SHORT);
		// 	return (
		// 		<View key={index}>
		// 	    	<Text style={{fontSize:16,marginLeft:15}}>{item}</Text>
		// 	  	</View>
		// 	  	);
		// });
		return(
			<Container key={this.state.myData.entity_id} style={styles.container}>
		    <ActionBar
	        backgroundColor={'#ffffff'}
	        title={'Shopcart'}
	        titleStyle={{textAlign:'center',color:'#00acec',fontSize:20}}
	        containerStyle={styles.bar}
	        leftIconName={'back-icon'}
	        onLeftPress={() =>  goBack()}
	        rightIcons={[
	            {
	                name: 'wish-icon',
	                onPress: () => addToWishList(),
	            },
	            {
	                name: 'cart-icon',
	                badge: this.state.count,
	                onPress: () => cartClick(),
	            },
	        ]}
	        />
	        <Content>
	        <ScrollView>
	     		<TouchableHighlight 
	     		style={styles.imgAlign}
	     		onPress={this.onImgClick}>			             	
					<Image style={{width: width, height: 280}} source = {{ uri:this.state.imageUrl}}/>
				</TouchableHighlight>

				<View style={styles.contentAlign}>
	            	<Text style={styles.nameText}>{this.state.name}</Text>
				</View>

				<View style={styles.priceView}>
					<Text style={styles.priceText}>₹ {Math.round(this.state.price)}</Text>
					<Text style={{marginLeft:10, fontSize:16,textDecorationLine: 'line-through'}}>₹ {Math.round(this.state.regularPrice)}</Text>
				</View>




				<View style={styles.colorOptions}>
	                <View style={styles.radioView}>
		                <RadioGroup 
			            radioGroupList={this.state.radioData}
			            onChange={(selected)=> configSelect(selected)}
			            />
	                </View>
			    </View>

				<View>
			    	<Text style={styles.descriptionTitle}>Description</Text>
					<Text style={styles.descriptionText}>{this.state.shortDescription}</Text>
	        	</View>
			</ScrollView>
		</Content>

		<Footer style={styles.footer}>
			<View style={styles.qtybtn}>
			<Text style={styles.qtyText}>Qty :</Text>

			<Picker style={styles.picker}
			selectedValue={this.state.qty}
			onValueChange={(itemValue, itemIndex) => this.setState({qty: itemValue})}>

			<Picker.Item label="1" value="1"/>
			<Picker.Item label="2" value="2"/>
			<Picker.Item label="3" value="3" />
			<Picker.Item label="4" value="4"/>
			<Picker.Item label="5" value="5" />
			</Picker>
			</View>

			<TouchableHighlight onPress={this.updateCart} >
				<View style={styles.footerbutton}>
				    <Text style={styles.buttonText}>UPDATE</Text>
				</View>
			</TouchableHighlight>
		</Footer>

		<Loader loading={this.state.load}/>
		</Container>
		);
	}
}
const styles = StyleSheet.create({
  container: {
  	height:height,
    width:width,
  	backgroundColor: '#ffffff',
  },
  checkBox:{
    marginLeft:15,
    marginTop:25
  },
  radioView:{
    width:width,
    justifyContent:'center',
    alignItems:'center',
    marginTop:15,
    marginBottom:15,
  },
  imgAlign:{
    alignItems: 'center',
  },
  productTitle:{
  	 textAlign: 'center',
  },
  footer:{
    backgroundColor:'#00acec',
    height:60
  },
  priceView:{
    marginTop:10,
    flex:1,
    alignItems: 'center',
    justifyContent:'center',
    flexDirection: 'row',
  },
  qtybtn:{
  	width: 150,
    height:60,
    flexDirection: 'row',
    backgroundColor:'#00acec',
    alignItems: 'center',
  },
  qtyText:{
  	marginLeft:40,
    color: 'white',
    fontSize:16,
  },
  picker:{
  	flex: 1,
  	marginRight:5,
  	color:'white',
  },
  footerbutton: {
  	width: width/1.5,
    height:60,
    backgroundColor:'#00acec',
    alignItems: 'center',
  },
  buttonText: {
    textAlign: 'center',
    marginTop:18,
    color: 'white',
    fontSize:16,
  },
  contentAlign:{
    alignItems: 'center',
  },
  nameText:{
  	color:'#2a2a2a',
  	marginTop:5,
  	fontSize:20,
  },
  priceText:{
  	color:'#00acec',
  	fontWeight:'bold',
  	fontSize:26,
  },
  descriptionTitle:{
  	color:'#373737',
  	fontSize:18,
  	marginLeft:15,
  	marginTop:10,
  	marginBottom:10,
  },
  descriptionText:{
  	color:'#6a6a6a',
  	fontSize:16,
  	marginLeft:10,
  	marginBottom:10,
    textAlign: 'center',
  },
  colorOptions:{
  	flexDirection: 'row',
  	justifyContent: 'center'
  },
})