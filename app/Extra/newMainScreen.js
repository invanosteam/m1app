import React, { Component } from 'react';
import {View,Text,Image,Button,ScrollView,Alert,DrawerLayoutAndroid,Dimensions,BackHandler,StatusBar,TouchableWithoutFeedback,TouchableHighlight,StyleSheet,BackAndroid,ToolbarAndroid,ListView,TouchableOpacity,AsyncStorage,ToastAndroid  } from 'react-native';

import {Content,Card,CardItem,Body,Footer,Item, Right,Thumbnail} from 'native-base';

import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import Accordion from 'react-native-accordion';

import ImageSlider from 'react-native-image-slider';

import FooterTabBar from './FooterTabBar';

import ActionBar from 'react-native-action-bar';

import Bar from 'react-native-bar-collapsible';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';

var {height, width} = Dimensions.get('window');

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

import urls from '../config/urls';

import styles from '../styles/MainScreen.style';

import FetchData from './FetchData';

const fetchData = new FetchData();
const Login=fetchData.resetIndex('LoginTab');
export default class MainScreen extends Component{
	constructor(){
		super()
	    this.state={
		    clonedList:ds,
		    userState:'Sign in',
		    activeTabNo:0,
		    NoNetwork:0,
		    count:0,
	    }
	    this.handleBackButton = this._handleBackButton.bind(this)
	}

	componentDidMount() {
	  	this.getData();
	}

	componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('MainScreen'));
    }

    // componentWillUnmount(){
    // 	BackHandler.('hardwareBackPress', this.handleBackButton);
    // };

    _handleBackButton=()=>{
        Alert.alert(
            'Quiting',
            'Want to Exit?',
            [
                {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel'
                },
                { text: 'Exit', onPress: () => BackHandler.exitApp()}
            ],
            { cancelable: false }
        );
        return true;
    }

	getData(){
		global.MyProfileView=0;
		this.setState({count:global.cartItemCount});
	
		fetchData.getMaster(urls.getCustomerStatus).then((responseJson)=>{
	        if(responseJson.code==0){
	    		this.setState({userState:'Sign out'});
	    		global.firstName=responseJson.model.firstname;
	    		global.lastName=responseJson.model.lastname;
	    		global.userEmailId=responseJson.model.email;
	    		global.phoneNo=responseJson.model.tel;
	        }else{
	        	global.userEmailId=null;
	        	this.setState({userState:'Sign in'});
	        }
		})

		fetchData.getMaster(urls.getCategoryTree).then((responseJson)=>{
			if(responseJson!=null && responseJson!=''){
	        	var array =[];
		        for (let prop in responseJson) {
		             array.push(responseJson[prop]);
		        }
		        this.setState({
		        	clonedList:ds.cloneWithRows(array),
		        })
			}
		});
	}

    onClickEvent(screenName){
    	BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    	this.props.navigation.navigate(screenName);
	};

	openDrawer=()=> {
		this.refs['DRAWER'].openDrawer();
	};
		   
	_renderRow=(rowData)=>{
		passData=(rowData)=>{
			global.category_id=rowData.category_id;
			this.onClickEvent('ProductList');
      	};

	    // var rightIcon = (
		   //  <Right>
	    // 	    <Image source = {require('../logos/plus-icon.png')}/>
	    //     </Right>	  	
	    // );

	    // if(rowData.children[0]!=null){
	    // 	var showIcon=rightIcon;
	    // }

	    var header = (
	    	<View style={{flexDirection:'row',flex:1}}>
		   	  	<TouchableOpacity  onPress={()=>passData(rowData)}>
			        <View style={{width:width/1.55,marginTop:15,marginBottom:15,marginLeft:15,marginRight:16}}>
			            <Text style={styles.drawerFont}>{rowData.name}</Text>
			        </View>
		        </TouchableOpacity>
	        	<View style={{marginTop:15,marginRight:20}}>
	        	</View>
			</View>
	        		// {showIcon}
	    );

	    var contentData = (
	    	rowData.children.map((data,index)=>{
		      return(
		      	<View key={data.category_id}>
		      	  	<TouchableOpacity  onPress={()=>passData(data)}>
				      	<CardItem style={styles.drawerBg}>
				        	<Text style={styles.drawerChidFont}>{data.name}</Text>
				      	</CardItem>
			      	</TouchableOpacity>
			    </View>
		        )
		    })
	    );

    	// var content=(<View>{contentData}</View>);

	    return (
	    	<Accordion
		      	header={header}
		    	content={contentData}
		    />
	    );
  	};

  	userLogin=()=>{
	  	if(this.state.userState==='Sign in'){
	  		this.props.navigation.dispatch(Login);
	  	}else if(this.state.userState==='Sign out'){
	  		fetchData.getMaster(urls.logOut).then((responseJson)=>{
	  			if(responseJson.code===0){
					try {
						AsyncStorage.setItem('EmailId','');
						this.setState({userState:'Sign in'});
						// GoogleSignin.signOut();  
						ToastAndroid.show('logout', ToastAndroid.SHORT);
					} catch (error) {
						ToastAndroid.show(''+error, ToastAndroid.SHORT);
					}
				}
				else{
					ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
				}
	  		})
	  	}
  	};

  	render(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

		accountDetails=()=>{
			if(this.state.userState==='Sign out'){
				this.onClickEvent('MyProfileTab');
			}else{
				ToastAndroid.show('Please sing up with us ', ToastAndroid.SHORT);
			}
		};

		footerTabAction=(index)=>{
			if(index===0){
				this.onClickEvent('MainScreen');
			}else if(index===1){
				// this.onClickEvent('HotOffer');
				ToastAndroid.show('function is under devlopment !!', ToastAndroid.SHORT);

			}else if(index===2){
				this.onClickEvent('CartScreen');

			}else if(index===3){
	    		this.onClickEvent('SearchScreen');

			}else if(index===4){
				if(this.state.userState==='Sign out'){
	    			this.onClickEvent('MyProfileTab');

				}else{
					Alert.alert(
			            'Sign in',
			            'Register with us and enjoy more feature!',
			            [
			                {
			                    text: 'May be later',
			                    onPress: () => this.setState({activeTabNo:0}),
			                    style: 'cancel'
			                },
			                { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
			            ],
			            { cancelable: false }
			        );
				}
			}
		};

      	var navigationView = (
      		<View style={styles.view1}>
			    <ListView
			    dataSource={this.state.clonedList}
			    renderRow={(rowData) => this._renderRow(rowData)}/>

		    	<View style={styles.view2}>
			      	<TouchableHighlight onPress={()=>ToastAndroid.show('Track order', ToastAndroid.SHORT)}>
				      	<CardItem style={styles.drawerBg}>
				    		<Text style={styles.drawerFont}>Track order</Text>
				    	</CardItem>
			    	</TouchableHighlight>

			    	<TouchableHighlight onPress={()=>accountDetails()}>
				    	<CardItem style={styles.drawerBg}>
				    		<Text style={styles.drawerFont}>Account detalis</Text>
				    	</CardItem>
			    	</TouchableHighlight>

			      	<TouchableHighlight onPress={()=>ToastAndroid.show('Settings', ToastAndroid.SHORT)}>
				    	<CardItem style={styles.drawerBg}>
							<Text style={styles.drawerFont}>Settings</Text>
						</CardItem>
					</TouchableHighlight>

			      	<TouchableHighlight onPress={()=>this.userLogin()}>
						<CardItem style={styles.drawerBg}>
							<Text style={styles.drawerFont}>{this.state.userState}</Text>
						</CardItem>
					</TouchableHighlight>
				</View>
		  	</View>
		  );

		return(
			<DrawerLayoutAndroid
			    drawerWidth={width/1.2}
			    ref={'DRAWER'}
			    drawerPosition={DrawerLayoutAndroid.positions.Left}
			    renderNavigationView={() => navigationView}>

			    <StatusBar barStyle="dark-content"/>

			    <ActionBar
				    backgroundColor={'#ffffff'}
				    containerStyle={styles.actionbarContainer}
				    titleContainerStyle={styles.actionbarTitleContainer}
				    leftIconName={'menu-icon'}
				    onLeftPress={() => this.openDrawer()}
				    onTitlePress={() => ToastAndroid.show('TitlePress', ToastAndroid.SHORT)}/>


			    <ScrollView style={styles.container}>
			    	<View style={styles.banner1}>
					    <ImageSlider
					    images={[
					        urls.MainScreenBanner1,
					        urls.MainScreenBanner1,
					        urls.MainScreenBanner1
				        ]}
						height={responsiveHeight(25)}/>
				    </View>
				   	
				   	<View style={styles.container2}>
					    <View>
						    <TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
						      	<Image style={styles.banner2} 
						      	source = {{ uri:urls.MainScreenBanner2}}
						      	/>
						    </TouchableOpacity>
						    <TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
						      	<Image style={styles.banner2} 
						      	source = {{ uri:urls.MainScreenBanner3}}
						      	/>
						    </TouchableOpacity>
					    </View>
				      	<View>
					      	<TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
					      		<Image style={styles.banner3}
					      		source = {{ uri:urls.MainScreenBanner12}}
					      		/>
					      	</TouchableOpacity>
					      	<TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
					      		<Image style={styles.banner4} 
					      		source = {{ uri:urls.MainScreenBanner4}}
					      		/>
				      		</TouchableOpacity>
				      	</View>		    
				   	</View>



				   	{(this.state.userState==='Sign in')?
				      	<TouchableOpacity onPress={()=>this.onClickEvent('Login')}>
						   	<Card style={styles.homeLoginCart}>
				   				<View style={styles.homeLoginView}>
				   					<Image style={styles.homeLoginImg}source = {require ('../logos/home-login.png')}/>
					   				<View>
						   				<Text numberOfLines={1} style={styles.homeLoginText1}>Login Now </Text>
						   				<Text numberOfLines={1} style={styles.homeLoginText2}>Access Wishlist and many more special offers !</Text>
				   					</View>
				   				</View>
						   	</Card>
			      		</TouchableOpacity>
				   		:
				   		null
				   	}
				    <View style={styles.container3}>
					    <View>
						    <TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
						      	<Card>
							      	<Image 
							      		style={styles.banner5} 
							      		source = {{ uri:urls.MainScreenBanner8}}
							      	/>
						      	</Card>
						    </TouchableOpacity>
						    <TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
						      	<Card>
							      	<Image 
							      		style={styles.banner6} 
							      		source = {{ uri:urls.MainScreenBanner11}}
							      	/>
						      	</Card>
						    </TouchableOpacity>
					    </View>
				      	<View>
					      	<TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
					      		<Card>
						      		<Image 
						      			style={[styles.banner6]}
						      			source = {{ uri:urls.MainScreenBanner9}}
					      			/>
					      		</Card>
					      	</TouchableOpacity>
					      	<TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
					      		<Card>
						      		<Image 
						      			style={[styles.banner7]} 
						      			source = {{ uri:urls.MainScreenBanner10}}
						      		/>
					      		</Card>
				      		</TouchableOpacity>
				      	</View>	
				   	</View>
			   		<View style={{height:responsiveHeight(25),marginTop:5,marginBottom:5}}>
					    <Card>
						    <ImageSlider
						    images={[
						        urls.MainScreenBanner5,
						        urls.MainScreenBanner6,
						        urls.MainScreenBanner7
					        ]}
							height={responsiveHeight(25)}/>
						</Card>
				    </View>
				</ScrollView>
				<Footer style={{height:58}}>
	       			<FooterTabBar activeTabNo={this.state.activeTabNo} count={this.state.count}/>	
			    </Footer>
			</DrawerLayoutAndroid>
		);
	}
}

    

				   	//  <View style={styles.container3}>
					   //  <View>
						  //   <TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
						  //     	<Card>
							 //      	<Image 
							 //      		style={styles.banner6} 
							 //      		source = {{ uri:urls.MainScreenBanner2}}
							 //      	/>
						  //     	</Card>
						  //   </TouchableOpacity>
						  //   <TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
						  //     	<Card>
							 //      	<Image 
							 //      		style={styles.banner5} 
							 //      		source = {{ uri:urls.MainScreenBanner12}}
							 //      	/>
						  //     	</Card>
						  //   </TouchableOpacity>
					   //  </View>
				    //   	<View>
					   //    	<TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
					   //    		<Card>
						  //     		<Image 
						  //     			style={[styles.banner7]} 
						  //     			source = {{ uri:urls.MainScreenBanner3}}
						  //     		/>
					   //    		</Card>
				    //   		</TouchableOpacity>
					   //    	<TouchableOpacity onPress={()=>this.onClickEvent('ProductList')}>
					   //    		<Card>
						  //     		<Image 
						  //     			style={[styles.banner6]}
						  //     			source = {{ uri:urls.MainScreenBanner4}}
					   //    			/>
					   //    		</Card>
					   //    	</TouchableOpacity>
				    //   	</View>	
				   	// </View>
























	// 			   	MainScreenBanner1:bannerImage+'-main.jpg',
	// MainScreenBanner2:bannerImage+'-man.jpg',
	// MainScreenBanner3:bannerImage+'-mans-shoes.jpg',
	// MainScreenBanner4:bannerImage+'-women.jpg',
	// MainScreenBanner12:bannerImage+'new2.jpg',
















import { StyleSheet, Dimensions, StatusBar } from 'react-native'

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	container: {
		flex:1,
		backgroundColor: '#ffffff',
		width:width,
		height:height,
	},
	actionbarContainer:{
		paddingLeft:8,
	},
	actionbarTitleContainer:{
		marginRight:32,
		alignItems:'center',
	},
	homeLoginCart:{
		justifyContent:'center',
		height:50,
	},
	homeLoginView:{
		flexDirection:'row',
	},
	homeLoginImg:{
		width:50,
		height:50,
	},
	homeLoginText1:{
		paddingLeft:10,
		color:'#201e1f',
		fontSize:20,
		fontWeight:'bold',
	},
	homeLoginText2:{
		paddingLeft:10,
		color:'#201e1f',
		fontSize:16,
	},
	view1:{
		flex:1,
		backgroundColor:'#201e1f',
	},
	view2:{
		borderTopWidth:0.5,
		borderColor:'#ffffff',
	},
	container2: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	container3: {
		marginLeft:10,
		marginRight:10,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	button: {
		marginBottom: 30,
		width: 120,
		marginLeft:10,
		marginRight:10,
		alignItems: 'center',
		backgroundColor: '#2196F3'
	},
	buttonText: {
		padding: 10,
		color: 'white'
	},
	banner1:{
		width: responsiveWidth(100),
		height: responsiveHeight(25),
	// justifyContent:'center',
	},
	banner2:{
		margin:responsiveWidth(0.5),
		width: responsiveWidth(50),
		height: responsiveHeight(25),
		alignItems:'flex-start'
	},
	banner3:{
		margin:responsiveWidth(0.5),
		width: responsiveWidth(50),
		height: responsiveHeight(25),
		alignItems:'flex-end'
	},
	banner4 :{
		margin:responsiveWidth(0.5),
		width:responsiveWidth(50),
		height: responsiveHeight(25),
		alignItems:'flex-end'
	},
	banner5:{
		// margin:responsiveWidth(0.5),
		width: responsiveWidth(46),
		height: responsiveHeight(36),
		resizeMode:'contain',
		alignItems:'flex-start'
	},
	banner6:{
		// margin:responsiveWidth(0.5),
		width: responsiveWidth(45),
		height: responsiveHeight(25),
		resizeMode:'contain',
		alignItems:'flex-end'
	},
	banner7 :{
		width:responsiveWidth(46),
		height: responsiveHeight(36),
		resizeMode:'contain',
		alignItems:'flex-end',
	},
	drawerBg:{
		backgroundColor:'#201e1f',
	},
	drawerFont:{
		color:'white',
		fontSize:16,
	},
	drawerChidFont:{
		color:'white',
		fontSize:15,
	}
})

















