import React ,{Component} from 'react';
import {View,Text,Image,ToastAndroid,ScrollView,Button,AsyncStorage,Dimensions,ToolbarAndroid,Picker,TouchableHighlight,StyleSheet } from 'react-native';
import {Content,Container,Footer,Item,Card,Input,CardItem,Body,Thumbnail} from 'native-base';
import CheckBox from 'react-native-checkbox';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'

import ActionBar from 'react-native-action-bar';

import Loader from './Loader';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
export default class ProductUpdateScreen extends Component{
    constructor(){
        super()
        this.state={
            myData:[],
            qty:'',
            color:'',
            entityId:0,
            name:'',
            price:0,
            regularPrice:0,
            shortDescription:'',
            load:true,
            count:0,
            cartItemId:0,
        }
    }

    componentDidMount() {
        this.getData();
    }

    componentWillMount() {
        const analytics = new Analytics('UA-110543062-1');
        analytics.hit(new PageHit('ProductUpdateScreen'));
    }

    async getData(){

        var productId = await AsyncStorage.getItem('ProductId');
        var ItemId = await AsyncStorage.getItem('ItemId');

        this.setState({
            count:global.cartItemCount,
            cartItemId:global.itemId,
        });

        // ToastAndroid.show(''+productId, ToastAndroid.SHORT);
        let url='http://stageapp.invanos.net/xapi/products/getProductDetail/product_id/'+global.product_id;
        return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.model!==null){
                this.setState({
                    load:false,
                    entityId:responseJson.model.entity_id,
                    name:responseJson.model.name,
                    imageUrl:responseJson.model.image_url,
                    price:responseJson.model.price,
                    regularPrice:responseJson.model.regular_price_with_tax,
                    shortDescription:responseJson.model.short_description,
                });
            }
        })
        .catch((error) => {
            alert('ErrorMsg P0:'+error);
            this.setState({load:false});
        });
    }

    onImgClick=()=>{
        try {
            AsyncStorage.setItem('imgUrl',this.state.imageUrl);
            this.props.navigation.navigate('FullScreenImg');
        } catch (error) {
            alert('ErrorMsg P1:'+error);
        }
    };

    updateCart=()=>{
        this.setState({load:true});
        var data='cart['+global.itemId+'][qty]='+this.state.qty;

        fetchData.postMaster(urls.cartUpdate,data).then((responseJson)=>{
            if(responseJson.code===0){
                this.setState({load:false});
                this.props.navigation.navigate('CartScreen');
            }else{
                this.setState({load:false});
                ToastAndroid.show(''+responseJson.msg,ToastAndroid.SHORT);
            }
        })
    };

    onSelect(index, value){
        this.setState({color:value});
    };

    addToWishList=()=>{
        return fetch('http://stageapp.invanos.net/xapi/customer/status')
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.code==0){
                this.props.navigation.navigate('MyProfileTab');
            }else if(responseJson.code==5){
                ToastAndroid.show('Please sing up with us', ToastAndroid.SHORT);
            }
        })
        .catch((error) => {
            alert('ErrorMsg P3 :'+error);  
        });
    };

    render(){
        const {goBack} = this.props.navigation;

        cartClick=()=>{
            this.setState({load:true});
            return fetch('http://stageapp.invanos.net/xapi/cart/getCartInfo')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({load:false});
                // ToastAndroid.show(''+responseJson.msg, ToastAndroid.SHORT);
                if(responseJson.code === 5 &&responseJson.msg ==='Cart is empty!'){
                    this.props.navigation.navigate('EmptyCartScreen');
                }
                else if(responseJson.code===1 && responseJson.msg===null){
                    this.props.navigation.navigate('CartScreen');
                }
            })
            .catch((error) => {
                alert('ErrorMsg P4 :'+error);
                this.setState({load:false});
            });
        };


    	return(
    		<Container key={this.state.entityId} style={styles.container}>
                <ActionBar
                    backgroundColor={'#ffffff'}
                    containerStyle={{paddingLeft:8}}
                    titleContainerStyle={{marginLeft:32,alignItems:'center',}}
                    rightTouchableChildStyle={{width:35}}
                    leftIconName={'back-icon'}
                    onLeftPress={() =>  goBack()}
                    rightIcons={[
                        {
                            name: 'wish-icon',
                            onPress: () => this.addToWishList(),
                        },
                        {
                            name: 'cart-icon',
                            badge: this.state.count,
                            onPress: () => cartClick(),
                        },
                    ]}
                />
    			<Content>
    				<ScrollView>
          				<TouchableHighlight
          				style={styles.contentAlign}
          		 		onPress={this.onImgClick}>
    				        <Image style={{width: width, height: 280,}} source = {{ uri:this.state.imageUrl}}/>
    				    </TouchableHighlight>

                        <View style={styles.contentAlign}>
                            <Text style={styles.nameText}>{this.state.name}</Text>
                        </View>

                        <View style={styles.priceView}>
                            <Text style={styles.priceText}>₹ {Math.round(this.state.price)}</Text>
                            <Text style={{marginLeft:10, fontSize:16,textDecorationLine: 'line-through'}}>₹ {Math.round(this.state.regularPrice)}</Text>
                        </View>

                        <View>
                            <Text style={styles.descriptionTitle}>Description</Text>
                            <Text style={styles.descriptionText}>{this.state.shortDescription}</Text>
                        </View>
                    </ScrollView>
                </Content>
                <Footer style={styles.footer}>
                    <View style={styles.qtybtn}>
                        <Text style={styles.qtyText}>Qty :</Text>
                        <Picker 
                        style={styles.picker}
                        selectedValue={this.state.qty}
                        onValueChange={(itemValue, itemIndex) => this.setState({qty: itemValue})}>
                            <Picker.Item label="1" value="1"/>
                            <Picker.Item label="2" value="2"/>
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4"/>
                            <Picker.Item label="5" value="5" />
                        </Picker>
                    </View>

                    <TouchableHighlight onPress={()=>this.updateCart()} >
                        <View style={styles.footerbutton}>
                            <Text style={styles.buttonText}>UPDATE</Text>
                        </View>
                    </TouchableHighlight>
                </Footer>
                <Loader loading={this.state.load}/>
    		</Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        height:height,
        width:width,
        backgroundColor: '#ffffff',
    },
    contentAlign:{
        alignItems: 'center',
    },
    footer:{
        backgroundColor:'#201e1f',
        height:60
    },
    footerbutton: {
        width: width/1.5,
        height:60,
        backgroundColor:'#201e1f',
        alignItems: 'center',
    },
    picker:{
        flex: 1,
        marginRight:5,
        color:'white',
    },
    qtyText:{
        marginLeft:40,
        color: 'white',
        fontSize:16,
    },
    qtybtn:{
        width: 150,
        height:60,
        flexDirection: 'row',
        backgroundColor:'#201e1f',
        alignItems: 'center',
    },
    buttonText: {
        textAlign: 'center',
        marginTop:18,
        color: 'white',
        fontSize:16,
    },
    priceView:{
        marginTop:10,
        flex:1,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection: 'row',
    },
    nameText:{
        color:'#2a2a2a',
        marginTop:15,
        fontSize:20,
    },
    priceText:{
        color:'#201e1f',
        fontWeight:'bold',
        fontSize:26, 
    },
    descriptionTitle:{
        color:'#373737',
        fontSize:18,
        marginLeft:18,
        marginTop:10,
        marginBottom:10,
    },
    descriptionText:{
        color:'#6a6a6a',
        fontSize:16,
        marginLeft:10,
        marginBottom:10,
        textAlign: 'center',
    },
})