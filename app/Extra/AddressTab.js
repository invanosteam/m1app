import React, {Component} from 'react';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import MaskTabBar from 'react-native-scrollable-tab-view-mask-bar';

import NewAddressScreen from'./NewAddressScreen';
import SavedAddressScreen from'./SavedAddressScreen';


export default class AddressTab extends Component {
    constructor(){
        super()
        this.state={
            initialPage:0,
        }
    }

    render() {
        return (
            <ScrollableTabView 
                renderTabBar={() => <MaskTabBar someProp={'here'} showMask={true} maskMode='light'/>}
                tabBarBackgroundColor='#ffffff'
                initialPage={global.MyProfileView}

                tabBarActiveTextColor='#201e1f'
                tabBarInactiveTextColor='#969696'
                tabBarUnderlineStyle={{backgroundColor:'#efac27'}}>
                    <NewAddressScreen tabLabel="NewAddress"/>
                    <SavedAddressScreen tabLabel="SavedAddress"/>
            </ScrollableTabView>
        );
    }
}
