import React, { Component } from 'react';
import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,ToolbarAndroid,List,StatusBar,
  ListView,TouchableOpacity,AsyncStorage,ToastAndroid,Checkbox,Picker,Dimensions} from 'react-native';
import { Container, Header,Body, Content, Form,Button, Item,Footer,Input,Segment,Label,Left,Right } from 'native-base';

import ActionBar from 'react-native-action-bar';

var {height, width} = Dimensions.get('window');


import CheckBox from 'react-native-checkbox';

export default class AddressScreen extends Component{
	 constructor(props) {
   super(props)

   this.state = {
    email:'',
   	firstName:'',
    middleName:'',
    lastName:'',
    company:'',
    address:'',
    street:'',
    city:'',
    currentState:'',
    zip:'',
    country:'',
    contactNo:'',
    fax:'',
    value: true,
    isForShipping:1,
    qty:'',
    countryList:[],
}
}

  componentDidMount() {
    this.getData();    
  }
  async getData(){
    var EmailId= await AsyncStorage.getItem('EmailId');
    this.setState({email:EmailId});
    ToastAndroid.show('emailId'+EmailId,ToastAndroid.SHORT);

     fetch('http://stageapp.invanos.net/xapi/checkout/getcountries')
    .then((response) => response.json())
    .then((responseJson) => {
      ToastAndroid.show('responseJson:'+responseJson.msg,ToastAndroid.SHORT);

      this.setState({countryList:responseJson.model.countries})
    })
    .catch((error) => {
      console.error(error);
    });
}

 // ToastAndroid.show('Welcome '+ firstName+','+middleName+','+lastName+','+company+','+address+','+street+','+city+','+currentState+','
 //   +country+','+zip+','+contactNo+','+fax, ToastAndroid.SHORT);

checkBoxData=(data)=>{

  if(data==true){
    this.setState({value:false,isForShipping:0})
  }else{
    this.setState({value:true,isForShipping:1})
  }
 }
 
 setAddress=()=>{
  const { firstName,middleName,lastName,company,address,street,city,currentState,country,zip,contactNo,fax,email,isForShipping } = this.state

    ToastAndroid.show(''+this.state.isForShipping, ToastAndroid.SHORT)

  if(firstName!='' && lastName!='' && address!='' && street!='' && city!='' && currentState!='' && country!='' && zip!='' && contactNo!=''){
    ToastAndroid.show('not null '+email+','+ city+','+country, ToastAndroid.SHORT);

  fetch('http://stageapp.invanos.net/xapi/checkout/setBilling', {
    method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
       },
       body:'billing[email]='+email+'&billing[address_id]=null&billing[firstname]='+firstName+'&billing[middlename]='+middleName+'&billing[lastname]='+lastName+'&billing[company]='+company+'&billing[street][]='+address+'&billing[street][]='+street+'&billing[city]='+city+'&billing[region_id]=null&billing[region]='+currentState+'&billing[postcode]='+zip+'&billing[country_id]='+country+'&billing[telephone]='+contactNo+'&billing[fax]='+fax+'&billing[save_in_address_book]=1&billing[use_for_shipping]='+isForShipping+'&billing_address_id:null'

    }).then((response) => response.json())
      .then((responseData) => {

        if(responseData.code==0){
          alert('msg : '+responseData.msg);
            if(isForShipping===0){
                this.props.navigation.navigate('SetShippingAddress');
            }else{
                this.props.navigation.navigate('ShippingMethod');
            }
        }else if(responseData.code===1){
                  alert('errormsg : '+responseData.error);
        }
      }).done();
  }else{
        ToastAndroid.show('Address data is Empty', ToastAndroid.SHORT);
  }
 };
 nextScreen=()=>{
    this.props.navigation.navigate('ShippingMethod');
 };

 render(){
        let cDataList=this.state.countryList.map((item,index)=>{
      // ToastAndroid.show('Welcome '+item.name, ToastAndroid.SHORT);
      // alert("Data"+item.name);
      return(
        <Picker.Item key={index} value={item.country_id} label={item.name} />
        )
    });
 	return(
    <Container>
      <StatusBar barStyle="dark-content"/>

        <Content padder style={styles.container}>
           <Segment >
            <Button first style={{backgroundColor:'#201e1f',borderColor:'#201e1f'}}>
              <Text style={{color:'white'}}>New address</Text>
            </Button>
            <Button last style={{borderColor:'#201e1f'}}>
              <Text style={{color:'#949494'}}>Saved address</Text>
            </Button>
          </Segment>
                
          <Form>
          <View style={styles.rowView}>
            <Item floatingLabel style={{width:170}}>
              <Label >First Name</Label>
              <Input
              onChangeText={(firstName) => this.setState({firstName:firstName})}
              />
            </Item>

            <Item floatingLabel style={{width:150}}>
              <Label>Middle Name</Label>
              <Input 
              onChangeText={(middleName) => this.setState({middleName:middleName})}
              />
            </Item>        
          </View>

            <Item floatingLabel style={{width:150}}>
              <Label>Last Name</Label>
              <Input 
              onChangeText={(lastName) => this.setState({lastName:lastName})}
              />
            </Item>

            <Item floatingLabel style={{width:200}}>
              <Label>Company</Label>
              <Input 
              onChangeText={(company) => this.setState({company:company})}
              />
            </Item>

            <Item floatingLabel style={{width:300}}>
              <Label>Address</Label>
              <Input 
              onChangeText={(address) => this.setState({address:address})}
              />
            </Item>

              <Item floatingLabel style={{width:300}}>
              <Label>Street Address</Label>
              <Input 
              onChangeText={(street) => this.setState({street:street})}
              />
            </Item>

             <Item floatingLabel>
              <Label>Contact No</Label>
              <Input 
              onChangeText={(contactNo) => this.setState({contactNo:contactNo})}
              maxLength = {10}
              keyboardType = 'numeric'
              autoCapitalize={'none'}
              />
            </Item>

            <View style={styles.rowView}>
              <View style={{marginLeft:15,marginTop:10}}>
                <Label>City</Label>
                <Picker style={styles.picker}
                onValueChange={(itemValue, itemIndex) => this.setState({city:itemValue})}
                selectedValue={this.state.city}>
                <Picker.Item label="Mumbai" value="Mumbai"/>
                <Picker.Item label="Delhi" value="Delhi"/>
                <Picker.Item label="Bangalore" value="Bangalore"/>
                <Picker.Item label="Bangalore" value="Bangalore"/>
                <Picker.Item label="Ahmedabad" value="Ahmedabad"/>
                <Picker.Item label="Chennai" value="Chennai"/>
                <Picker.Item label="Kolkata" value="Kolkata"/>
                <Picker.Item label="Surat" value="Surat"/>
                <Picker.Item label="Pune" value="Pune"/>
                <Picker.Item label="Jaipur" value="Jaipur"/>
                </Picker>
              </View>

              <View style={{marginLeft:15,marginTop:10}}>
                <Label>State</Label>
                <Picker style={styles.picker}
                onValueChange={(itemValue, itemIndex) => this.setState({currentState: itemValue})}
                selectedValue={this.state.currentState}>
                <Picker.Item label="Maharashtra" value="Maharashtra"/>
                <Picker.Item label="Delhi" value="Delhi"/>
                <Picker.Item label="Karnataka" value="Karnataka" />
                <Picker.Item label="Telangana" value="Telangana"/>
                <Picker.Item label="Gujarat" value="Gujarat" />
                </Picker>
              </View>
            </View>

            <View style={styles.rowView}>
              <View style={{marginLeft:15,marginTop:10}}>
                  <Label>Country</Label>
                  <Picker style={styles.picker}
                  selectedValue={this.state.country}
                  onValueChange={(country) => this.setState({country: country})}>
                  <Picker.Item label="Choose country"/>
                  {cDataList}
                  </Picker>
                </View>

              <Item floatingLabel style={{width:150}}>
                <Label>Zip Code</Label>
                <Input
                 maxLength = {8}
                 keyboardType = 'numeric'
                 onChangeText={(zip) => this.setState({zip:zip})}
                />
              </Item>
            </View>

            <Item floatingLabel>
              <Label>Fax</Label>
              <Input
               maxLength = {8}
               keyboardType = 'numeric'
               onChangeText={(fax) => this.setState({fax:fax})}
              />
            </Item>

            <View  style={styles.checkBox}>
            <CheckBox
            label='Same for Shipping?'
            checkedImage={require('../logos/checked.png')}
            uncheckedImage={require('../logos/unchecked.png')}
            onChange={(checked) =>this.checkBoxData(checked)}
            checked={this.state.value}/>
            </View>

          </Form>

          <Text style={{marginTop:20 ,marginLeft:15 ,color:'#282828' }}>Delivery</Text>

          <View style={{justifyContent:'center',flexDirection:'row',marginTop:20,marginBottom:20}}>
            <Button style={{margin:5,width:80,height:50,backgroundColor: '#ffffff'}}>
            <Text style={{marginLeft:10}}>Test</Text>
            </Button>

            <Button style={{margin:5,marginLeft:10, width:80,height:50,backgroundColor:'#ffffff'}}>
            <Text style={{marginLeft:10}}>Test</Text>
            </Button>

            <Button style={{margin:5,marginLeft:10, width:80,height:50,backgroundColor:'#ffffff'}}>
            <Text style={{marginLeft:10 }}>Test</Text>
            </Button>
          </View>

          <Footer style={{backgroundColor:'#201e1f'}}>
          <TouchableOpacity onPress={this.setAddress}>
          <View style={styles.footerbutton}>
          <Text style={styles.buttonText}>CONTINUE TO PAYMENT</Text>
          </View>
          </TouchableOpacity>
          </Footer>
                    </Content>

      </Container>
			);
	}
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
  },
  rowView:{
    flexDirection: 'row',
  },
  picker:{
    width:170,
  },
  checkBox:{
    marginLeft:15,
    marginTop:25,
    marginBottom:25,

  },
   footerbutton: {
    width: 300,
    marginTop:10,
    marginLeft:35,
    marginRight:35,
    alignItems: 'center',
  },
  button:{
    marginBottom: 30,
    width: 100,
    marginLeft:25,
    marginRight:25,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  buttonText: {
    padding: 10,
    color: 'white'
  },

})