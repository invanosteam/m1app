import React, {Component} from 'react';

import {View,Text,Image,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,Dimensions,ToolbarAndroid,List,ListView,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import { Container, Content,Button,Footer } from 'native-base';

import FooterTabBar from './FooterTabBar';

var {height, width} = Dimensions.get('window');


export default class EmptyCartScreen extends Component {

    render() {
        return (
            <Container style={styles.container}>
                <View style={styles.mainView}>
                    <Text style={styles.label}>Nothing here</Text>
                    <Image style={styles.logo} source = {require('../logos/emptycart.png')}/>
                    <Text style={styles.labelOffers}>Checkout great products & offers</Text>

                    <View style={{marginTop:10}}>
                        <Button rounded style={styles.button} onPress={()=>this.props.Click}>
                            <Text style={styles.buttonText}>Shop Now</Text>
                        </Button>
                    </View>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        width:width,
        height:height,
    },
    mainView:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    label:{
        marginBottom:10,
        fontSize: 20,
        justifyContent: 'center',
    },
    labelOffers:{
        marginBottom:10,
        fontSize: 18,
        justifyContent: 'center',
    },
    editText:{
        width:250,
        height:40
    },
    button: {
        marginTop:15,
        marginBottom: 10,
        width: 200,
        height:50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#201e1f'
    },
    buttonText: {
        color: 'white'
    },
    alternativeLayoutButtonContainer: {
        alignItems: 'center',
        paddingTop:20
    },
    logo:{
        height:100,
        width:100,
        marginLeft:50,
        marginRight:50,
        marginBottom: 10,
    }
})

