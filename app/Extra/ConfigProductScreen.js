import React ,{Component} from 'react';
import {View,Text,Image,ToastAndroid,ScrollView,Alert,ListView,ActivityIndicator,Button,TouchableHighlight,TouchableWithoutFeedback,Dimensions,AsyncStorage,Picker,ToolbarAndroid,TouchableOpacity,StyleSheet } from 'react-native';
import {Content,Container,Footer,Item,Label,Card,CardItem,Body,Thumbnail} from 'native-base';
import ActionBar from 'react-native-action-bar';
import RadioGroup from 'react-native-custom-radio-group1';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import CheckBox from 'react-native-checkbox';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import Loader from './Loader';

var {height, width} = Dimensions.get('window');

import styles from '../styles/ConfigProductScreen.style';

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class ConfigProductScreen extends Component{
	constructor(){
		super()
		this.state={
			myData:[],
			configData:[],
			qty:1,
			color:'',
			entityId:0,
			name:'',
			price:0,
			regularPrice:0,
			shortDescription:'',
			superAttribute:'',
			selectedOption:'',
			load:true,
			clicked:false,
			count:0,
			relatedProducts:ds,
            productImages:ds,
            array:[],
		}
	}

	componentDidMount() {
		this.getData();
	}

	componentWillMount() {
		const analytics = new Analytics(urls.AppId);
		analytics.hit(new PageHit('ConfigProductScreen'));
	}
	
	getData(){
		var url=urls.productDetail+global.product_id;
        fetchData.getMaster(url).then((responseJson)=>{
	  		if(responseJson.model!=null){
				global.imageUrl=responseJson.model.image_url;
				var regularPrice=Math.round(responseJson.model.regular_price_with_tax);
				var price=Math.round(responseJson.model.price);

				if(responseJson.model.related!=null && responseJson.model.related!= ''){
                    for (let prop in responseJson.model.related) {
                        this.state.array.push(responseJson.model.related[prop]);
                    }
                 
                    this.setState({
                        relatedProducts:ds.cloneWithRows(this.state.array),
                    });
                }

				this.setState({
					productImages:ds.cloneWithRows(responseJson.model.mediaGallery),
                    productimage:responseJson.model.mediaGallery[0],
					// productimage:responseJson.model.image_url,
					entityId:responseJson.model.entity_id,
					count:global.cartItemCount,
					name:responseJson.model.name,
					price:'₹ '+price,
					regularPrice:'₹ '+regularPrice,
					shortDescription:responseJson.model.short_description,
					superAttribute:responseJson.model.attribute_options.label.Color,
					load:false,
				});

				var collection =[];
				for (let prop in responseJson.model.attribute_options.collection) {
					collection.push(responseJson.model.attribute_options.collection[prop]);
				}

				var labelsKey=[];
				for (let prop in responseJson.model.attribute_options.labels_key.Color) {
					labelsKey.push(responseJson.model.attribute_options.labels_key.Color[prop]);
				}

				var array1=[];
				if(labelsKey.length === collection.length){
					for (var i = 0; i < labelsKey.length; i++) {
						array1.push({'label':labelsKey[i],'value':collection[i]})
					}
				}

				// var array1=[
				// 	{'label':labelsKey[0],'value':collection[0]},
				// 	{'label':labelsKey[1],'value':collection[1]},
				// ];

				this.setState({
					configData:labelsKey,
					radioData:array1
				});
		    }
           
        })
	};

	changeImage=(rowData)=>{
        this.setState({productimage:rowData})
        global.imageUrl=rowData;
    }

	onClickEvent(screenName){
		this.props.navigation.navigate(screenName);
	};

	addToWishList=()=>{
		var url=urls.getCustomerStatus;
		fetchData.getMaster(url).then((responseJson)=>{
			if(responseJson.code==0){
				global.MyProfileView=2;
				this.onClickEvent('MyProfileTab');
			}else if(responseJson.code==5){
				Alert.alert(
                    'Sign in',
                    'Register with us and enjoy more feature!',
                    [
                        {
                            text: 'May be later',
                            onPress: () => {},
                            style: 'cancel'
                        },
                        { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
                    ],
                    { cancelable: false }
                );
			}
		});
	};

	addToCart=()=>{
		if(this.state.selectedOption!=''){
			this.setState({clicked:true});
			var url=urls.productAddToCart+this.state.entityId+'/qty/'+this.state.qty+'?'+'super_attribute['+this.state.superAttribute+']='+this.state.selectedOption;

			fetchData.getMaster(url).then((responseJson)=>{
	            if(responseJson.code==0){
	                this.setState({clicked:false});
	                this.onClickEvent('CartScreen');
	            }else if(responseJson.code==5) {
	                alert(responseJson.msg);
	                this.setState({clicked:false});
	            }
	        });
		}else{
			ToastAndroid.show('Please select option', ToastAndroid.SHORT);
		}
	};

	onSelect(index, value){
	    this.setState({gender:value});
	};

	handleOnPress(value){
		this.setState({radioValue:value});
    }

	render(){
		const {goBack} = this.props.navigation;
		
		cartClick=()=>{
			this.onClickEvent('CartScreen');
		}

		configSelect=(selected)=>{
			this.setState({selectedOption:selected});
		};

		checkData=(data,rowData)=>{
            var url;
            if(data===false){
                url=urls.wishListAdd+rowData.entity_id

            }else{
                url=urls.wishListDel+rowData.entity_id
            }

            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.code==0&&responseJson.msg!=null){
                    this.setState({checked:true})
                    ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
                }else if(responseJson.code==5) {
                    Alert.alert(
                        'Sign in',
                        'Register with us and enjoy more feature!',
                        [
                            {
                                text: 'May be later',
                                onPress: () => {this.setState({checked:false})},
                                style: 'cancel'
                            },
                            { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
                        ],
                        { cancelable: false }
                    );
                }
            })
        };
		

		return(
			<Container key={this.state.myData.entity_id} style={styles.container}>
			    <ActionBar
			        backgroundColor={'#ffffff'}
			        containerStyle={styles.actionbarContainer}
		            titleContainerStyle={styles.actionbarTitleContainer}
		            rightTouchableChildStyle={{width:35}}
			        leftIconName={'back-icon'}
			        onLeftPress={() =>  goBack()}
			        rightIcons={[
			            {
			                name: 'wish-icon',
			                onPress: () => this.addToWishList(),
			            },
			            {
			                name: 'cart-icon',
			                badge: this.state.count,
			                onPress: () => cartClick(),
			            },
			        ]}
		        />
		        <Content>
			        <ScrollView>
				    {(!this.state.load)?
				    	<View style={styles.imgAlign}>
				     		<TouchableHighlight onPress={()=>this.onClickEvent('FullScreenImg')}>			             	
								<Image style={styles.img} source = {{ uri:this.state.productimage}}/>
							</TouchableHighlight>

							<ListView
	                            horizontal={true}
	                            dataSource={this.state.productImages}
	                            renderRow={(rowData) => 
	                                <TouchableOpacity onPress={()=>this.changeImage(rowData)}>
	                                    <View style={{borderColor:'#000000',borderWidth:0.5,margin:5}}>
	                                        <Image style={{height:80,width:80,resizeMode:'contain'}} source = {{ uri:rowData}}/>
	                                    </View>
	                                </TouchableOpacity>
	                            }
	                        />
                        </View>
		        		:
		        		null
		        	}

		        	{(!this.state.load)?
		        		<View>
							<View style={styles.contentAlign}>
				            	<Text style={styles.nameText}>{this.state.name}</Text>
							</View>

							<View style={styles.priceView}>
								<Text style={styles.priceText}>{this.state.price}</Text>
								<Text style={styles.regularPrice}>{this.state.regularPrice}</Text>
							</View>
						</View>
						:
						null
					}

					<View style={styles.colorOptions}>
		                <RadioGroup 
				            radioGroupList={this.state.radioData}
				            onChange={(selected)=> configSelect(selected)}
			            />
				    </View>

				    {(!this.state.load)?
						<View>
					    	<Text style={styles.descriptionTitle}>Description</Text>
							<Text style={styles.descriptionText}>{this.state.shortDescription}</Text>
			        	</View>
			        	:
			        	null
			        }
			        {(!this.state.load && (this.state.array).length>0)?
                        <View style={{margin:5,borderBottomColor:'#000000',borderBottomWidth:0.5,}}>
                            <View style={styles.contentAlign}>
                                <Text style={{fontSize:responsiveFontSize(2),color:'#efac27',fontWeight:'bold'}}>Similar Products</Text>
                            </View>
                            <ListView
                                horizontal={true}
                                style={{flex:1,}}
                                dataSource={this.state.relatedProducts}
                                renderRow={(rowData) => 
                                    <TouchableOpacity onPress={()=>this.passData(rowData)}>
                                        <View style={styles.containerRelated}>
                                            <View style={styles.imgView}>
                                                <Image style={styles.imgSize} source = {{ uri:rowData.image_url}}/>
                                            </View>
                                            <View style={styles.nameViewRelated}>
                                                <Text numberOfLines={1} style={styles.nameTextRelated}>{rowData.name}</Text>
                                            </View>
                                            <View style={styles.priceViewRelated}>
                                                <Text numberOfLines={1} style={styles.priceTextRelated}>₹ {Math.round(rowData.price)}</Text>
	                                            <View>
	                                                <CheckBox
	                                                    // label='.'
	                                                    checked={this.state.checked}
	                                                    labelStyle={{color:'white'}}
	                                                    uncheckedImage={require('../logos/wish-icon.png')}
	                                                    checkedImage={require('../logos/wish-icon1.png')}
	                                                    onChange={(data) =>checkData(data,rowData)}
	                                                />
	                                            </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        :
                        null
                    }
                 	{(!this.state.load && (this.state.array).length>0)?
                        <View style={{margin:5,borderBottomColor:'#000000',borderBottomWidth:0.5,}}>
                            <View style={styles.contentAlign}>
                                <Text style={{fontSize:responsiveFontSize(2),color:'#efac27',fontWeight:'bold'}}>You might be interested in</Text>
                            </View>
                            <ListView
                                horizontal={true}
                                style={{flex:1,}}
                                dataSource={this.state.relatedProducts}
                                renderRow={(rowData) => 
                                    <TouchableOpacity onPress={()=>this.passData(rowData)}>
                                        <View style={styles.containerRelated}>
                                            <View style={styles.imgView}>
                                                <Image style={styles.imgSize} source = {{ uri:rowData.image_url}}/>
                                            </View>
                                            <View style={styles.nameViewRelated}>
                                                <Text numberOfLines={1} style={styles.nameTextRelated}>{rowData.name}</Text>
                                            </View>
                                            <View style={styles.priceViewRelated}>
                                                <Text numberOfLines={1} style={styles.priceTextRelated}>₹ {Math.round(rowData.price)}</Text>
	                                            <View>
	                                                <CheckBox
	                                                    // label='.'
	                                                    checked={this.state.checked}
	                                                    labelStyle={{color:'white'}}
	                                                    uncheckedImage={require('../logos/wish-icon.png')}
	                                                    checkedImage={require('../logos/wish-icon1.png')}
	                                                    onChange={(data) =>checkData(data,rowData)}
	                                                />
	                                            </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        :
                        null
                    }
					</ScrollView>
				</Content>

				<Footer style={styles.footer}>
					<View style={styles.qtybtn}>
						<Text style={styles.qtyText}>Qty :</Text>
						<Picker 
							style={styles.picker}
							selectedValue={this.state.qty}
							onValueChange={(itemValue, itemIndex) => this.setState({qty: itemValue})}>
							<Picker.Item label="1" value="1"/>
							<Picker.Item label="2" value="2"/>
							<Picker.Item label="3" value="3" />
							<Picker.Item label="4" value="4"/>
							<Picker.Item label="5" value="5" />
						</Picker>
					</View>

					<TouchableWithoutFeedback onPress={this.addToCart}>
						{(this.state.clicked)?
	                        <View style={styles.view6}>
	                            <ActivityIndicator size={30} color='#efac27' />
	                        </View>
	                        :
							<View style={styles.footerbutton}>
							    <Text style={styles.buttonText}>ADD TO CART</Text>
							</View>
						}
					</TouchableWithoutFeedback>
				</Footer>
				<Loader loading={this.state.load}/>
			</Container>
		);
	}
}