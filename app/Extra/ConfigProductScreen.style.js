import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	container: {
		height:height,
		width:width,
		backgroundColor: '#ffffff',
	},
	actionbarContainer:{
		paddingLeft:8,
	},
	actionbarTitleContainer:{
		marginLeft:32,
		alignItems:'center',
	},
	checkBox:{
		marginLeft:15,
		marginTop:25
	},
	// radioView:{
	// 	width:width,
	// 	// justifyContent:'center',
	// 	// alignItems:'center',
	// 	marginTop:15,
	// 	marginBottom:15,
	// },
	imgAlign:{
		borderBottomColor:'#000000',
		borderBottomWidth:0.5,
        paddingBottom:10,
	},
	img:{
        borderWidth:1,
        width: responsiveWidth(100),
        height: responsiveHeight(50),
        resizeMode:'contain',
    },
	productTitle:{
		 textAlign: 'center',
	},
	footer:{
		backgroundColor:'#201e1f',
		height:60
	},
	qtybtn:{
		width: 150,
		height:60,
		flexDirection: 'row',
		backgroundColor:'#201e1f',
		alignItems: 'center',
	},
	qtyText:{
		marginLeft:40,
		color: 'white',
		fontSize:16,
	},
	picker:{
		flex: 1,
		marginRight:5,
		color:'white',
	},
	footerbutton: {
		width: width/1.5,
		height:60,
		backgroundColor:'#201e1f',
		alignItems: 'center',
	},
	view6:{
		justifyContent:'center',
		alignItems: 'center',
		width: width/1.5,
    },
	buttonText: {
		textAlign: 'center',
		marginTop:18,
		color: 'white',
		fontSize:16,
	},
	contentAlign:{
		// alignItems: 'center',
		marginLeft:15,
	},
	nameText:{
		color:'#2a2a2a',
    	marginTop:15,
    	fontSize:responsiveFontSize(2.5),
    	width:responsiveWidth(93),
	},
	priceView:{
        flex:1,
		marginTop:10,
        marginLeft:15,
        flexDirection: 'row',
		// justifyContent:'center',
		// flexDirection: 'row',
	},
	priceText:{
		color:'#201e1f',
		fontWeight:'bold',
		fontSize:responsiveFontSize(2.5),
	},
	regularPrice:{
		marginLeft:10, 
		fontSize:responsiveFontSize(2.2),
		marginTop:5,
		textDecorationLine:'line-through',
	},
	descriptionTitle:{
		color:'#373737',
		fontSize:responsiveFontSize(2.5),
		marginLeft:15,
		marginTop:10,
		marginBottom:10,
	},
	descriptionText:{
		color:'#6a6a6a',
		fontSize:responsiveFontSize(1.8),
		marginLeft:15,
		width:responsiveWidth(93),
		marginBottom:10,
		// textAlign: 'center',
	},
	colorOptions:{
		marginTop:5,
		flexDirection: 'row',
		// justifyContent: 'center'
	},
	containerRelated: {
        flex:1,
        backgroundColor: '#ffffff',
        margin:5,
    },
    imgView:{
        borderBottomWidth:1,
        borderColor:'#ededed',
    },
    imgSize:{
        width: width/2,
        height: 230,
        resizeMode: 'contain',
    },
    nameViewRelated:{
        marginLeft:12,
        marginTop:5,
        height:40,
    },
    nameTextRelated: {
        color:'#979797',
        fontSize:responsiveFontSize(1.8),
        width:responsiveWidth(40),
    },
    priceViewRelated:{
        flex:1,  
        flexDirection:'row',
        marginBottom:5,
        marginTop:5,
    },
    priceTextRelated:{
        color:'#201e1f',
        fontSize:responsiveFontSize(2),
        fontWeight: 'bold',
        marginLeft:12,
        width:responsiveWidth(33),
    },
})