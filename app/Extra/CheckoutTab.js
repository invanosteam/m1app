import React, {Component} from 'react';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import MaskTabBar from 'react-native-scrollable-tab-view-mask-bar';

import AddressTab from'./AddressTab';
import PaymentMethod from'./PaymentMethod';
import OrderPlace from'./OrderPlace';



export default class CheckoutTab extends Component {
    constructor(){
        super()
        this.state={
            initialPage:0,
        }
    }

    render() {
        return (
            <ScrollableTabView 
                renderTabBar={() => <MaskTabBar someProp={'here'} showMask={false} maskMode='light' />}
                tabBarBackgroundColor='#ffffff'
                initialPage={global.MyProfileView}

                tabBarActiveTextColor='#201e1f'
                tabBarInactiveTextColor='#969696'
                tabBarUnderlineStyle={{backgroundColor:'#efac27'}}>
                    <AddressTab tabLabel="Address" />
                    <PaymentMethod tabLabel="Payment"/>
                    <OrderPlace tabLabel="OrderPlace" />
            </ScrollableTabView>
        );
    }
}
