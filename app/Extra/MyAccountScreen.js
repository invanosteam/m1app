
import React, {Component} from 'react';
import {View,Text,Image,Button,ScrollView,DrawerLayoutAndroid,TouchableHighlight,StyleSheet,ToolbarAndroid,List,ListView,Dimensions,TouchableOpacity,AsyncStorage,ToastAndroid} from 'react-native';
import {Content,Container,Item,Card,CardItem,Body,Thumbnail,Footer} from 'native-base';

import FooterTabBar from './FooterTabBar'

var {height, width} = Dimensions.get('window');


export default class MyAccountScreen extends Component {
    constructor(){
        super()
        this.state={
            customerName:'',
            customerEmail:'',
            customerShippingAmount:'',
            customerOdrerDate:'',
            customerOrderId:'',
            customerOdrerSubtotal:'',
            customerOdrerGrandtotal:'',
            activeTabNo:4,
        }
    }

    componentDidMount() {
        this.getUserInfo();
        this.getUserOrderInfo();
    }

    getUserInfo(){
        return fetch('http://stageapp.invanos.net/xapi/customer/status')
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.code===0 && responseJson.msg===null){
                if(responseJson.model!=null){
                    this.setState({
                        customerName:responseJson.model.name,
                        customerEmail:responseJson.model.email
                    });  
                }
            }else{
                ToastAndroid.show('Please try again'+responseJson.msg, ToastAndroid.SHORT);
            }
        })
        .catch((error) => {
        console.error(error);
        ToastAndroid.show('Welcome '+error, ToastAndroid.SHORT);
        });
    }
    getUserOrderInfo(){
      return fetch('http://stageapp.invanos.net/xapi/Order/getOrderList')
          .then((response) => response.json())
          .then((responseJson) => {
          // ToastAndroid.show('Welcome '+responseJson.model[0].order_id, ToastAndroid.SHORT);

            if(responseJson.code===0 && responseJson.msg===null){
              if(responseJson.model[0]!=null){
              this.setState({
                  customerOrderId:responseJson.model[0].order_id,
                  customerOdrerDate:responseJson.model[0].created_at,
                  customerShippingAmount:responseJson.model[0].shipping_amount,
                  customerOdrerSubtotal:responseJson.model[0].subtotal,
                  customerOdrerGrandtotal:responseJson.model[0].grand_total
                });  
              }
            }else{
              ToastAndroid.show('Please try again getOrderList'+responseJson.msg, ToastAndroid.SHORT);
            }
        })
        .catch((error) => {
          console.error(error);
          ToastAndroid.show('Welcome '+error, ToastAndroid.SHORT);
        });
    }

	onClick=()=>{
		
	};
  
  render() {

    return (
      <Container style={styles.container}>
    	<ScrollView>
		    <Card>
	    	<TouchableHighlight>
		    <CardItem style={styles.profileContainer}>
			    <View>
			        <Thumbnail large style={{justifyContent: 'center'}} source = {{uri:'http://stageapp.invanos.net/mainbanner.jpg'}}/>
				</View>
				<View style={styles.profileName}>
					<Text>{this.state.customerName}</Text>
					<Text>{this.state.customerEmail}</Text>
				</View>
       	    </CardItem>
       	    </TouchableHighlight>
	   	    </Card>

	   	    <Card>
   	    	<TouchableHighlight>
	        <CardItem style={styles.container}>
		        <View>
		        <Text>OrderReview</Text>
            <Text>{this.state.customerOrderId}</Text>
            <Text>{this.state.customerOdrerDate}</Text>
            <Text>{this.state.customerShippingAmount}</Text>
            <Text>{this.state.customerOdrerSubtotal}</Text>
            <Text>{this.state.customerOdrerGrandtotal}</Text>
		        </View>
	        </CardItem>
	        </TouchableHighlight>
		    </Card>

		    <Card>
	   	    <TouchableHighlight>
	        <CardItem style={styles.container}>
		        <View>
		        	<Text>OrderReviewScreen3</Text>
		        </View>
	        </CardItem>
   	        </TouchableHighlight>
		    </Card>

		    <Card>
	   	    <TouchableHighlight>
	        <CardItem style={styles.container}>
		        <View>
		        	<Text>OrderReviewScreen4</Text>
		        </View>
	        </CardItem>
	        </TouchableHighlight>
		    </Card>
	        <Card>
	        <CardItem >
		        <TouchableOpacity onPress={this.onClick}>
		        <View style={styles.button}>
		        <Text style={styles.buttonText}>GoTo Cart</Text>
		        </View>
		        </TouchableOpacity>
	        </CardItem>
	        </Card>
      </ScrollView>
        <Footer>
          <FooterTabBar activeTabNo={this.state.activeTabNo}/>
        </Footer>
        </Container>
        
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    width:width,
    height:height,
  },
  editTextContainer:{
    padding: 10
  },
  editText:{
    width:250,
    height: 40
  },
  button: {
    alignItems: 'center',
    width: 180,
    backgroundColor: '#2196F3',
  },
  buttonText: {
    padding: 10,
    color: 'white'
  },
  profileContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  profileName:{
  	marginLeft:10,
  }
})