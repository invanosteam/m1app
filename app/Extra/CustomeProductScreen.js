import React ,{Component} from 'react';
import {View,Text,Image,ToastAndroid,ScrollView,Alert,ActivityIndicator,ListView,Button,AsyncStorage,Dimensions,TouchableOpacity,ToolbarAndroid,Picker,TouchableHighlight,TouchableWithoutFeedback,StyleSheet } from 'react-native';
import {Content,Container,Footer,Item,Card,Input,Label,CardItem,Body,Thumbnail} from 'native-base';

import CheckBox from 'react-native-checkbox';

import Loader from './Loader';
import ActionBar from 'react-native-action-bar';
import SelectMultiple from 'react-native-select-multiple';

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import { Analytics, PageHit } from 'react-native-googleanalytics';

var {height, width} = Dimensions.get('window');

import styles from '../styles/CustomeProductScreen.style';

import urls from '../config/urls';

import FetchData from './FetchData';

const fetchData = new FetchData();
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class CustomeProductScreen extends Component{
    constructor(){
        super()
        this.state={
            clicked:false,
            qty:1,
            DropDown:'',
            color:'',
            entityId:0,
            name:'',
            shortDescription:'',
            load:true,
            customOptions:[],
            customOptionsId:[],
            customOptionsValue:[],
            selectedValue: [],
            multipleSelectId:'',
            checkedValue:[],
            checkboxId:'',
            opt:1,
            radioValue:0,
            radioId:'',
            customField:'',
            customFieldId:0,
            customArea:'',
            customAreaId:0,
            dropDownValue:'',
            customDropDownId:0,
            count:0,
            relatedProducts:ds,
            productImages:ds,
            array:[],
        }
    }
    componentDidMount() {
        this.getData();
    }

    componentWillMount() {
        const analytics = new Analytics(urls.AppId);
        analytics.hit(new PageHit('CustomeProductScreen'));
    }

    getData(){
        var url=urls.productDetail+global.product_id;

        fetchData.getMaster(url).then((responseJson)=>{
            if(responseJson.model!=null){
                var regularPrice=Math.round(responseJson.model.regular_price_with_tax);
                var price=Math.round(responseJson.model.price);
                global.imageUrl=responseJson.model.image_url;

                if(responseJson.model.related!=null && responseJson.model.related!=''){
                    for (let prop in responseJson.model.related) {
                        this.state.array.push(responseJson.model.related[prop]);
                    }
                 
                    this.setState({
                        relatedProducts:ds.cloneWithRows(this.state.array),
                    });
                }



                var customOptionsArray = [];
                for (let prop in responseJson.model.custom_options) {
                    customOptionsArray.push(responseJson.model.custom_options[prop]);
                }

                this.setState({
                    productImages:ds.cloneWithRows(responseJson.model.mediaGallery),
                    productimage:responseJson.model.mediaGallery[0],
                    // imageUrl:responseJson.model.image_url,
                    load:false,
                    count:global.cartItemCount,
                    entityId:responseJson.model.entity_id,
                    name:responseJson.model.name,
                    price:'₹ '+price,
                    regularPrice:'₹ '+regularPrice,
                    shortDescription:responseJson.model.short_description,
                    customOptions:customOptionsArray,
                });
            }
        });
    }

    onClickEvent(screenName){
    	this.props.navigation.navigate(screenName);
    };

    changeImage=(rowData)=>{
        this.setState({productimage:rowData})
        global.imageUrl=rowData;
    }

    addToWishList=()=>{
        var url=urls.getCustomerStatus;
        fetchData.getMaster(url).then((responseJson)=>{
            if(responseJson.code==0){
                global.MyProfileView=2;
                this.onClickEvent('MyProfileTab');
            }else if(responseJson.code==5){
                Alert.alert(
                    'Sign in',
                    'Register with us and enjoy more feature!',
                    [
                        {
                            text: 'May be later',
                            onPress: () => {},
                            style: 'cancel'
                        },
                        { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
                    ],
                    { cancelable: false }
                );
            }
        });
    };

    addToCart=()=>{
        multiselect=[]
        if(this.state.selectedValue!=''){
            for (var i = 0; i < this.state.selectedValue.length; i++) {
                multiselect.push('&options['+this.state.multipleSelectId+'][]='+this.state.selectedValue[i].value);
            } 
        }
        var multipleselectData=multiselect.join('');

        checkbox=[]
        if(this.state.checkedValue!=''){
            for (var i = 0; i < this.state.checkedValue.length; i++) {
                checkbox.push('&options['+this.state.checkboxId+'][]='+this.state.checkedValue[i].value);
            } 
        }
        var checkBoxData=checkbox.join('');
        // if(){
            this.setState({clicked:true});

            var url='http://stageapp.invanos.net/xapi/cart/add/product_id/'+this.state.entityId+'/qty/'+this.state.qty+'?options['+this.state.customFieldId+']='+this.state.customField+'&options['+this.state.customAreaId+']='+this.state.customArea+multipleselectData+checkBoxData+'&options['+this.state.radioId+']='+this.state.radioValue+'&options['+this.state.customDropDownId+']='+this.state.dropDownValue;

            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.code==0){
                    this.setState({clicked:false});
                    this.onClickEvent('CartScreen');
                }else if(responseJson.code==5) {
                    this.setState({clicked:false});
                    alert(responseJson.msg);
                }else{
                    this.setState({clicked:false});
                    alert(responseJson.msg);
                }
            });
        // }else{
        //     ToastAndroid.show('Please select option', ToastAndroid.SHORT);
        // }
    };


    onSelectionsChange = (selectedValue,item) => {
        this.setState({multipleSelectId:item.option_id,selectedValue})
    };

    onSelectionsChangeCheckBox = (checkedValue,item) => {
        this.setState({checkboxId:item.option_id,checkedValue});
    };

    onSelectionsChangeRadio=(radioValue,item) => {
        this.setState({radioId:item.option_id,radioValue});
    };

    render(){
        const {goBack} = this.props.navigation;

        let att=this.state.customOptions.map(function(item,index){

            if(item.custom_option_type==='field'){
                return(
                    <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
                        <Item floatingLabel style={styles.textArea}>
                            <Label>
                                <Text style={styles.label}>{item.custom_option_title}</Text>
                            </Label>
                            <Input 
                                style={styles.inputLabel}
                                onChangeText={(field) => this.setState({customField:field,customFieldId:item.option_id})}
                            />
                        </Item>
                    </View>
                )
            }

            if(item.custom_option_type==='checkbox'){
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                    array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                    data[i]={label:array[i].title ,value:array[i].option_type_id}
                }

                return(
                    <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
                        <SelectMultiple
                            items={data}
                            selectedItems={this.state.checkedValue}
                            onSelectionsChange={(value)=>this.onSelectionsChangeCheckBox(value,item)} 
                        />
                    </View>
                )
            }

            if(item.custom_option_type==='area'){
                return(
                    <View key={index} style={{borderWidth:1,paddingLeft:10,paddingRight:10}}>
                        <Item floatingLabel style={styles.textArea}>
                            <Label>
                                <Text style={styles.label}>{item.custom_option_title}</Text>
                            </Label>
                            <Input style={styles.inputLabel}
                                multiline={true}
                                numberOfLines={4}
                                onChangeText={(area) => this.setState({customArea:area,customAreaId:item.option_id})}
                            />
                        </Item>
                    </View>
                )
            }

            if(item.custom_option_type==='drop_down'){
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                    array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                    data[i]={label:array[i].title ,value:array[i].option_type_id}
                }

                let cDataList1=data.map((item,index)=>{
                    return(
                        <Picker.Item key={index} label={item.label} value={item.value}/>
                    )
                });

                return(
                    <View style={{borderWidth:1,flexDirection:'row',alignItems:'center',paddingLeft:10,paddingRight:10}} key={index}>
                        <Text style={{textAlign:'center'}}>Drop Down :</Text>
                        <Picker 
                            style={{width:250}}
                            selectedValue={this.state.dropDownValue}
                            onValueChange={(itemValue) => this.setState({dropDownValue: itemValue,customDropDownId:item.option_id})}>
                            {cDataList1}
                        </Picker>
                    </View>
                )
            }

            if(item.custom_option_type==='radio'){
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                    array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                    data[i]={label:array[i].title ,value:array[i].option_type_id}
                }
                return(
                    <View style={{borderWidth:1,padding:10}} key={index}>
                        <RadioForm
                            radio_props={data}
                            animation={true}
                            formHorizontal={true}
                            labelStyle={{fontSize:16}}
                            // style={{marginLeft:80}}
                            onPress={(value) =>this.setState({radioValue:value,radioId:item.option_id})}
                        />
                    </View>
                )
            }

            if(item.custom_option_type==='multiple'){                
                data=[]
                var array = [];
                for (let prop in item.custom_option_value) {
                    array.push(item.custom_option_value[prop]);
                }

                for (var i = 0; i < array.length; i++) {
                    data[i]={label:array[i].title ,value:array[i].option_type_id}
                }

                return(
                    <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
                        <SelectMultiple
                            items={data}
                            selectedItems={this.state.selectedValue}
                            onSelectionsChange={(value)=>this.onSelectionsChange(value,item)}
                        />
                    </View>
                )
            }

            // if(item.custom_option_type==='date'){
            //     return(
            //         <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
            //             <Text>Date</Text>
            //         </View>
            //     )
            // }

            // if(item.custom_option_type==='date_time'){
            //     return(
            //         <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
            //             <Text>Date Time</Text>
            //         </View>
            //     )
            // }

            // if(item.custom_option_type==='time'){
            //     return(
            //         <View style={{borderWidth:1,paddingLeft:10,paddingRight:10}} key={index}>
            //             <Text>Time</Text>
            //         </View>
            //     )
            // }

        },this);

        cartClick=()=>{
            this.onClickEvent('CartScreen');
        }

        checkData=(data,rowData)=>{
            var url;
            if(data===false){
                url=urls.wishListAdd+rowData.entity_id

            }else{
                url=urls.wishListDel+rowData.entity_id
            }

            fetchData.getMaster(url).then((responseJson)=>{
                if(responseJson.code==0&&responseJson.msg!=null){
                    this.setState({checked:true})
                    ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
                }else if(responseJson.code==5) {
                    Alert.alert(
                        'Sign in',
                        'Register with us and enjoy more feature!',
                        [
                            {
                                text: 'May be later',
                                onPress: () => {this.setState({checked:false})},
                                style: 'cancel'
                            },
                            { text: 'Sign in', onPress: () => this.onClickEvent('Login')}
                        ],
                        { cancelable: false }
                    );
                }
            })
        };

    	return(
    		<Container key={this.state.entityId} style={styles.container}>
                <ActionBar
                    backgroundColor={'#ffffff'}
                    containerStyle={styles.actionbarContainer}
                    titleContainerStyle={styles.actionbarTitleContainer}
                    rightTouchableChildStyle={{width:35}}
                    leftIconName={'back-icon'}
                    onLeftPress={() =>  goBack()}
                    rightIcons={[
                        {
                            name: 'wish-icon',
                            onPress: () => this.addToWishList(),
                        },
                        {
                            name: 'cart-icon',
                            badge: this.state.count,
                            onPress: () => cartClick(),
                        },
                    ]}
                />
                <Content>
                    <ScrollView>
                        {(!this.state.load)?
                            <View style={styles.imgAlign}>
                                <TouchableHighlight onPress={()=>this.onClickEvent('FullScreenImg')}>
                                    <Image style={styles.img} source = {{ uri:this.state.productimage}}/>
                                </TouchableHighlight>
                                <ListView
                                    horizontal={true}
                                    // style={{}}
                                    dataSource={this.state.productImages}
                                    renderRow={(rowData) => 
                                        <TouchableOpacity onPress={()=>this.changeImage(rowData)}>
                                            <View style={{borderColor:'#000000',borderWidth:0.5,margin:5}}>
                                                <Image style={{height:80,width:80,resizeMode:'contain'}} source = {{ uri:rowData}}/>
                                            </View>
                                        </TouchableOpacity>
                                    }
                                />
                            </View>
                            :
                            null
                        }
                        {(!this.state.load)?
                            <View style={styles.contentAlign}>
                                <Text style={styles.nameText}>{this.state.name}</Text>
                            </View>
                            :
                            null
                        }

                        {(!this.state.load)?
                            <View>
                                <View style={styles.priceView}>
                                    <Text style={styles.priceText}>{this.state.price}</Text>
                                    <Text style={styles.regularPrice}>{this.state.regularPrice}</Text>
                                </View>

                                <View>
                                    <Text style={styles.descriptionTitle}>Description</Text>
                                    <Text style={styles.descriptionText}>{this.state.shortDescription}</Text>
                                </View>
                            </View>
                            :
                            null
                        }
                        {att}
                        {(!this.state.load && (this.state.array).length>0)?
                        <View style={{margin:5,borderBottomColor:'#000000',borderBottomWidth:0.5,}}>
                            <View style={styles.contentAlign}>
                                <Text style={{fontSize:responsiveFontSize(2),color:'#efac27',fontWeight:'bold'}}>Similar Products</Text>
                            </View>
                            <ListView
                                horizontal={true}
                                style={{flex:1,}}
                                dataSource={this.state.relatedProducts}
                                renderRow={(rowData) => 
                                    <TouchableOpacity onPress={()=>this.passData(rowData)}>
                                        <View style={styles.containerRelated}>
                                            <View style={styles.imgView}>
                                                <Image style={styles.imgSize} source = {{ uri:rowData.image_url}}/>
                                            </View>
                                            <View style={styles.nameViewRelated}>
                                                <Text numberOfLines={1} style={styles.nameTextRelated}>{rowData.name}</Text>
                                            </View>
                                            <View style={styles.priceViewRelated}>
                                                <Text numberOfLines={1} style={styles.priceTextRelated}>₹ {Math.round(rowData.price)}</Text>
                                                <View>
                                                    <CheckBox
                                                        // label='.'
                                                        checked={this.state.checked}
                                                        labelStyle={{color:'white'}}
                                                        uncheckedImage={require('../logos/wish-icon.png')}
                                                        checkedImage={require('../logos/wish-icon1.png')}
                                                        onChange={(data) =>checkData(data,rowData)}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        :
                        null
                    }
                    {(!this.state.load && (this.state.array).length>0)?
                        <View style={{margin:5,borderBottomColor:'#000000',borderBottomWidth:0.5,}}>
                            <View style={styles.contentAlign}>
                                <Text style={{fontSize:responsiveFontSize(2),color:'#efac27',fontWeight:'bold'}}>Similar Products</Text>
                            </View>
                            <ListView
                                horizontal={true}
                                style={{flex:1,}}
                                dataSource={this.state.relatedProducts}
                                renderRow={(rowData) => 
                                    <TouchableOpacity onPress={()=>this.passData(rowData)}>
                                        <View style={styles.containerRelated}>
                                            <View style={styles.imgView}>
                                                <Image style={styles.imgSize} source = {{ uri:rowData.image_url}}/>
                                            </View>
                                            <View style={styles.nameViewRelated}>
                                                <Text numberOfLines={1} style={styles.nameTextRelated}>{rowData.name}</Text>
                                            </View>
                                            <View style={styles.priceViewRelated}>
                                                <Text numberOfLines={1} style={styles.priceTextRelated}>₹ {Math.round(rowData.price)}</Text>
                                                <View>
                                                    <CheckBox
                                                        // label='.'
                                                        checked={this.state.checked}
                                                        labelStyle={{color:'white'}}
                                                        uncheckedImage={require('../logos/wish-icon.png')}
                                                        checkedImage={require('../logos/wish-icon1.png')}
                                                        onChange={(data) =>checkData(data,rowData)}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        :
                        null
                    }
                    </ScrollView>
                </Content>

    			<Footer style={styles.footer}>
                    <View style={styles.qtybtn}>
    					<Text style={styles.qtyText}>Qty :</Text>
    					<Picker style={styles.picker}
    					selectedValue={this.state.qty}
    					onValueChange={(itemValue, itemIndex) => this.setState({qty: itemValue})}>

    					<Picker.Item label="1" value="1"/>
    					<Picker.Item label="2" value="2"/>
    					<Picker.Item label="3" value="3" />
    					<Picker.Item label="4" value="4"/>
    					<Picker.Item label="5" value="5" />
    					</Picker>
                    </View>

        			<TouchableWithoutFeedback onPress={this.addToCart}>
                        {(this.state.clicked)?
                            <View style={styles.view6}>
                                <ActivityIndicator size={30} color='#efac27' />
                            </View>
                            :
            				<View style={styles.footerbutton}>
            				    <Text style={styles.buttonText}>ADD TO CART</Text>
            				</View>
                        }
        			</TouchableWithoutFeedback>
    			</Footer>
                <Loader loading={this.state.load}/>
    		</Container>
        );
    }
}

// const styles = StyleSheet.create({
//     container: {
//         flex:1,
//         height:height,
//         width:width,
//     	backgroundColor: '#ffffff',
//     },
//     actionbarContainer:{
//         paddingLeft:8,
//     },
//     actionbarTitleContainer:{
//         marginLeft:32,
//         alignItems:'center',
//     },
//     checkBox:{
//         marginLeft:15,
//         marginTop:25
//     },
//     imgAlign:{
//         alignItems: 'center',
//     },
//     productTitle:{
// 	   textAlign: 'center',
//     },
//     footer:{
//         backgroundColor:'#201e1f',
//         height:60
//     },
//     picker:{
//     	flex: 1,
//     	marginRight:5,
//     	color:'white',
//     },
//     qtyText:{
//     	marginLeft:40,
//         color: 'white',
//         fontSize:16,
//     },
//     qtybtn:{
//     	width: 150,
//         height:60,
//         flexDirection: 'row',
//         backgroundColor:'#201e1f',
//         alignItems: 'center',
//     },
//     footerbutton: {
//         width: width/1.5,
//         height:60,
//         backgroundColor:'#201e1f',
//         alignItems: 'center',
//     },
//     view6:{
//         justifyContent:'center',
//         alignItems: 'center',
//         width: width/1.5,
//     },
//     buttonText: {
//         textAlign: 'center',
//         marginTop:18,
//         color: 'white',
//         fontSize:16,
//     },
//     contentAlign:{
//         alignItems: 'center',
//     },
//     priceView:{
//         marginTop:10,
//         flex:1,
//         alignItems: 'center',
//         justifyContent:'center',
//         flexDirection: 'row',
//     },
//     nameText:{
//     	color:'#2a2a2a',
//     	marginTop:15,
//     	fontSize:20,
//     },
//     priceText:{
//     	color:'#201e1f',
//         fontWeight:'bold',
//         fontSize:26, 
//     },
//     regularPrice:{
//         marginLeft:10,
//         fontSize:16,
//         textDecorationLine:'line-through',
//     },
//     descriptionTitle:{
//     	color:'#373737',
//     	fontSize:18,
//     	marginLeft:18,
//     	marginTop:10,
//     	marginBottom:10,
//     },
//     descriptionText:{
//     	color:'#6a6a6a',
//     	fontSize:16,
//     	marginLeft:10,
//     	marginBottom:10,
//         textAlign: 'center',
//     },
//     colorOptions:{
//     	flexDirection: 'row',
//     	justifyContent: 'center'
//     },
// })