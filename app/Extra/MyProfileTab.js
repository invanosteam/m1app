import React, {Component} from 'react';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import MaskTabBar from 'react-native-scrollable-tab-view-mask-bar';

import ProfileScreen from'./ProfileScreen';
import MyOrderScreen from'./MyOrderScreen';
import OrderInfoScreen from'./OrderInfoScreen';
import WishListScreen from'./WishListScreen';
import MySavedAddress from'./MySavedAddress';


export default class MyProfileTab extends Component {
    constructor(){
        super()
        this.state={
            initialPage:0,
        }
    }

    onClick=()=>{
        this.props.navigation.navigate('MainScreen');
    };

    render() {


        return (
            <ScrollableTabView 
                renderTabBar={() => <MaskTabBar someProp={'here'} showMask={true} maskMode='light' />}
                tabBarBackgroundColor='#ffffff'
                initialPage={global.MyProfileView}
                tabBarActiveTextColor='#201e1f'
                tabBarInactiveTextColor='#969696'
                tabBarUnderlineStyle={{backgroundColor:'#efac27'}}>
                    <ProfileScreen tabLabel="Profile" />
                    <MyOrderScreen tabLabel="My order"/>
                    <WishListScreen tabLabel="Wishlist"  onClick={this.onClick}/>
                    <MySavedAddress tabLabel="Saved address" />
            </ScrollableTabView>
        );
    }
}
