import React, { Component } from 'react';
import { ToastAndroid,StyleSheet,ActivityIndicator,BackHandler,Text,ScrollView,TouchableWithoutFeedback,TextInput,Image,AsyncStorage,TouchableOpacity,ToolbarAndroid,View,Dimensions } from 'react-native';
import { Container, Header, Content, Form,Button,Icon,Item,Input,Label,Left,Right } from 'native-base';

import FBSDK, {LoginManager,AccessToken } from 'react-native-fbsdk';

import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';

import { Analytics, PageHit } from 'react-native-googleanalytics';

import { NavigationActions } from 'react-navigation';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import ActionBar from 'react-native-action-bar';

import color from '../../styles/colors';

var {height, width} = Dimensions.get('window');

export default class AppButton extends Component {
  	constructor(props) {
	    super(props);
  	}


	render() {
		return(
			<View>
			    <Button rounded style={[styles.button,color.buttonColor]} onPress={this.props.onPress}>
			        {(this.props.clicked)?
			            <View style={{flex:1,justifyContent:'center'}}>
			                <ActivityIndicator size={responsiveHeight(3)} color='#efac27' />
			            </View>
			            :
			            <Text style={styles.buttonText}>{this.props.buttonTitle}</Text>
			        }
			    </Button>
			</View>
		);
	}
}

const styles = StyleSheet.create({
    buttonView:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
        marginTop:15,
    },
    button:{
        marginTop:15,
        marginBottom: 10,
        width: responsiveWidth(50),
        height:responsiveHeight(6.5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00acec'
    },
    buttonText: {
        color: 'white',
        fontSize:responsiveFontSize(1.6),
        fontWeight: 'bold',
    },
})
