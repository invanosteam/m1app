import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        width:width,
        height:height,
        flex:1,
        backgroundColor: '#ffffff',
    },
    view1:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    view2:{
        flexDirection:'row',
        marginTop:responsiveHeight(1),
        justifyContent:'center',
    },
    view3:{
        width:responsiveWidth(100),
        height:responsiveHeight(40),
    },
    view4:{
        marginBottom:5,
    },
    text1:{
        marginTop:responsiveHeight(1),
        textAlign:'center',
        fontSize:responsiveFontSize(3),
        color:'#efac27',
        fontWeight: 'bold',
    },
    text2:{
        marginTop:5,
        textAlign:'center',
        fontSize:responsiveFontSize(1.6),
        color:'#2c2c2c',
    },
    text3:{
        textAlign:'center',
        fontSize:responsiveFontSize(1.6),
    },
    text4:{
        color:'#353535',
        fontSize:responsiveFontSize(1.6),
    },
    text5:{
        marginTop:0,
        textAlign:'center',
        fontSize:responsiveFontSize(2),
        color:'#efac27',
        fontWeight: 'bold',
    },
    text6:{
        marginTop:5,
        textAlign:'center',
        fontSize:responsiveFontSize(1.8),
        color:'#2c2c2c',
        marginBottom:15,
    },
    text7:{
        textAlign:'center',
        fontSize:responsiveFontSize(1.8),
        color:'#2c2c2c',
    },
    image1:{
        width:responsiveWidth(100),
        height:responsiveHeight(40),
        resizeMode:'contain',
        
    },
    image2:{
        height:18,
        width:18,
        resizeMode:'contain',
    },
    imageView:{
        width:25,
        marginRight:responsiveHeight(1),
    },
    imageView1:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    buttonView:{
        marginBottom:5,
        justifyContent:'center',
        alignItems:'center',
    },
    buttonTrack: {
        width: responsiveWidth(75),
        height:responsiveHeight(6.5),
        backgroundColor: '#333333'
    },
    buttonTextTrack: {
        flex:1,
        color: 'white',
        fontSize:responsiveFontSize(1.6),
        fontWeight: 'bold',
        textAlign:'center'
    },
    footerBackGround:{
        backgroundColor:'#201e1f',
        height:responsiveHeight(6.5)
    },
    footerButton:{
        flexDirection:'row',
        width: width,
        alignItems: 'center',
    },
    footerButtonView:{
        flex:1 ,
        alignItems:'center',
        marginRight:-10,
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontSize:responsiveFontSize(1.6)
    },
})