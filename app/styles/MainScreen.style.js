import { StyleSheet, Dimensions, StatusBar } from 'react-native'

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	container: {
		flex:1,
		backgroundColor: '#ffffff',
		width:width,
		height:height,
	},
	actionbarContainer:{
		paddingLeft:8,
	},
	actionbarTitleContainer:{
		marginRight:32,
		alignItems:'center',
	},
	homeLoginCart:{
		justifyContent:'center',
		height:responsiveHeight(9),
		width:responsiveWidth(50),
		margin:responsiveWidth(0.5),
	},
	homeLoginView:{
		flexDirection:'row',
	},
	homeLoginImg:{
		width:50,
		height:50,
	},
	homeLoginText1:{
		paddingLeft:10,
		color:'#201e1f',
		width:responsiveWidth(48),
		fontSize:responsiveFontSize(2),
		fontWeight:'bold',
	},
	homeLoginText2:{
		paddingLeft:10,
		color:'#201e1f',
		marginTop:8,
		marginBottom:8,
		width:responsiveWidth(40),
		fontSize:responsiveFontSize(1.6),
	},
	view1:{
		flex:1,
		backgroundColor:'#201e1f',
	},
	view2:{
		borderTopWidth:0.5,
		borderColor:'#ffffff',
	},
	container2: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	container3: {
		marginLeft:10,
		marginRight:10,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	button: {
		marginBottom: 30,
		width: 120,
		marginLeft:10,
		marginRight:10,
		alignItems: 'center',
		backgroundColor: '#2196F3'
	},
	buttonText: {
		padding: 10,
		color: 'white'
	},
	banner1:{
		width: responsiveWidth(100),
		height: responsiveHeight(35),
	// justifyContent:'center',
	},
	banner2:{
		margin:responsiveWidth(0.5),
		width: responsiveWidth(50),
		height: responsiveHeight(40),
		// aspectRatio:0.75,
		// alignItems:'center'
	},
	banner3:{
		margin:responsiveWidth(0.5),
		width: responsiveWidth(49),
		height: responsiveHeight(24.5),
		// aspectRatio:1,
		// resizeMode:'stretch',
		
		// alignItems:'center',
	},
	// banner4 :{
	// 	margin:responsiveWidth(0.5),
	// 	width:responsiveWidth(49),
	// 	height: responsiveHeight(24.5),
	// 	// aspectRatio:1,
	// 	// alignItems:'center'
	// },
	banner5:{
		width: responsiveWidth(48),
		height: responsiveHeight(40),
		// alignItems:'center'
	},
	banner6:{
		width: responsiveWidth(48),
		height: responsiveHeight(30),
		// alignItems:'center'
	},
	// banner7 :{
	// 	width:responsiveWidth(46),
	// 	height: responsiveHeight(36),
	// 	// alignItems:'center',
	// },
	drawerBg:{
		backgroundColor:'#201e1f',
	},
	drawerFont:{
		color:'white',
		fontSize:responsiveFontSize(1.6),
	},
	drawerChidFont:{
		color:'white',
		fontSize:responsiveFontSize(1.4),
	}
})