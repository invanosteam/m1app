import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        width:width,
        height:height,
        flex:1,
        backgroundColor: '#ffffff',
    },
    scrollView1:{
        marginTop:10,
        height:250,
    },
    footerIcon:{
        height:responsiveHeight(2.5),
        width:responsiveHeight(2.5),
        resizeMode:'contain',
    },
    view1:{
        flexDirection:'row',
        width: width,
        marginTop:10,
        alignItems: 'center',
    },
    view2:{
        flex:1,
    },
    view3:{
        marginRight:responsiveWidth(2.5),
    },
    view4:{
        justifyContent: 'flex-end',
        alignItems:'flex-end',
    },
    view5:{
        marginLeft:10,
        marginTop:10,
    },
    view6:{
        flexDirection:'row',
    },
    view7:{
        flexDirection:'row',
        marginBottom:15,
        width:responsiveWidth(93),
    },
    view8:{
        flexDirection:'row',
        width:width,
        alignItems:'center',
    },
    view9:{
        flexDirection:'row',
        width: width,
        marginTop:10,
        alignItems: 'center',
    },
    view10:{
        padding:10,
    },
    view11:{
        justifyContent: 'flex-end',
        alignItems:'flex-end',
        marginRight:25,
    },
    view12:{
        flexDirection:'row',
        width: width,
        marginTop:10,
        marginBottom:10,
        alignItems: 'center',
    },
    view13:{
        flex:1,
        alignItems:'center',
        marginRight:-responsiveWidth(2.5),
    },
    view14:{
        width:width,
        flexDirection:'row',
        // borderBottomColor:'#201e1f',
        // borderBottomWidth:.5,
    },
    view15:{
        // flexDirection:'row',
        marginTop:5,
        marginBottom:5,
    },
    text1:{
        marginLeft:10,
        fontSize:responsiveFontSize(1.6),
        fontWeight: 'bold',
    },
    text2:{
        fontSize:responsiveFontSize(1.4),
        color:'#201e1f',
        fontWeight: 'bold',  
    },
    text3:{
        fontSize:responsiveFontSize(1.6),
    },
    text4:{
        fontSize:responsiveFontSize(1.6),
        marginLeft:2,
    },
    text5:{
        fontSize:responsiveFontSize(1.6),
        color:'#efac27',
    },
    text6:{
        fontSize:responsiveFontSize(2),
        color:'#1b1b1b',
        fontWeight:'bold',
    },
    text7:{
        fontSize:responsiveFontSize(2.2),
        color:'#efac27',
        fontWeight: 'bold',
    },
    text8:{
        marginLeft:10,
        fontSize:responsiveFontSize(1.4),
        // width:responsiveWidth(80),
        // backgroundColor:'green',
    },
    text9:{
        marginLeft:10,
        fontSize:responsiveFontSize(1.4),
    },
    productImg:{
        marginLeft:10,
        width: responsiveWidth(20),
        height: responsiveHeight(15),
        resizeMode: 'contain',
    },
    productNameandPriceView:{
        width:width/2.2,
        marginTop:10,
        marginLeft:10
    },
    priceText:{
        color:'#201e1f',
        fontSize:responsiveFontSize(1.8),
        fontWeight: 'bold',
    },
    productName:{
        width:width/2,
        fontSize:responsiveFontSize(1.6),
        fontWeight: 'bold',
    },
    productContentView:{
        width:width/2.1,
        flexDirection: 'row',
        marginLeft:5,
        marginRight:5,
    },
    footerBackGround:{
        backgroundColor:'#201e1f',
        height:responsiveHeight(6.5),
    },
    footerButton:{
        flexDirection:'row',
        width: width,
        alignItems: 'center',
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontSize:responsiveFontSize(1.6),
    },
})