import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        width:width,
        height:height,
    },
    mainView:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    view1:{
        marginTop:10,
    },
    label:{
        marginBottom:10,
        fontSize: responsiveFontSize(2),
        color:'black',
        justifyContent: 'center',
    },
    labelOffers:{
        marginBottom:10,
        fontSize: responsiveFontSize(1.8),
        justifyContent: 'center',
    },
    button: {
        marginTop:15,
        marginBottom: 10,
        width: responsiveWidth(50),
        height:responsiveHeight(6.5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#201e1f'
    },
    buttonText: {
        color: 'white',
        fontSize:responsiveFontSize(1.8),
    },
    footer:{
        height:responsiveHeight(8.2),
    },
    image:{
        width:responsiveWidth(35),
        height:responsiveWidth(35),
        resizeMode: 'contain',
        marginLeft:50,
        marginRight:50,
        marginBottom: responsiveHeight(2),
    },
})