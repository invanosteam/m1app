import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#ffffff',
        width:width,
        height:height,
    },
    view1:{
        borderBottomWidth:.2,
        justifyContent:'center',
    },
    view2:{
        flexDirection:'row',
    },
    view3:{
        marginLeft:5,
        flexDirection:'column',
        justifyContent: 'center',
    },
    view4:{
        marginLeft:10,
        // backgroundColor:'red'
    },
    view5:{
        flexDirection:'row',
        marginTop:responsiveHeight(1.5),
        // backgroundColor:'green',
    },
    image:{
        height:responsiveHeight(15),
        width:responsiveHeight(15),
        resizeMode:'contain',
    },
    text1:{
        marginTop:15,
        fontSize:responsiveFontSize(1.8),
        width:responsiveWidth(65),
        color:'#000000',
    },
    text2:{
        fontSize:responsiveFontSize(1.8),
        color:'#201e1f',
        fontWeight:'bold',
    },
    text3:{
        marginTop:2,
        marginLeft:10,
        fontSize:responsiveFontSize(1.6),
        color:'#7a7a7a',
        textDecorationLine:'line-through',
    },
    footer:{
        height:responsiveHeight(8.2),
    },
    button: {
        marginTop:10,
        marginBottom: 10,
        width: responsiveWidth(40),
        height:responsiveHeight(6),
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor:'#efac27'
    },
    buttonText: {
        // padding: 10,
        color: '#efac27',
        fontWeight:'bold',
        fontSize:responsiveFontSize(1.6)
    },
})