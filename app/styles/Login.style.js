import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	container: {
        backgroundColor: '#ffffff',
        width:width,
        height:height,
    },
    AppIcon:{
        marginBottom:20,
        width:responsiveWidth(60),
        height:responsiveHeight(20),
        resizeMode:'contain',
    },
    titleView:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:responsiveWidth(20),
        marginTop:responsiveHeight(3),
    },
    title:{
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize:responsiveFontSize(3),
        color:'#282828'
    },
    skipView:{
        flex:1,
        flexDirection:'row',
    },
    skiplabel:{
        flex:1,
        alignItems: 'flex-end',
        margin:responsiveHeight(3),
        marginTop:responsiveHeight(1),
        // marginRight:25
    },
    passwordEyeIcon:{
        width:18,
        height:18,
        marginTop:25,
        marginLeft:-25,
        tintColor:'#efac27',
    },
    socialLabelView:{
        flex:1,
        marginLeft:20,
        justifyContent: 'flex-start',
        alignItems:'flex-start',
    },
    socialLabel:{
        textAlign:'left',
        color:'#a2a2a2',
        width:responsiveWidth(50),
        fontSize:responsiveFontSize(1.8),
    },
    socialIconView:{
        marginTop:responsiveHeight(2),
        marginBottom:responsiveHeight(2),
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'flex-end'
    },
    socialIcon:{
        height:responsiveHeight(3.5),
        width:responsiveHeight(3.5),
        resizeMode:'contain',
    },
    userTextArea:{
        borderBottomColor:'#201e1f',
        width:responsiveWidth(70),
        height:responsiveHeight(8),
    },
    inputIcon:{
        marginLeft:10,
        marginRight:5,
        marginTop:35,
        height:responsiveHeight(4),
        width:responsiveHeight(4),
        tintColor:'#efac27',
    },
    inputIcon1:{
        marginLeft:4,
        marginRight:5,
        marginTop:35,
        height:responsiveHeight(4),
        width:responsiveHeight(4),
        tintColor:'#efac27',
    },
    inputView:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    forgotView:{
        marginTop:10,
        alignItems: 'flex-end',
        marginRight:15,
    },
    label:{
        color:'#a2a2a2',
        fontSize:responsiveFontSize(1.4),
    },
   
    inputLabel:{
        color:'#201e1f',
        fontSize:responsiveFontSize(1.6),
        marginLeft:2,
    },
    buttonView:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
        marginTop:15,
    },
})