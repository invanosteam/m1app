import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex:1,
        // height:height,
        // width:width,
        backgroundColor: '#ffffff',
    },
    actionBarContainer:{
        paddingLeft:8,
        paddingRight:8,
    },
    titleContainerStyle:{
        alignItems:'center',
    },
    view1:{
        width:width,
        marginTop:10,
        paddingLeft:10,
        paddingRight:10,
    },
    view2:{
        paddingLeft:10,
        paddingRight:10,
        flexDirection:'row',
        justifyContent:'space-between',
    },
    view3:{
        paddingLeft:10,
        paddingRight:10,
        flexDirection:'row',
        justifyContent:'space-between',
    },
    view4:{
        marginTop:20,
        width:width,
        height:responsiveHeight(15),
        marginBottom:20,
        paddingLeft:10,
        paddingRight:10,
        // backgroundColor:'green',
    },
    view5:{
        paddingLeft:10,
        paddingRight:10,
    },
    text1:{
        color:'#201e1f',
        fontSize:responsiveFontSize(1.6),
        fontWeight:'bold',
        marginBottom:10,
    },
    text2:{
        color:'#efac27',
        fontSize:responsiveFontSize(1.6),
        fontWeight:'bold',
    },
    text3:{
        fontSize:responsiveFontSize(1.6),
    },
    radioButtonContainer:{
        borderWidth: 1,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        margin: responsiveHeight(2),
        width: responsiveWidth(18),
        height:responsiveHeight(5.5),
    },
    radioButtonText:{
        fontSize:responsiveFontSize(1.6),
    },
    checkbox:{
        width: responsiveHeight(4),
        height: responsiveHeight(4),
        marginRight: 5,
    },
    checkboxText:{
        fontSize:responsiveFontSize(1.6),
    },
    footer:{
        backgroundColor:'#201e1f',
        height:responsiveHeight(6.5)
    },
    footerbutton: {
        // width: width,
        // height:60,
        backgroundColor:'#201e1f',
        alignItems: 'center',
        justifyContent:'center',
    },
    buttonText: {
        // marginTop:18,
        textAlign: 'center',
        color: 'white',
        fontSize:responsiveFontSize(1.6),
    },
})