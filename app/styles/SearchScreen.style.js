import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: '#ffffff'
    },
    containerStyle:{
        paddingLeft:8,
    },
    footer:{
        height:responsiveHeight(8.2),
    },
    // textStyle:{
    //     fontSize:responsiveFontSize(2),
    // },
    text1:{
        fontSize:responsiveFontSize(2),
        width:responsiveWidth(90)
    },
    titleContainerStyle:{
        marginRight:32,
        alignItems:'center',
    },
    view1:{
        flex:1,
        paddingLeft:10,
        paddingRight:10,
        height:60,
        justifyContent:'center',
        borderBottomWidth:.2,
    },
})