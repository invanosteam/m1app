import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default StyleSheet.create({
	overlay: {
		position: 'absolute',
		top: 0,
		right: 0,
		bottom: 0,
		left: 0,
		opacity: 0.4,
		backgroundColor: '#000'
	},
    buttonColor:{
        backgroundColor: '#201e1f'
    },
    dateText:{
        textAlign:'center',
        fontWeight:'bold',
        width:responsiveWidth(15),
        fontSize:responsiveFontSize(2.4),
        color:'#ffffff',
    },
	fontStyle:{
		fontFamily:'opensans'
	},
	buttonView:{
        justifyContent: 'center',
        flex:1,
        alignItems:'center',
        marginTop:responsiveHeight(2),
    },
    button:{
        marginTop:responsiveHeight(2),
        marginBottom: responsiveHeight(2),
        width: responsiveWidth(45),
        height:responsiveHeight(6.5),
        borderRadius:15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0088ba'
    },
    buttonText: {
        color: 'white',
        fontSize:16,
        fontWeight: 'bold',
    },
    appTagLine:{
        fontFamily:'opensans',
        fontWeight:'bold',
        color:'#000000',
    },
    appTagLine1:{
        fontFamily:'opensans',
        fontWeight:'bold',
        color:'#0088ba',
    },
})