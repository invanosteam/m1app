import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        width:responsiveWidth(100),
        height:responsiveHeight(100),
    },
    img:{
        width:responsiveWidth(100),
        height:responsiveHeight(100),
    }

});