import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	container: {
		flex:1,
		backgroundColor: '#ffffff',
		margin:2,
	},
	actionbarContainer:{
		paddingLeft:8,
		paddingRight:8,
	},
	actionbarTitleContainer:{
		alignItems:'center',
	},
	iconSize:{
		height:22,
		width:22
	},
	shoppingBag:{
		height:22,
		width:22,
		resizeMode: 'contain',
	},
	button: {
		marginBottom: 30,
		width: 100,
		marginLeft:25,
		marginRight:25,
		alignItems: 'center',
		backgroundColor: '#2196F3'
	},
	imgView:{
		borderBottomWidth:1,
		borderColor:'#ededed',
	},
	imgSize:{
		width: responsiveWidth(50),
		height: responsiveHeight(40),
		// height:230,
		resizeMode: 'contain',
	},
	nameView:{
		marginLeft:12,
		marginTop:5,
		// height:40,
	},
	nameText: {
		color:'#979797',
		fontSize:responsiveFontSize(1.8),
		width:responsiveWidth(48),
		// backgroundColor:'green',
	},
	priceView:{
		flex:1,  
		flexDirection:'row',
		marginBottom:5,
		marginTop:10,
		width:responsiveWidth(50),
	},
	finalPrice:{
		color:'#201e1f',
		fontSize:responsiveFontSize(2),
		fontWeight: 'bold',
		marginLeft:12,
	},
	priceText:{
		// fontWeight: 'bold',
		marginTop:5,
		color:'#201e1f',
		marginLeft:12,
		fontSize:responsiveFontSize(1.6),
		textDecorationLine:'line-through',
	},

	checkBoxView: {
		flex:1,
		alignItems:'flex-end',
		marginRight:20,
		// backgroundColor:'green',
	},
})