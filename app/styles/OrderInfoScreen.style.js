import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        width:width,
        height:height,
    },
    containerStyle:{
        paddingLeft:8,
    },
    titleContainerStyle:{
        marginRight:32,
        alignItems:'center',
    },
    view1:{
        flexDirection:'row',
        flex:1,
        margin:15,
    },
    view2:{
        justifyContent:'center',
    },
    view3:{
        flexDirection:'row',
    },
    view4:{
        justifyContent:'center',
    },
    view5:{
        flex:1,
    },
    view6:{
        flexDirection:'row',
        flex:1,
    },
    text1:{
        fontSize:responsiveFontSize(2),
        color:'#474747',
    },
    text2:{
        color:'#201e1f',
        fontWeight:'bold',
        fontSize:responsiveFontSize(2),
        marginTop:8,
    },
    text3:{
        fontSize:responsiveFontSize(2.2),
        fontWeight:'bold',
        color:'#474747',
        marginBottom:10,
        marginTop:10,
        paddingLeft:10,
    },
    text4:{
        fontSize:responsiveFontSize(2),
        color:'#201e1f',
        textAlign:'right',
    },
    text5:{
        fontSize:responsiveFontSize(2),
        fontWeight:'bold',
        color:'#474747',
    },
    text6:{
        fontSize:responsiveFontSize(2),
        color:'#201e1f',
        fontWeight:'bold',
        textAlign:'right',
    },
    imgSize:{
        width: responsiveWidth(25),
        height: responsiveHeight(25),
        resizeMode: 'contain',
    },
    titleText:{
        fontSize:responsiveFontSize(2),
        color:'#474747',
    },
})