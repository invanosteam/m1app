import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        padding:10,
    },
    view1:{
        marginTop:10,
    },
    view2:{
        backgroundColor:'#f1f1f1',
    },
    view3:{
        width:25,
        marginRight:10,
    },
    view4:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    view5:{
        marginLeft:10,
    },
    image:{
        height:responsiveHeight(2.5),
        width:responsiveHeight(2.5),
        resizeMode: 'contain',
    },
    text1:{
        marginLeft:10,
    },
    picker:{
        paddingLeft:5,
        paddingRight:5,
        width:responsiveWidth(60),
    },
    deliveryView:{
        width:width,
        justifyContent:'center',
        alignItems:'center',
        marginTop:15,
        marginBottom:15,
    },
    deliveryText:{
        marginTop:15,
        marginLeft:25,
        color:'#282828',
        fontSize:16,
    },
    footerBackGround:{
        backgroundColor:'#201e1f',
        height:responsiveHeight(6.5),
        
    },
    footerButton: {
        flexDirection:'row',
        width: width,
        alignItems: 'center',
    },
    buttonTextView:{
        flex:1 ,
        alignItems:'center',
        marginRight:-10
    },
    buttonView:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    button:{
        marginTop:15,
        marginBottom: 10,
        width: responsiveWidth(60),
        height:responsiveHeight(6.5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#201e1f'
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontSize:responsiveFontSize(1.6),
    },

    mainView:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#ffffff'
    },
    label:{
        marginBottom:10,
        fontSize: responsiveFontSize(2),
        justifyContent: 'center',
    },
})