import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	 container: {
        backgroundColor: '#ffffff',
        width:width,
        height:height,
    },
    inputView:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    textArea:{
        borderBottomWidth:.5,
        borderBottomColor:'#201e1f',
        width:responsiveWidth(70),
        height:responsiveHeight(8),
        // marginRight:50,
    },
    inputIcon:{
        marginLeft:10,
        marginRight:5,
        marginTop:35,
        height:responsiveHeight(3.5),
        width:responsiveHeight(3.5),
        tintColor:'#efac27',
    },
    socialLabelView:{
        marginLeft:20,
        // marginBottom:20,
        justifyContent: 'flex-start',
        alignItems:'flex-start',
        flex:1,
    },
    socialLabel:{
        textAlign:'left',
        color:'#a2a2a2',
        width:responsiveWidth(50),
        fontSize:responsiveFontSize(1.8),
    },
    socialIconView:{
        marginTop:responsiveHeight(2),
        marginBottom:responsiveHeight(2),
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'flex-end'
    },
    socialIcon:{
        height:responsiveHeight(3.5),
        width:responsiveHeight(3.5),
        resizeMode:'contain',
    },
    label:{
        textAlign: 'center',
        color:'#a2a2a2'
    },
    genderView:{
        flex:1,
        flexDirection:'row',
        marginTop :20,
        marginBottom:10
    },
    gender:{
        // color:'#a2a2a2',
        marginRight:10,
        marginTop:6,
        fontSize:responsiveFontSize(1.4),
    },
    genderText:{
      fontSize:responsiveFontSize(1.4),  
    },
    title:{
        marginTop:responsiveHeight(3),
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize:responsiveFontSize(3),
        color:'#282828',
    },
    inputLabel:{
        color:'#201e1f',
        fontSize:responsiveFontSize(1.6),
        marginLeft:2,
    },
    buttonView:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
        marginTop:responsiveHeight(3),
    },
})