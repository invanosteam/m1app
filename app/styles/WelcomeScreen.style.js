import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default StyleSheet.create({
	container: {
        backgroundColor: '#ffffff',
        width:responsiveWidth(100),
        height:responsiveHeight(100),
    },
    img:{
        width:responsiveWidth(100),
        height:responsiveHeight(65),
    },
    dotColor:{
        backgroundColor:'#cccccc',
    },
    activeDotColor:{
        backgroundColor:'#ffffff',
    },
    view1:{
        flex:1,
        width:responsiveWidth(100),
    },
    view2:{ 
        width: 8, 
        height: 8, 
        borderRadius: 7, 
        marginLeft: 5, 
        marginRight: 5,
        marginTop:0,
    },
    view4:{
        justifyContent:'center',
        alignItems:'center',
        flex:1,
    },
    view5:{
        paddingTop:15 ,
        paddingBottom:15,
    },
    view6:{
        flex:1,
        justifyContent:'center',
    },
    footerbutton: {
        width: responsiveWidth(100),
        height:responsiveHeight(6.5),
        justifyContent:'center',
        alignItems: 'center',
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontSize:responsiveFontSize(1.6),
    },
    descriptionLabelView:{
        paddingTop:5,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    borderBottom:{
        flexDirection: 'row',
        borderBottomWidth: 4,
        borderBottomColor: '#efac27'
    },
    descriptionLabel:{
        marginRight:-5,
        padding:6,
        fontSize:responsiveFontSize(2.5),
        color:'#282828'
    },
    boldDescriptionLabel:{
        marginLeft:-5,
        padding:6,
        fontWeight: 'bold',
        fontSize:responsiveFontSize(2.5),
        color:'#282828'
    },
    descriptionData:{
        fontSize:responsiveFontSize(1.8),
        marginLeft:5,
        marginRight:5,
        textAlign: 'center',
        color:'#7a7977'
    },
    footerBackGround:{
        backgroundColor:'#201e1f',
        height:responsiveHeight(6.5),
    },
});