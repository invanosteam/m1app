import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#ffffff',
        width:width,
        height:height,
    },
    footer:{
        height:responsiveHeight(8.2),
    },
    view1:{
        alignItems:'center' ,
        marginTop:25,
        marginBottom:20,

    },
    view2:{
        justifyContent:'flex-end',
        flexDirection:'row',
        flex:2,
        borderBottomWidth:.5,
        paddingRight:15,
    },
    view3:{
        borderBottomWidth:.5,
        justifyContent:'center',
    },
    view4:{
        flexDirection:'row',
        flex:1,
        margin:15,
    },
    view5:{
        marginLeft:5,
        flexDirection:'column',
        justifyContent: 'center',
    },
    view6:{
        flexDirection:'row',
        marginLeft:20,
    },
    view7:{
        marginLeft:5,
    },
    textInput1:{
        fontSize:responsiveFontSize(2),
        width:responsiveWidth(40),
    },
    text1:{
        fontSize:responsiveFontSize(1.8),
        color:'#201e1f',
        fontWeight:'bold',
        marginBottom:5,
    },
    imgSize:{
        height:26,
        width:26,
        resizeMode: 'contain',
        tintColor:'#efac27',
    },
    passTextArea:{
        borderColor:'transparent',
        width:responsiveWidth(80),
        marginLeft:15
    },
    customerInfoText:{
        fontSize:responsiveFontSize(2),
        width:responsiveWidth(80),
        marginLeft:20,
    },
    changePassText:{
        fontSize:responsiveFontSize(2),
        width:responsiveWidth(80),
        marginLeft:20,  
        color:'#c5c5c5',
    },
    inputLabel:{
        fontSize:responsiveFontSize(1.8),
    },
    thumbnailImage:{
        height:responsiveHeight(15),
        width:responsiveHeight(15),
    },
    
})