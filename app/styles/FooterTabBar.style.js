import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	iconSize:{
		height:responsiveHeight(3),
		width:responsiveHeight(3),
		resizeMode: 'contain',
		padding:5,
	},
	shoppingBag:{
		height:responsiveHeight(3),
		width:responsiveHeight(3),
		resizeMode: 'contain',
	},
	iconColor:{
		tintColor:'#efac27',
	}
})