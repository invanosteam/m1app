import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex:1,
        width:width,
        height:height,
        backgroundColor:'#ffffff',
    },
    footerBackGround:{
        backgroundColor:'#201e1f',
        height:responsiveHeight(6.5),
    },
    footerButton: {
        flexDirection:'row',
        alignItems: 'center',
        width: width,
    },
    promoBg:{
        backgroundColor:'#f1f1f1'
    },
    promoTitle:{
        fontWeight:'bold',
        marginTop:20,
        marginLeft:15,
        fontSize:responsiveFontSize(2),
    },
    promoInputView:{
        flex:1,
    },
    promoInput:{
        backgroundColor:'#ffffff',
        borderWidth:0.5,
        marginLeft:15,
        paddingLeft:15,
        width:responsiveWidth(70),
        height:responsiveHeight(6),
        fontSize:responsiveFontSize(1.6),
    },
    promoButtonView:{
        justifyContent:'center',
        flexDirection:'row',
        height:responsiveHeight(10),
        marginTop:20,
        marginBottom:20
    },
    promoButton:{
        marginRight:15,
        width:responsiveWidth(20),
        height:responsiveHeight(6),
        backgroundColor:'#201e1f',
        justifyContent:'center',
    },
    promoButtonText:{
        color:'white',
        fontSize:responsiveFontSize(1.4),
    },
    titleView:{
        flexDirection:'row',
        justifyContent:'center',
        paddingLeft:10,
        paddingTop:20,
        paddingBottom:20,
        borderBottomWidth:0.5,
    },
    titleText:{
        fontSize:responsiveFontSize(2),
        color:'#323232',
        alignSelf: "center",
    },
    radioView:{
        justifyContent:'center',
        alignItems:'flex-end',
        marginRight:15,
        flex:1,
    },
    totalView:{
        justifyContent:'space-between',
        flexDirection:'row',
        marginBottom:20,
        marginLeft:10,
        marginRight:10,
    },
    totalAmountText:{
        fontSize:responsiveFontSize(2),
        color:'#1b1b1b',
        fontWeight:'bold',
    },
    totalPriceText:{
        fontSize:responsiveFontSize(2.2),
        color:'#efac27',
        fontWeight:'bold',
    },
    discountTotalView:{
        justifyContent:'space-between',
        flexDirection:'row',
        marginBottom:10,
        marginLeft:10,
        marginRight:10,
    },
    discountTotalAmountText:{
        fontSize:responsiveFontSize(1.8),
        color:'#1b1b1b',
    },
    discountTotalPriceText:{
        fontSize:responsiveFontSize(2),
        color:'#efac27',
        fontWeight:'bold',
    },
    buttonTextView:{
        flex:1 ,
        alignItems:'center',
        marginRight:-responsiveWidth(2.5),
    },
    buttonText:{
        textAlign: 'center',
        color: 'white',
        fontSize:responsiveFontSize(1.6),
    },
    buttonImgView:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    buttonImg:{
        resizeMode:'contain',
        height:responsiveHeight(2.5),
        width:responsiveHeight(2.5),
    },
    buttonImgViewContainer:{
        // width:25,
        marginRight:responsiveWidth(2.5),
    },
})