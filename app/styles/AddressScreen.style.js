import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex:1,
        width:width,
        height:height,
        backgroundColor: '#ffffff',
    },
    view1:{
        paddingTop:responsiveHeight(2),
        justifyContent:'center',
        alignItems:'center',
    },
    view2:{
        justifyContent:'center',
        alignItems:'center',
    },
    view3:{
        marginTop:responsiveHeight(2),
    },
    view4:{
        marginRight:responsiveWidth(2),
        // width:25,
        // backgroundColor:'green'
    },
    view5:{
        justifyContent: 'flex-end',
        alignItems:'flex-end',
    },
    image:{
        height:responsiveHeight(2.5),
        width:responsiveHeight(2.5),
        resizeMode: 'contain',
    },
    titleText:{ 
        padding:5,
        fontSize:responsiveFontSize(2.2),
        color:'#efac27',
        fontWeight:'bold',
        borderBottomWidth:.2,
    },
    item1:{
        marginTop:responsiveHeight(1),
        width:responsiveWidth(50),
        height:responsiveHeight(8),
        // backgroundColor:'red',
    },
    item2:{
        marginTop:responsiveHeight(1),
        width:responsiveWidth(40),
        height:responsiveHeight(8),
        // backgroundColor:'red',
    },
    item3:{
        marginTop:responsiveHeight(1),
        width:responsiveWidth(65),
        height:responsiveHeight(8),
    },
    item5:{
        marginTop:responsiveHeight(1),
        width:responsiveWidth(90),
        height:responsiveHeight(8),
    },
    item6:{
        marginTop:15,
        width:responsiveWidth(45),
        height:responsiveHeight(8),
    },
    subContainer:{
        padding:responsiveHeight(2.5),
    },
    rowView:{
        flexDirection: 'row',
    },
    picker:{
        width:responsiveWidth(46),
    },
    checkBox:{
        marginLeft:5,
        marginTop:25,
        marginBottom:20,
    },
    deliveryView:{
        width:width,
        justifyContent:'center',
        alignItems:'center',
        marginTop:15,
        marginBottom:15,
    },
    deliveryText:{
        marginTop:15,
        marginLeft:25,
        color:'#282828',
        fontSize:responsiveFontSize(1.8),
    },
    footerButton:{
        width: width,
        height:responsiveHeight(6.5),
        flexDirection:'row',
        alignItems: 'center',
    },
    footerBackGround:{
        backgroundColor:'#201e1f',
        height:responsiveHeight(6.5),
    },
    buttonTextView:{
        flex:1 ,
        alignItems:'center',
        marginRight:-responsiveWidth(2.5),
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontSize:responsiveFontSize(1.6)
    },
    dataInput:{
        fontSize:responsiveFontSize(1.6),
    },
    radioButtonContainer: {
        borderWidth: 1,
        borderRadius: responsiveFontSize(2.5),
        alignItems: 'center',
        justifyContent: 'center',
        margin: responsiveHeight(2.5),
        width: responsiveWidth(25),
        height:responsiveHeight(12.5),
    },
    radioButtonText:{
        fontSize: responsiveFontSize(2.5),
    }
})