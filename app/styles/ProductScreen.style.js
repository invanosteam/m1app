import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex:1,
        height:height,
        width:width,
    	backgroundColor: '#ffffff',
    },
    view1:{
        borderColor:'#000000',
        borderWidth:0.5,
        margin:5,
    },
    smallImage:{
        height:responsiveHeight(15),
        width:responsiveHeight(15),
        resizeMode:'contain',
    },
    actionbarContainer:{
        paddingLeft:8,
    },
    actionbarTitleContainer:{
        marginLeft:32,
        alignItems:'center',
    },
    checkBox:{
        marginLeft:15,
        marginTop:25
    },
    imgAlign:{
        borderBottomColor:'#000000',
        borderBottomWidth:0.5,
        paddingBottom:10,
    },
    img:{
        width: responsiveWidth(100),
        height: responsiveHeight(50),
        resizeMode:'contain',
    },

    productTitle:{
    	 textAlign: 'center',
    },
    footer:{
        backgroundColor:'#201e1f',
        height:responsiveHeight(6.5)
    },
    picker:{
    	flex: 1,
    	marginRight:5,
    	color:'white',
        width:responsiveWidth(30),
    },
    qtyText:{
    	marginLeft:responsiveWidth(2),
        color: 'white',
        fontSize:responsiveFontSize(1.4),
    },
    qtybtn:{
    	width: responsiveWidth(35),
        height:responsiveHeight(6.5),
        flexDirection: 'row',
        backgroundColor:'#201e1f',
        alignItems: 'center',
    },
    footerbutton: {
        width: responsiveWidth(65),
        height:responsiveHeight(6.5),
        backgroundColor:'#201e1f',
        alignItems: 'center',
        justifyContent:'center',
        borderLeftColor:'#ffffff',
        borderWidth:1,
    },
    view6:{
        justifyContent:'center',
        alignItems: 'center',
        width: responsiveWidth(6.5),
        borderLeftColor:'#ffffff',
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontSize:responsiveFontSize(1.6),
    },
    contentAlign:{
        // alignItems: 'center',
        marginLeft:15,
    },
    nameText:{
    	color:'#2a2a2a',
    	marginTop:15,
    	fontSize:responsiveFontSize(2.2),
        width:responsiveWidth(93),
    },
    priceView:{
        marginTop:10,
        flex:1,
        marginLeft:15,
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent:'center',
    },
    priceText:{
    	color:'#201e1f',
        fontWeight:'bold',
        fontSize:responsiveFontSize(2.3), 
    },
    regularPrice:{
        marginLeft:10,
        fontSize:responsiveFontSize(2.2),
        marginTop:5,
        textDecorationLine:'line-through',
    },
    descriptionTitle:{
    	color:'#373737',
    	fontSize:responsiveFontSize(2.2),
    	marginLeft:15,
    	marginTop:10,
    	marginBottom:10,
    },
    descriptionText:{
    	color:'#6a6a6a',
    	fontSize:responsiveFontSize(1.8),
    	marginLeft:15,
        width:responsiveWidth(93),
    	marginBottom:10,
        // textAlign: 'center',
    },
    colorOptions:{
    	marginTop:5,
        flexDirection: 'row',
    },
    containerRelated: {
        flex:1,
        backgroundColor: '#ffffff',
        margin:5,
    },
    imgView:{
        borderBottomWidth:1,
        borderColor:'#ededed',
    },
    imgSize:{
        width:responsiveWidth(50),
        height: responsiveHeight(40),
        resizeMode: 'contain',
        // width: width/2,
        // height: 230,
    },
    nameViewRelated:{
        marginLeft:12,
        marginTop:5,
        height:40,
    },
    nameTextRelated: {
        color:'#979797',
        fontSize:responsiveFontSize(1.8),
        width:responsiveWidth(48),
    },
    priceViewRelated:{
        flex:1,  
        flexDirection:'row',
        marginBottom:5,
        marginTop:5,
    },
    priceTextRelated:{
        color:'#201e1f',
        fontSize:responsiveFontSize(1.8),
        fontWeight: 'bold',
        marginLeft:12,
        width:responsiveWidth(40),
    },
    checkBoxView: {
        flex:1,
        alignItems:'flex-end',
        marginRight:20,
    },
    radioButtonContainer:{
        borderWidth: 1,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        margin: responsiveHeight(2),
        width: responsiveWidth(18),
        height:responsiveHeight(6),
    },
    radioButtonText:{
        fontSize:responsiveFontSize(1.6),
    },

    textArea:{
        marginTop:responsiveHeight(1),
        height:responsiveHeight(8),
        width:responsiveWidth(93),
        borderBottomWidth:0.5,
    },
    inputLabel:{
        fontSize:responsiveFontSize(1.6),
    }
})
