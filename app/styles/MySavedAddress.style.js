import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        width:width,
        height:height,
    },
    view1:{
        borderBottomWidth:.2,
        justifyContent:'center',
    },
    view2:{ 
        flex:1,
        flexDirection:'row',
        margin:15,
    },
    view3:{
        flex:1,
    },
    view4:{
        flex:1,
        flexDirection:'column',
        marginTop:5,
    },
    view5:{
        marginLeft:5,
        flexDirection:'column',
    },
    text1:{
        fontSize:responsiveFontSize(1.8),
        color:'#959595',
    },
    text2:{
        fontSize:responsiveFontSize(2),
        fontWeight:'bold',
        color:'#474747',
    },
    text3:{
        fontSize:responsiveFontSize(1.8),
        color:'#201e1f',
    },
    footer:{
        height:responsiveHeight(8.2),
    },
})