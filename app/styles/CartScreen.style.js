import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#ffffff',
        // width: width,
        // height: 100
    },
    actionbarContainer:{
        paddingLeft:8,
        // paddingRight:8,
    },
    actionbarTitleContainer:{
        marginRight:32, 
        alignItems:'center',
    },
    totalView:{
        paddingLeft:20,
        paddingRight:20,
        marginTop:10,
    },
    totalView1:{
        flexDirection:'row',
    },
    totalView2:{
        flex:1,
    },
    totalText:{
        fontSize:responsiveFontSize(2),
        color:'#201e1f',
        fontWeight:'bold',
        textAlign:'right',
    },
    productImg:{
        marginLeft:5,
        width: responsiveWidth(20),
        height: responsiveHeight(15),
        resizeMode: 'contain',
    },
    customDataView:{
        flexDirection:'row',
        marginTop:5,
        marginBottom:5,
    },
    customDataText:{
        marginLeft:10,
        fontSize:responsiveFontSize(1.8),
    },
    productImgView:{
        width:responsiveWidth(20),
        height:responsiveHeight(15),
        justifyContent:'center',
        alignItems:'center',
    },
    updateItemView:{
        flex:2,
        justifyContent:'center',
        alignItems:'flex-end',
    },
    updateItemView1:{
        marginRight:15,
    },
    titleText:{
        fontSize:responsiveFontSize(2),
        color:'#474747',
    },
    productContentView:{
        width:width,
        flexDirection: 'row',
        // backgroundColor:'green'
        // marginLeft:5,
    },

    productNameandPriceView:{
        // width:width/1.9,
        width:responsiveWidth(53),
        marginLeft:10,
        justifyContent:'center',
        // backgroundColor:'green',
    },
    productName:{
        // width:width/2,
        width:responsiveWidth(50),
        fontSize:responsiveFontSize(2),
        fontWeight: 'bold',
    },
    plus:{
        fontSize:responsiveFontSize(2),
        textAlign:'center',
        color:'#efac27',
    },
    minus:{
        fontSize:responsiveFontSize(2.4),
        textAlign:'center',
        color:'#efac27',
    },
    priceView:{
        flexDirection:'row',
        marginTop:5,
    },
    priceText:{
        color:'#201e1f',
        fontSize:responsiveFontSize(2.2),
        fontWeight: 'bold',
    },
    priceBlock: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginRight:25
    },
    priceLabel:{
        marginRight:10
    },
    qtyView:{
        width:responsiveHeight(8),
        height:responsiveWidth(8),
        borderColor:'#efac27',
        justifyContent:'center',
    },
    qtyText:{
        fontSize:responsiveFontSize(1.8),
        color:'#201e1f',
        // marginLeft:23,
        fontWeight: 'bold',

    },
    buttonView:{
        justifyContent:'center',
        alignItems: 'center',
        marginTop:15,
        marginBottom:10
    },
    button: {
        marginTop:10,
        marginBottom: 10,
        width: responsiveWidth(75),
        height:responsiveHeight(6.5),
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor: '#201e1f'
    },
    buttonText: {
        color: 'white',
        fontSize:responsiveFontSize(1.7),
    },
    smallButton:{
        width: 120,
        backgroundColor: '#201e1f',
        alignItems: 'center',
    },
    footerbutton: {
        width: 180,
        height:60,
        backgroundColor:'#201e1f',
        alignItems: 'center',
    },
    alternativeLayoutButtonContainer:{
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    datalabel:{
        flexDirection: 'row',
    },
    infolabel:{
        fontSize:16,
        marginTop:5,
    },


    containerCrossSell: {
        flex:1,
        margin:5,
        backgroundColor: '#ffffff',
    },
    imgView:{
        borderBottomWidth:1,
        borderColor:'#ededed',
    },
    imgSize:{
        width: responsiveHeight(25),
        height:responsiveHeight(25),
        resizeMode: 'contain',
    },
    nameViewCrossSell:{
        marginLeft:12,
        marginTop:5,
    },
    nameTextCrossSell: {
        color:'#979797',
        fontSize:responsiveFontSize(1.8),
        width:responsiveWidth(40),
    },
    priceViewCrossSell:{
        flex:1,  
        flexDirection:'row',
        marginBottom:5,
        marginTop:5,
    },
    priceTextCrossSell:{
        color:'#201e1f',
        fontWeight: 'bold',
        fontSize:responsiveFontSize(2),
        width:responsiveWidth(33),
        marginLeft:12,
    },


    mainView:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    label:{
        marginBottom:10,
        fontSize: responsiveFontSize(1.8),
        justifyContent: 'center',
    },
    labelOffers:{
        marginBottom:10,
        fontSize: responsiveFontSize(1.8),
        justifyContent: 'center',
    },
    editText:{
        width:250,
        height:40
    },
    button1: {
        marginTop:15,
        marginBottom: 10,
        width: responsiveWidth(40),
        height:responsiveHeight(5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#201e1f'
    },
    buttonText1: {
        color: 'white',
        fontSize:responsiveFontSize(1.6),
    },
    logo:{
        width:responsiveWidth(25),
        height:responsiveHeight(25),
        resizeMode:'contain',
        marginLeft:50,
        marginRight:50,
        marginBottom: 10,
    },
})