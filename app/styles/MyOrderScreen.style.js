import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

var {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#ffffff',
    },
    footer:{
        height:responsiveHeight(8.2),
    },
    view1:{
        borderBottomWidth:.2,
        justifyContent:'center',
    },
    view2:{
        flex:1,
        flexDirection:'row',
        margin:15,
    },
    view3:{
        flex:1,
        flexDirection:'column',
        justifyContent: 'center',
    },
    view4:{
        marginLeft:5,
        flexDirection:'column',
        justifyContent: 'center',
    },
    image:{
        height:18,
        width:18,
        resizeMode:'contain',
    },
    text1:{
        fontSize:responsiveFontSize(2),
        fontWeight:'bold',
        color:'#474747',
    },
    text2:{
        fontSize:responsiveFontSize(1.8),
        color:'#959595',
    },
    text3:{
        color:'#f3a117',
        fontSize:responsiveFontSize(1.8),
        textAlign:'right',
    },
})