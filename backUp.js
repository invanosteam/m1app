import React from 'react';
import {AppRegistry,StyleSheet, Text,Button,View,} from 'react-native';
import { StackNavigator } from 'react-navigation';


class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Welcome',
  };
	render() {
	const { navigate } = this.props.navigation;
     return (
       <View style={styles.container}>
         <Button style={styles.Button}onPress={() => navigate('Second', { user: 'Lucy' })}
           title="Click"/>
       </View>
     );
   }
}

class SecondScreen extends React.Component {
  // Nav options can be defined as a function of the screen's props:
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.user}`,
  });
  render() {
    // The screen's current route is passed in to `props.navigation.state`:
    const { params } = this.props.navigation.state;
    return (
      <View>
        <Text>Chat with {params.user}</Text>
      </View>
    );
  }
}

class SplashScreen extends React.Component {
	static navigationOptions = {
		title: '',
	};

  componentWillMount() {
		const { navigate } = this.props.navigation;
    setTimeout(() => {
			navigate('Home')
    }, 2500);
  }
  render() {
    return (
      <View style={{flex: 1,
				backgroundColor: '#246dd5',
				alignItems: 'center',
				justifyContent: 'center'}}>
        <Text style={{color: 'white', fontSize: 32,}}>Welcome</Text>
      </View>
    );
  }
}


const SimpleApp = StackNavigator({
	SplashScreen:{screen: SplashScreen},
  Home: { screen: HomeScreen },
	Second: { screen: SecondScreen },

});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  Button: {
		margin: 100,
		width: 100,
		height: 50,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
	text:{
		textAlign: 'center',
	},
});


AppRegistry.registerComponent('AwesomeProject', () => SimpleApp);
